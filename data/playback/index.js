var convertToConll = function(word, i) {
    return i+1 + '\t' + word.original + '\t_\t' + word.pos_tag + '\t' + word.pos_tag +
        '\t_\t' + word.syntax_parent_local + '\t' + word.syntax_relation;
};

function sentenceIdForWord(wordId, data_sentences) {
    return _.findIndex(data_sentences, function(sentence) {
        return wordId >= sentence.start_word && wordId < sentence.end_word; });
}

function wordsForSentence(sentence, words) {
    return _.filter(words, function(word, i) {
        return i >= sentence.start_word && i < sentence.end_word; });
}

$(document).ready(function() {
    var audio = $('#greeting')[0];

    // set the audio extension from the config
    $.getJSON("config.json", function(data) {
        var parts = data.input.audio.split(".");
        var ext = parts[parts.length - 1];
        $('#greeting').attr("src", "audio." + ext);
    });

    var pop = Popcorn("#greeting");

    var old_sentence_id = -1;
    var last_word_end;

    $.getJSON("data.json", function(data) {
        var text = $("#text");
        var jsonArea = $("#current-json-chunk");

        // build dom
        $.each(data.chunks, function(chunk_id, chunk) {
            var penaltyElement = $('<div class="penalty"></div>');
            var chunkElement = $('<div class="chunk"></div>');
            chunkElement.attr("id", "chunk" + chunk_id);

            var words = data.words.slice(chunk.start_word, chunk.end_word);

            // this assumes only one linebreak ever
            if (chunk.line_breaks.length > 0) {
                var lineBreakPos = chunk.line_breaks[0];
            }

            var chunkStart = null, chunkStop = null;
            var chunkText = "";
            var chunkJSONWords = [];

            // add chunk penalty information to end of line
            chunkPenaltyFormatted =
                '<i>length penalty:</i> ' +
                chunk.penalty[1] + (chunk.penalty[1] !== '' ? ' = ' : '') +
                '<b>' + Math.round(chunk.penalty[0]*100)/100 + '</b><br>' +
                '<i>single penalties:</i><br>' +
                _.map(chunk.penalty[2], function(p) {
                    return (p === 'len splits <= 1' ?
                        'len splits <= 1' : '<b>' + p[0] + '</b>: ' + p[1]);
                }).join('<br>') +
                '<br><b>line end penalty</b>:<br>' + chunk.penalty[3];
;
            // console.log(chunk.penalty[3]) +;

            console.log(chunk.penalty[2]);
            var penaltySpan = $('<span class="penalties">' + chunkPenaltyFormatted + '</span><br/>');
            penaltyElement.append(penaltySpan);

            $.each(words, function(i, word) {
                // var metaInfo = JSON.stringify(word);
                var metaInfo = 'index: ' + word.index + '\n' +
                                'syntax_relation: ' + word.syntax_relation + '\n' +
                                'syntax_parent: ' + + word.syntax_parent + '\n' +
                                'pos_tag: ' + word["pos_tag"] + '\n';

                // insert line break maybe
                if (i == lineBreakPos) {
                    chunkElement.append($('<br>'));

                    // for the current-chunk box
                    chunkText+='<br/>';
                }
                var wordElement = $('<span title="'+metaInfo+'" class="word"></span>');
                chunkElement.append(wordElement);

                wordId = i + chunk.start_word;

                wordElement.text(word.original);
                wordElement.attr("id", "w" + wordId);
                wordElement.addClass(word.start ? "found" : "notfound");

                // add highlight footnote for word
                pop.footnote({
                    start: word.start/1000,
                    text: '',
                    target: wordElement.attr("id"),
                    effect: "applyclass",
                    applyclass: "selected"
                });

                // console.log(convertToConll(word, i));

                if(word.start) {
                    wordElement.click(function() {
                        audio.currentTime = word.start/1000;
                        audio.play();
                    });
                }

                var sentence_id = sentenceIdForWord(wordId, data.sentences);
                // console.log(word.start/1000, last_word_end);
                if (sentence_id != old_sentence_id) {
                    var sentence = data.sentences[sentence_id];
                    var wordsForSentence_ = wordsForSentence(sentence, data.words);
                    var rawConllForSentence = _.map(wordsForSentence_, function(word,i)     { return convertToConll(word, i);});

                    // console.log(rawConllForSentence);


                    pop.code({
                        start: word.start/1000,
                        onStart: function(options) {
                            // $('#current-graph').html(sentence_id);
                            drawTree('.tree svg', rawConllForSentence.join('\n'), false);
                        },
                        onEnd: function(options) {}
                    });

                }

                chunkText += word.original;

                chunkJSONWords.push(word);

                if(word.start && (chunkStart == null || word.start < chunkStart)) chunkStart = word.start;
                if(word.stop  && (chunkStop  == null || word.stop  > chunkStop )) chunkStop  = word.stop;

                old_sentence_id = sentence_id;
                last_word_end = word.stop/1000;
            });


            // highlight current row with light blue
            pop.footnote({
                start: chunkStart/1000,
                end: chunkStop/1000,
                text: '',
                target: chunkElement.attr("id"),
                effect: "applyclass",
                applyclass: "active"
            });

            // highlight json display with blue when chunk is being displayed
            pop.footnote({
                start: chunkStart/1000,
                end: chunkStop/1000,
                text: '',
                target: jsonArea.attr('id'),
                effect: "applyclass",
                applyclass: "active"
            });

            // show chunk in black box on top
            pop.footnote({
                start: chunkStart/1000,
                end: chunkStop/1000,
                text: chunkText,
                target: "current-chunk"
            });

            // show the corresponding json
            pop.footnote({
                start: chunkStart/1000,
                end: chunkStop/1000,
                text: '<pre>' + library.json.prettyPrint(chunkJSONWords) + '</pre>',
                target: "current-json-chunk"
            });

            pop.code({
                start: chunkStart/1000,
                end: chunkStop/1000,
                onStart: function( options ) {
                    var text = $("#text");
                    var top = chunkElement.offset().top - text.offset().top + text.scrollTop() - text.height() / 2 + chunkElement.height() / 2;
                    $("#text").animate({
                        scrollTop: top
                    });
                },
                onEnd: function () {}
            });

            text.append(penaltyElement);
            text.append(chunkElement);
        });
    });

    $("html").on("keydown", function (e) {
        if(e.keyCode == 32) e.preventDefault();
    });

    $('body').keydown(function(e){
        // user has pressed space
        var audio = $('#greeting')[0];
        if(e.keyCode == 32){
            if(audio.paused) {
                audio.play();
            }
            else {
                audio.pause();
            }
            e.preventDefault();
            return false;
        }
        // // left arrow: skip back 1s
        // else if (e.keyCode == 37) {
        //     audio.currentTime -= 1;
        // }
        // // right arrow: skip forward 1s
        // else if (e.keyCode == 39) {
        //     audio.currentTime += 1;
        // }
    });


});
