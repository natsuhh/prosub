import subprocess, os
rootDir = subprocess.check_output(
    'git rev-parse --show-toplevel'.split(' ')).strip().decode('utf-8', 'replace')
resourcesDir = os.path.join(rootDir, 'resources')

import sys

def srt_parser(srt_file):
    with open(srt_file) as lines:
        for line in lines:
            nr = line
            timing = next(lines)

            text = ''
            while 1:
                text_line = next(lines).strip()
                if text_line != '':
                    text += text_line
                else: break

            yield text

def convert_srt(srt_file):
    text_lines = list(srt_parser(srt_file))

    # slight preprocessing: remove html markup annotations (i.e. '<i>applause</i>')
    result_without_markup = [line for line in text_lines if not line.startswith('<')]

    result = '\n'.join(result_without_markup)

    # now output to *.txt file
    output_file = os.path.basename(srt_file).split('.')[0] + '.txt'
    output_file_path = os.path.join(resourcesDir, output_file)
    print('\nOutput to {}.'.format(output_file_path))

    open(output_file_path, 'w').write(result)

if __name__ == "__main__":
    if len(sys.argv) == 1:
        print('Usage: convert-srt.py <input.srt>')
        sys.exit()

    args = sys.argv
    input_file = args[1]
    convert_srt(os.path.join(resourcesDir,input_file))

