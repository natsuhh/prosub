#!/bin/bash

PARSER=TurboParser-2.2.0

cd data/

if ! hash cython 2>/dev/null; then
    echo "Please install cython first."
    exit
fi

if [[ ! -e $PARSER ]]; then
    wget http://www.cs.cmu.edu/~afm/TurboParser/$PARSER.tar.gz
    tar xf $PARSER.tar.gz
    rm $PARSER.tar.gz
fi

cd $PARSER
./install_deps.sh

cd python
./install_wrapper.sh