#!/bin/bash

name="$1"
dir="/informatik2/nats/projects/spokenWikipedia/german"

{
./run.py -v -c config/DE.json -n "$name" -o "input.audio=$dir/{name}/audio.wav" -o "data=$dir/$name/normalized.json" -o modules.align.memory=6G -o modules.align.gzip-log=yes -o steps=align
} &> "log/${name}.log"
