#!/bin/bash

name="$1"
dir="/informatik2/nats/projects/spokenWikipedia/german"

{
	~/ffmpeg/ffmpeg-git-20150125-64bit-static/ffmpeg -y -i "$dir/$name/audio.ogg" -acodec pcm_s16le -af "highpass=f=60" -ac 1 -ar 16000 "$dir/$name/audio.wav"
	./run.py -v -c config/DE.json -n "$name" -o "input.audio=$dir/{name}/audio.wav" -o "input.transcript=$dir/{name}/audio.txt" -o "modules.input.mary-url=http://localhost:59125/process"  -o "modules.normalize.mary-url=http://localhost:59125/process" -o steps=input,normalize
} &> "log/${name}.log"
