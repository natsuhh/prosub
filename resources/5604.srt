1
00:00:09,571 --> 00:00:14,927
Host: Ok next up we have Trevor Paglen and the six landscapes of the surveillance state.

2
00:00:14,957 --> 00:00:18,960
Something like that. Ok you know we will work it out.

3
00:00:21,630 --> 00:00:27,440
<i>applause</i>

4
00:00:27,440 --> 00:00:28,977
Thank you guys all so much for coming

5
00:00:28,977 --> 00:00:32,447
this is a fantastic honor to be here with you all

6
00:00:32,447 --> 00:00:38,834
Lets get right into it. We don't have as much time as I would like to have.

7
00:00:39,003 --> 00:00:42,413
I have been spending a lot of time in helicopters latly.

8
00:00:43,389 --> 00:00:48,331
I'm an artist, but I'm not somebody who goes into a studio every day

9
00:00:48,331 --> 00:00:53,470
with some paint and makes a mess and comes out at the end of the day with a painting or anything like that.

10
00:00:53,470 --> 00:00:55,433
I have a very empirical practice,

11
00:00:55,433 --> 00:00:58,545
meaning i go out into the world to do things

12
00:00:58,545 --> 00:01:01,531
and i spent a lot of time running around looking at stuff.

13
00:01:01,531 --> 00:01:08,574
And one of the overarching themes of my projects has to do with trying to push vision

14
00:01:08,574 --> 00:01:13,758
and trying to push perception as far as I can.

15
00:01:13,758 --> 00:01:17,463
Usually to the point were it starts to break down.

16
00:01:17,463 --> 00:01:23,231
The reason for that is that I hope by investigating some of these limit cases of vision,

17
00:01:23,231 --> 00:01:27,517
these limit cases of perception, that we can create a vantage point

18
00:01:27,517 --> 00:01:34,775
that we can than use to look back on ourselves with different kinds of eyes, with fresh eyes if you will.

19
00:01:34,775 --> 00:01:37,569
And for me thats really what i want out of art.

20
00:01:37,569 --> 00:01:40,133
A lot of people want a lot of different things out of art.

21
00:01:58,072 --> 00:01:58,322
A lot of people want a lot of different things out of art.

22
00:01:40,133 --> 00:01:43,159
Some people want beauties, some people want something nice that hangs over their sofa.

23
00:01:44,209 --> 00:01:49,775
What I want out of art is things that help us see the historical moment that we are living in.

24
00:01:49,775 --> 00:01:55,799
So thats what I want. So for me thats what this is sort of about.

25
00:01:55,799 --> 00:01:58,322
Today I want to talk about secrecy.

26
00:01:59,057 --> 00:02:04,983
And I want to talk about how to go about trying to see secrecy.

27
00:02:05,414 --> 00:02:09,121
And this is something that I have spend a lot of time thinking about over the years.

28
00:02:09,121 --> 00:02:19,658
A lot of times people think about secrecy as what you get to know vs. what you don't get to know.

29
00:02:19,658 --> 00:02:26,615
So secrecy is stuff that we don't get to know. And stuff that's not secret is stuff that we do get to know.

30
00:02:26,688 --> 00:02:31,758
I think that way of thinking about secrecy is wrong.

31
00:02:31,808 --> 00:02:37,038
I think in the first instance secrecy is more about a way of doing things

32
00:02:37,096 --> 00:02:42,605
its a way of trying to organize human activities and it has political aspects to it

33
00:02:42,605 --> 00:02:47,320
it has economic aspects to it, it has legal aspects to it, it has cultural aspects to it.

34
00:02:47,583 --> 00:02:57,743
And its a way of trying to do things whose goal is invisibility, silence, obscurity.

35
00:02:57,991 --> 00:03:03,336
So in this first instance of what secrecy is i would think of it as an abstract space.

36
00:03:04,188 --> 00:03:09,038
A kind of organizing logic and I call it an abstrakt space, because it doesn't actually exist.

37
00:03:09,038 --> 00:03:14,930
It conceptual thing, its a way of trying to organize things.

38
00:03:14,930 --> 00:03:18,443
And in that first instance secrecy is very immaterial.

39
00:03:18,443 --> 00:03:29,248
However in the real world secrecy only exists in so far as its logic is applied to things in the world.

40
00:03:29,248 --> 00:03:34,617
In other words the material stuff that everything in the world is made of.

41
00:03:34,617 --> 00:03:41,153
So in real life secrecy is composed of infrastructure and institutions things like the CIA or NSA.

42
00:03:41,490 --> 00:03:47,230
Economic institutions like the so called <i>black budget</i> in the United States

43
00:03:48,204 --> 00:03:51,854
It also is composed of social engineering institutions such as the security classification system,

44
00:03:53,888 --> 00:04:00,195
legal institutions such as the fisa court and the states secret president of the United States and so on and so on.

45
00:04:00,195 --> 00:04:09,251
All of that is a way of trying to underline the fact that I don't think about secrecy as what you get to know vs. what you don't get to know.

46
00:04:09,251 --> 00:04:12,289
And think about it instead as a kind of state within a state.

47
00:04:12,289 --> 00:04:19,618
A state that operates according to a very, very different logic than what we would normally think of as a democratic state.

48
00:04:19,618 --> 00:04:26,281
And also as a series of material practices. Things that happen in the world.

49
00:04:26,281 --> 00:04:43,187
So the question than is: 
If secrecy is a way of organizing institutions and human activities in such a way as to try to render them silent, to render them invisible, how do we go about trying to see them?

50
00:04:43,187 --> 00:04:51,049
And sometimes when I talk about how do about trying to see secrecy, I use this metaphor from cosmology.

51
00:04:51,049 --> 00:04:57,485
That metaphor of dark matter, were we know that 97% of the universe is made of dark matter and dark energy

52
00:04:57,485 --> 00:05:06,648
and stuff that we can't see, we can't directly detect, but we can infer its existence by the influence that it exerts on the visible universe

53
00:05:06,648 --> 00:05:09,849
So its something we can only indirectly detect.

54
00:05:09,849 --> 00:05:18,115
In other words by seeing that interaction of this visible matter with the invisible matter we can learn something about it.

55
00:05:18,115 --> 00:05:26,081
And I think perhaps something is true of secrecy as well, because I think that secrecy is an inherently contradictory thing.

56
00:05:26,081 --> 00:05:32,112
Its a self-contradictory thing and the reason for that is that this organizing logic

57
00:05:32,112 --> 00:05:41,419
of secrecy as it has to take hold as is articulated in the material world it's never completely efficient.

58
00:05:41,419 --> 00:05:46,930
In the first instance the idea of secrecy is not efficient

59
00:05:46,930 --> 00:05:52,320
because if it's true that secrecy has to be made of the same stuff that the rest of the world is made out of

60
00:05:52,320 --> 00:05:58,987
stuff in the world tends to reflect light
right it's visible and fundamentally.

61
00:05:58,987 --> 00:06:04,918
So we have this kind of originally contradiction
in the idea of secrecy.

62
00:06:04,918 --> 00:06:10,514
For example if you gonna build a secret airplane.you can't build it in an invisible factory

63
00:06:10,514 --> 00:06:14,328
with ghost workers you have to build in a factory that looks like any kind of other factory.

64
00:06:14,328 --> 00:06:19,712
You have to build it with people that you know do the same things than any other person does.

65
00:06:19,712 --> 00:06:23,649
So secrecy is not efficient it has as originally contradiction.

66
00:06:23,649 --> 00:06:30,483
And I think that that originally contradiction gives rise
to all sorts of other kinds of contradictions

67
00:06:30,483 --> 00:06:36,051
So methodologically what I'm trying to do often
is find those contradictions.

68
00:06:36,051 --> 00:06:41,323
Where does that secret world intersect with something I can see.

69
00:06:41,323 --> 00:06:44,553
Where does it intersect with something that I can find.

70
00:06:44,553 --> 00:06:55,020
Using that methodology trying to get a glimpse
into the aspect of secrecy and the secret state that surrounds us

71
00:06:55,020 --> 00:07:01,268
all of the time but that we really generally have not trained ourselves to see very well.

72
00:07:01,268 --> 00:07:05,328
So I want to talk about a series of these kinds of contradictions.

73
00:07:05,328 --> 00:07:11,449
One contradiction that has been enormously helpful to me has to do with logistics.

74
00:07:11,449 --> 00:07:20,659
If you are going to you as the United States does
if your gonna operate secret wars all over the world

75
00:07:20,659 --> 00:07:26,153
and your gonna have military bases in nearly 200 countries and spying posts and

76
00:07:26,153 --> 00:07:31,713
spy satellites and this sort of thing you need to have logistical infrastructures to to support that

77
00:07:31,713 --> 00:07:35,050
to you know ferry people around with things around that sort of thing.

78
00:07:35,050 --> 00:07:40,487
A crucial part of any logistical infrastructure tends to involve airplanes.

79
00:07:40,487 --> 00:07:44,803
Just because you have to fly people around, documents around so on and so forth.

80
00:07:44,803 --> 00:07:51,555
A number of years ago this is something I picked up quite a lot

81
00:07:51,555 --> 00:07:58,651
I was very interested in the CIA's extraordinary rendition program this is they're kidnapping
and torture program that they're running all over the world.

82
00:07:58,651 --> 00:08:05,264
This is a guy whom I spent some time working with on the stuff.

83
00:08:05,264 --> 00:08:10,081
One of the ideas that I was working with an investigative journalist,

84
00:08:10,081 --> 00:08:12,754
there's about a dozen people around the world from human rights groups,

85
00:08:12,754 --> 00:08:14,912
other journalists who are trying to figure this stuff out

86
00:08:14,912 --> 00:08:23,451
One of the ideas was that if you could track the movements of airplanes that were known to be involved in this program

87
00:08:23,451 --> 00:08:31,387
then you could perhaps get an understanding of what the geography of this a covert action look like.

88
00:08:31,387 --> 00:08:37,254
We knew people were disappearing are from all over the world we didn't know where they were going

89
00:08:37,254 --> 00:08:43,387
the CIA or the military would come out and say something we caught Abu Zubaydah for example

90
00:08:43,387 --> 00:08:46,515
and then this guy wouldn't show up at Guantanamo Bay.

91
00:08:46,515 --> 00:08:50,916
So if you thought about it you thought this guy has to be in the world somewhere.

92
99:59:59,999 --> 99:59:59,999
And because this guy has to be in the world somewhere, there has got to be some secret prisons.

93
99:59:59,999 --> 99:59:59,999
And that's what we are interested in.

94
99:59:59,999 --> 99:59:59,999
The classic way that you organize logistics if your CIA or certain parts of the military is to create front companies.

95
99:59:59,999 --> 99:59:59,999
You create fake companies to do your logistics for you.

96
99:59:59,999 --> 99:59:59,999
This is very good idea for several reasons.

97
99:59:59,999 --> 99:59:59,999
First: it keeps your name off of things. You can create a fake company, its doing stuff around the world.

98
99:59:59,999 --> 99:59:59,999
It never says CIA, NSA, etc. in the paperwork.

99
99:59:59,999 --> 99:59:59,999
Its a very good idea secondly because there is much more freedom of movement to civilians than military.

100
99:59:59,999 --> 99:59:59,999
If you want to charter a plane and go to Pakistan as a civilian, thats appsolutely no problem.
