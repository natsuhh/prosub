1
00:00:18,240 --> 00:00:25,240
Thank you very much can you.. You can hear me? Yes! I've been at this now 23 years we

2
00:00:25,849 --> 00:00:31,539
worked with my colleagues and I've worked in about 30 countries with advised 9 truth

3
00:00:31,539 --> 00:00:38,539
commissions official truth commissions for UN missions for international criminal tribunals

4
00:00:38,680 --> 00:00:43,010
We have testified in four different cases to internationally to domestically and we've

5
00:00:43,010 --> 00:00:47,670
advised dozens and dozens of non-governmental human-rights groups around the world. The

6
00:00:47,670 --> 00:00:54,360
point of this stuff is to figure out how to bring the knowledge of the people who've suffered

7
00:00:54,360 --> 00:01:00,909
human rights violations to bear On demanding accountability from the perpetrators. Our

8
00:01:00,909 --> 00:01:07,909
job is to figure out how we can tell the truth. It is on of the moral foundations of the international

9
00:01:08,330 --> 00:01:14,490
human rights movement that we speak truth to power we look in the face of the powerful

10
00:01:14,490 --> 00:01:21,490
and we tell them what we believe they have done that is wrong if that's gonna work, we

11
00:01:21,939 --> 00:01:28,939
have to speak the truth we have to be right, we have to get the analysis on. That's not

12
00:01:30,079 --> 00:01:36,469
always easy and to get there they're sort of three themes that I wanna try to touch

13
00:01:36,469 --> 00:01:40,749
in this talk. Since the talk is pretty short I'm really gonna touch on two of them so at

14
00:01:40,749 --> 00:01:45,179
the very end of the talk all invite people like to talk more about the specifically technical

15
00:01:45,179 --> 00:01:52,020
aspects of this work about classifiers about clustering about statistical estimation about

16
00:01:52,020 --> 00:01:56,280
database techniques. People wanna talk about that I'd love to gather and will try to find

17
00:01:56,280 --> 00:01:59,170
a space. I've been fighting with the wiki for two days I think I'm probably not the

18
00:01:59,170 --> 00:02:05,490
only one. We can gather we can talk about that stuff more in detail. So today, in the

19
00:02:05,490 --> 00:02:12,490
next 25 minutes I'm going to focus specifically on the trial of General Jose Efrain Rios Montt

20
00:02:14,870 --> 00:02:21,870
who ruled Guatemala from march 1982 until August 1983. That's general Rios there in

21
00:02:22,610 --> 00:02:29,610
the upper corner in the red tie. During the government of general Rios Montt tens of thousands

22
00:02:31,530 --> 00:02:37,750
of people were killed by the army of Guatemala and the question that has been facing guatemalan

23
00:02:37,750 --> 00:02:44,750
since that time is, did the pattern of killing that the army committed constitute acts and

24
00:02:46,950 --> 00:02:52,690
genocide. Now genocide is a very specific crime in international law. It does not mean

25
00:02:52,690 --> 00:02:58,820
you killed a lot of people, there are other war crimes for mass killing. Genocide specifically

26
00:02:58,820 --> 00:03:04,450
means that you picked out a particular group and to the exclusion of other groups nearby

27
00:03:04,450 --> 00:03:11,450
them you focused on eliminating that group. That's key because for statistician that gives

28
00:03:12,610 --> 00:03:18,010
us a hypothesis we can test which is what is the relative risk, what is the differential

29
00:03:18,010 --> 00:03:23,180
probability of people in the target group being killed relative to their neighbors who

30
00:03:23,180 --> 00:03:30,180
are not in the target group. So without further ado let's look at the relative risk of being

31
00:03:30,560 --> 00:03:37,170
killed for indigenous people in the three rural counties of Chajul, Cotzal and Nebaj

32
00:03:37,170 --> 00:03:42,180
relative to their non-indigenous relative to their non-indigenous neighbors. We haven't

33
00:03:42,180 --> 00:03:47,900
all talked in a moment about how we have this, we have information on evidence in estimations

34
00:03:47,900 --> 00:03:54,900
of the death of about 2150 indigenous people. People killed by the army in the period of

35
00:03:55,370 --> 00:04:02,120
the government of General Rios. The population the total number of people alive who were

36
00:04:02,120 --> 00:04:09,120
Indigenous in those counties in the census of 1981 is about 39,000. So the approximate

37
00:04:10,180 --> 00:04:16,810
crude mortality rate due to homicide by the army is 5.5 percent for indigenous people

38
00:04:16,810 --> 00:04:22,160
in that period. Now that's relative to the homicide rate for non-indigenous people in

39
00:04:22,160 --> 00:04:28,039
the same place of approximately .7 percent. So what we ask is what is the ratio between

40
00:04:28,039 --> 00:04:32,630
those two numbers? And the ratio between those two numbers is the relative risk it's approximately

41
00:04:32,630 --> 00:04:39,630
8. We interpret that as if you were an indigenous person alive in one of those three counties

42
00:04:39,780 --> 00:04:46,780
in 1982, your probability of being killed by the army was eight times greater, than

43
00:04:47,349 --> 00:04:54,349
a person also living in those three counties who was not indigenous. Eight times, eight

44
00:04:55,090 --> 00:05:02,090
times! To put that in relative terms the probability, the relative risk of being in Bosnia relative

45
00:05:02,090 --> 00:05:08,419
to being served in Bosnia during the war in Bosnia was a a little less than three. So

46
00:05:08,419 --> 00:05:12,780
your relative risk of being indigenous was more than twice nearly three times as much

47
00:05:12,780 --> 00:05:19,150
as your relative risk of being Bosnia in the Bosnian war. It's an astonishing level of

48
00:05:19,150 --> 00:05:26,150
focus. It shows a tremendous planning and coherence I believe. So, again coming back

49
00:05:27,080 --> 00:05:31,110
to the statistical conclusion, how do we come to that? How do we find that information how

50
00:05:31,110 --> 00:05:34,680
do we make that conclusion? First we're only looking at homicides committed by the army

51
00:05:34,680 --> 00:05:38,090
we're not looking at homicides committed by other parties by the guerrillas by private

52
00:05:38,090 --> 00:05:45,090
actors, we're not looking at excess mortality, the mortality that we might find in conflict

53
00:05:45,680 --> 00:05:50,060
and is in excess of normal peacetime mortality. We're not looking at any of that, only homicide.

54
00:05:50,060 --> 00:05:54,719
And the percentage relates the number people killed by the army with the population that

55
00:05:54,719 --> 00:05:59,219
was alive that's crucial here we're looking at rates and we're comparing the rate of the

56
00:05:59,219 --> 00:06:03,490
indigenous people shown in the blue bar to non-indigenous people shown in the green bar.

57
00:06:03,490 --> 00:06:09,990
The width of the bar show the relative populations in each of those two communities. So clearly

58
00:06:09,990 --> 00:06:13,979
the there are many more indigenous people but a higher fraction of them are also killed.

59
00:06:13,979 --> 00:06:17,770
The bars also show something else and that's what I focus on for the rest of the talk there

60
00:06:17,770 --> 00:06:21,590
are two sections to each of the two bars, a dark section on the bottom a lighter section

61
00:06:21,590 --> 00:06:27,979
on top and what that indicates is what we know in terms of being able to name people

62
00:06:27,979 --> 00:06:33,039
with their first and last name their location and dates of death and what we must infer

63
00:06:33,039 --> 00:06:37,520
statistically. Now I'm beginning to touch on the second theme of my talk: Which is the

64
00:06:37,520 --> 00:06:44,520
when we are studying mass violence in war crimes, we cannot do statistical or pattern

65
00:06:45,270 --> 00:06:52,180
analysis with raw information we must use the tools of mathematical statistics to understand

66
00:06:52,180 --> 00:06:57,090
what we don't know. The information which cannot be observed directly we have to estimate

67
00:06:57,090 --> 00:07:03,139
that in order to control for the process of the production information Information doesn't

68
00:07:03,139 --> 00:07:07,610
just fall out of the sky, the way it does for industry. If I'm running an ISP I know

69
00:07:07,610 --> 00:07:13,180
every packet that runs through my routers. That's not how the social world works. In

70
00:07:13,180 --> 00:07:17,360
order to find information about killings we have to hear about that killing from someone,

71
00:07:17,360 --> 00:07:21,849
we have to investigate, we have to find the human remains and if we can't observe the

72
00:07:21,849 --> 00:07:28,379
killing we won't hear about it and many killings are hidden. In my team we have a a kind of

73
00:07:28,379 --> 00:07:34,379
catch phrase: that the world if a lawyer is killed in a big city at high noon the world

74
00:07:34,379 --> 00:07:40,120
knows about it before dinner time. Every single time. But when a rural peasant is killed three

75
00:07:40,120 --> 00:07:44,770
days walk from a road in the dead of night, were unlikely to ever hear. And technology

76
00:07:44,770 --> 00:07:48,159
is not changing this I'll talk later about that technologies is actually making the problem

77
00:07:48,159 --> 00:07:55,159
worse. So, let's get back to Guatemala and just conclude that the little vertical bars

78
00:07:56,689 --> 00:08:01,789
little vertical lines at the top of each bar indicate the confidence interval. Which is

79
00:08:01,789 --> 00:08:06,199
similar to what lay people sometimes call a margin of error. It is our level of uncertainty

80
00:08:06,199 --> 00:08:11,599
about each of those estimates and you noticed the uncertainty is much much smaller the difference

81
00:08:11,599 --> 00:08:16,159
between the two bars the uncertainty does not affect our ability to draw the conclusion,

82
00:08:16,159 --> 00:08:20,460
that there was a spectacular difference in the mortality rates between the people who

83
00:08:20,460 --> 00:08:27,460
were the hypothesized target of genocide and does who were not. Now the data: first read

84
00:08:29,529 --> 00:08:34,620
the census of 1981, this was a crucial piece. I think there's very interesting questions

85
00:08:34,620 --> 00:08:40,229
to ask about why the government of Guatemala conducted a census on the eve of committing

86
00:08:40,229 --> 00:08:46,320
genocide their is excellent work done by historical demographers about the use of censuses in

87
00:08:46,320 --> 00:08:52,980
mass violence. It has been common throughout history. Similarly, or excuse me, in parallel

88
00:08:52,980 --> 00:08:59,980
there were four very large projects, first the CEH a group of non-governmental human-rights

89
00:09:00,149 --> 00:09:07,149
groups, collected 1240 records of deaths in this three-county region, next the Catholic

90
00:09:07,550 --> 00:09:14,430
Church collected a bit fewer than 800 deaths, the truth commission the Comisión para

91
00:09:14,430 --> 00:09:20,850
el Esclarecimiento Histórico conducted a really big research project in the late

92
00:09:20,850 --> 00:09:26,250
1990s and of that we got information about a little bit more than a thousand deaths and

93
00:09:26,250 --> 00:09:33,250
then the national program for compensation is very very large and gave us about 4700

94
00:09:34,660 --> 00:09:41,660
records of death. Now this is interesting but this is not unique many of the deaths

95
00:09:41,670 --> 00:09:47,790
are reported in common across those data sources and so we think about this in terms of a bend

96
00:09:47,790 --> 00:09:52,240
diagram. We think about how did these different data sets intersect with each other or collide

97
00:09:52,240 --> 00:09:57,790
with each other and we can diagram that as in the sense of these three white circles

98
00:09:57,790 --> 00:10:04,790
intersecting but as I mentioned earlier we're also interested in what we have not observed

99
00:10:05,980 --> 00:10:11,310
and this is crucial for us because when we're thinking about how much information we have

100
00:10:11,310 --> 00:10:15,470
we have to distinguish between the world on the left in which are intersecting circles

101
00:10:15,470 --> 00:10:19,940
cover about a third of the reality versus the world on the right where our intersecting

102
00:10:19,940 --> 00:10:25,060
circles cover all of reality. These are very different worlds and the reason they're so

103
00:10:25,060 --> 00:10:29,550
different is not simply because we want to know the magnitude, not simply because we

104
00:10:29,550 --> 00:10:34,860
want to know the total number of killings thats important. But even more important we

105
00:10:34,860 --> 00:10:41,769
have to know that we've covered with estimated in equal proportions the two parties. We have

106
00:10:41,769 --> 00:10:46,410
to estimate in equal proportions the number of deaths of non-indigenous people and the

107
00:10:46,410 --> 00:10:51,120
number of deaths of indigenous people because if we don't get those estimates correct our

108
00:10:51,120 --> 00:10:56,269
comparison of their mortality rates will be biased. Our store will be wrong we will fail

109
00:10:56,269 --> 00:11:03,089
to speak truth to power. Can't have that. So what do we do? Algebra! Algebra is our

110
00:11:03,089 --> 00:11:07,959
friend, so I'm gonna give you just a tiny taste of how we solve this problem and I'm

111
00:11:07,959 --> 00:11:11,250
going to introduce a series of assumptions those of you who would like to debate those

112
00:11:11,250 --> 00:11:16,930
assumptions: I invite you to join me after the talk and we will talk endlessly and tediously

113
00:11:16,930 --> 00:11:23,930
about capture heterogeneity. But in the short term, we have a universe of total killings

114
00:11:24,790 --> 00:11:31,040
in a specific time-space ethnicity location and of that we have to projects A and B. A

115
00:11:31,040 --> 00:11:36,910
captures some number of deaths from the universe N, and the probability of with which a death

116
00:11:36,910 --> 00:11:42,500
is captured by project A from the universe N is by elementary probability theory the

117
00:11:48,060 --> 00:11:53,370
N. Similarly the probability with which a death from N is documented by project B is

118
00:11:53,370 --> 00:11:59,509
B over N and this is the cool part: the probability with which are death is documented by both

119
00:11:59,509 --> 00:12:05,949
A and B is M. Now we can put the two databases together we can compare them. Let's talk about

120
00:12:05,949 --> 00:12:10,899
the use of random force classifiers and clustering to do that later but we can put the two databases

121
00:12:10,899 --> 00:12:15,589
together compare them, determine the deaths that are in M that is in both A and B and

122
00:12:15,589 --> 00:12:22,589
divide M by N But also by probability theory the probability that a death occurs in M is

123
00:12:23,529 --> 00:12:29,860
equal to the product of the individual probabilities. The probability of any compound event, an

124
00:12:29,860 --> 00:12:34,459
event made up of two independent events is equal to the product of those two events,

125
00:12:34,459 --> 00:12:41,459
so M over N is equal to A over N times B over N solve for N. solve for N. Multiplied by

126
00:12:42,310 --> 00:12:46,800
through N squared and divide by M we have an estimate N which is equal to AB over M.

127
00:12:46,800 --> 00:12:50,980
square Now the lights in my eyes I can't see but I saw a few light bulbs go off over people's

128
00:12:50,980 --> 00:12:57,670
heads and when I showed this proof to the judge in the trial of general Rios I saw a

129
00:12:57,670 --> 00:13:04,670
light bulb go on over her head. It's a beautiful thing, so a beautiful thing. *Applause* So

130
00:13:10,000 --> 00:13:13,709
we don't do it in two systems because that takes a lot of assumptions we do it in four.

131
00:13:13,709 --> 00:13:16,300
You will recall that we have four data sources that we have for data sources we organize

132
00:13:16,300 --> 00:13:22,940
the data sources in this format such that we have an inclusion an explosion pattern

133
00:13:22,940 --> 00:13:27,610
in the table on the left which, for which we can define the number of deaths which fall

134
00:13:27,610 --> 00:13:32,800
into each of these intersecting patterns and I'll give you a very quick metaphor here.

135
00:13:32,800 --> 00:13:37,860
The metaphor is: imagine that you have two dark rooms and you want to assess the size

136
00:13:37,860 --> 00:13:41,970
of those two rooms which room is larger and the only tool that you have to assess the

137
00:13:41,970 --> 00:13:46,750
size of those rooms is a handful a little rubber balls. The little rubber balls have

138
00:13:46,750 --> 00:13:50,910
a property that when they hit each other they make a sound *Makes one click sound* so we

139
00:13:50,910 --> 00:13:55,870
throw the balls into the first round and we listen and we hear *Makes several click sounds

140
00:13:55,870 --> 00:13:59,009
We collect the balls and go to the second room, throw them with equal force imagining

141
00:13:59,009 --> 00:14:03,620
us (???) with equal density we throw the balls into the second room with equal force and

142
00:14:03,620 --> 00:14:09,009
we hear *Makes one clicking sound* So which room is larger? The second room, because we

143
00:14:09,009 --> 00:14:14,290
hear fewer collisions, right? Well the estimation, the tox example I gave in the previous slide

144
00:14:14,290 --> 00:14:20,089
is the mathematical formalization of the intuition that fewer collisions mean a larger space

145
00:14:20,089 --> 00:14:24,439
and so what we're doing here is laying out the pattern of collisions not just the collisions

146
00:14:24,439 --> 00:14:30,319
the pairwise collisions but the three way and 4-way collisions and that allows us to

147
00:14:30,319 --> 00:14:37,319
make the estimate that was shown in the bar graph of the light part of each of the bars.

148
00:14:37,519 --> 00:14:41,790
So we can come back to our conclusion and put a confidence interval on the estimates

149
00:14:41,790 --> 00:14:46,579
and the confidence intervals are shown there. Now I'm gonna move through this somewhat more

150
00:14:46,579 --> 00:14:51,579
quickly to get to the end of the talk but I wanna put up one more slide that was used

151
00:14:51,579 --> 00:14:58,579
in the testimony and that is that we divided time into sixteen month periods and compared

152
00:14:59,370 --> 00:15:04,649
the 16-month period of general Riosis governance of only 16 month because we April and july

153
00:15:04,649 --> 00:15:09,269
because it is only a few days in August a few days in march so we shaved those off it.

154
00:15:09,269 --> 00:15:14,290
16-month period of general Riosis government and compared it to several periods before

155
00:15:14,290 --> 00:15:21,290
and after and I think that the key observation here is met the rate of killing against indigenous

156
00:15:21,850 --> 00:15:27,019
people is substantially higher done under general Riosis government than under previous

157
00:15:27,019 --> 00:15:34,019
or succeeding governments but more importantly the ratio between the two the relative risk

158
00:15:34,470 --> 00:15:39,759
of being killed as an indigenous person was at its peak during the government of general

159
00:15:39,759 --> 00:15:46,759
Riosis. Have we proven genocide? No. This is evidence consistent with the hypothesis

160
00:15:49,279 --> 00:15:53,790
that a genocide was committed. The finding of genocide is a legal finding not so much

161
00:15:53,790 --> 00:15:59,350
a scientific one. So as scientists our job is to provide evidence but the finders of

162
00:15:59,350 --> 00:16:04,259
fact the judges in this case can use in their determination this is evidence consistent

163
00:16:04,259 --> 00:16:08,749
with the hypothesis. Where this evidence otherwise as scientists we would say we would rechecked

164
00:16:08,749 --> 00:16:13,220
the hypothesis that genocide was committed however with this evidence we find that the

165
00:16:13,220 --> 00:16:20,220
evidence the data is consistent with the the prosecutions hypothesis. So, it worked! Rios

166
00:16:25,709 --> 00:16:32,709
Montt was convicted on genocide charges. *Applause* For a week! Then the Constitutional Court

167
00:16:40,910 --> 00:16:45,420
intervened, there I know a couple of experts on Guatemala here in the audience who can

168
00:16:45,420 --> 00:16:51,930
tell you more about why that happened exactly what happened. However the constitutional

169
00:16:51,930 --> 00:16:58,930
court ordered a new trial, which is at this time scheduled for the very beginning of 2015.

170
00:16:59,699 --> 00:17:06,699
And I look forward to testifying again and again and again and again! *Applause* Look

171
00:17:14,040 --> 00:17:17,410
but i wanna come back to this point because as a bunch of technologist there is a lot

172
00:17:17,410 --> 00:17:23,270
of folks really like technology here. I really like it too. Technology doesn't get us to

173
00:17:23,270 --> 00:17:28,069
science you have to have science to get you to science. Technology helps you to organize

174
00:17:28,069 --> 00:17:32,090
the data. It helps you do all kinds extremely great and cool things without which we wouldn't

175
00:17:32,090 --> 00:17:38,350
be able to even do the science, but you can't have just technology you can't just have a

176
00:17:38,350 --> 00:17:43,429
bunch of data and make conclusions that's naive and you will get the wrong conclusions.

177
00:17:43,429 --> 00:17:47,029
The point of rigorous statistics is to be right and there is a little bit of a caveat

178
00:17:47,029 --> 00:17:50,900
there - or to at least know how uncertain you are. Statistics is often called the science

179
00:17:50,900 --> 00:17:57,150
of uncertainty that is actually my favorite definition of it. So I'm going to assume that

180
00:17:57,150 --> 00:18:04,150
we care about getting it right. No one laughed, that's good. Not everyone does to my distress.

181
00:18:09,150 --> 00:18:14,450
So if you only have some of the data and I will argue that with always only have some

182
00:18:14,450 --> 00:18:19,950
of the data you need some kind of model that will tell you the relationship between your

183
00:18:19,950 --> 00:18:25,400
data and the real world. Statistician pollen inference in order to get from here to there

184
00:18:25,400 --> 00:18:30,029
you're gonna need some kind of a probability model that tells you why your data is like

185
00:18:30,029 --> 00:18:34,600
the world or in what sense you have to tweet, twiddle and do algebra with your data to get

186
00:18:34,600 --> 00:18:41,600
from what you can observe to what is actually true. And statistics is about comparisons

187
00:18:41,950 --> 00:18:45,520
yeah we get a big number and journalist love the big number but it's really about these

188
00:18:45,520 --> 00:18:51,429
relationships and patterns so to get those relationships and patterns in order for them

189
00:18:51,429 --> 00:18:55,620
to be right in order for our answer to be correct everyone of the estimates we make

190
00:18:55,620 --> 00:19:02,620
for every point in the pattern has to be right. It's a hard problem it's a hard problem and

191
00:19:03,039 --> 00:19:06,559
what I worry about is that we have come into this world in which people throw the notion

192
00:19:06,559 --> 00:19:12,169
of big data around as though the data allows us to make an end-run around problems of sampling

193
00:19:12,169 --> 00:19:18,400
and modeling it doesn't. So as technologist reason - you know ranting at you guys about

194
00:19:18,400 --> 00:19:25,400
it is that it's very tempting to have a lot of data and think you have an answer and it

195
00:19:25,690 --> 00:19:32,690
even more tempting because an industry contacts you might be right. Not so much in human rights

196
00:19:33,190 --> 00:19:38,659
not so much. Violence is a hidden process the people who commit violence have an enormous

197
00:19:38,659 --> 00:19:43,919
commitment to hiding, distorting it, explaining it in different ways. All of those things

198
00:19:43,919 --> 00:19:47,960
dramatically affect the information that is produced from the violence that we're going

199
00:19:47,960 --> 00:19:54,730
to use to do our analysis. So we usually don't know what we don't know in human rights and

200
00:19:54,730 --> 00:20:01,120
data collection and that means that we don't know if what we don't know is systematically

201
00:20:01,120 --> 00:20:06,330
different from what we do know. Maybe we know about all the lawyers and we don't know about

202
00:20:06,330 --> 00:20:11,570
the people the countryside maybe we know about all the indigenous people and not the non-indigenous

203
00:20:11,570 --> 00:20:16,990
people. If that were true the argument that I just made would be merely an artifact of

204
00:20:16,990 --> 00:20:21,890
the reporting process rather than some true analysis. Now we did that estimations why

205
00:20:21,890 --> 00:20:26,059
I believe we can reject that critic, but that's what we have to worry about and let's go back

206
00:20:26,059 --> 00:20:31,380
to the bend diagrammed and say which of these is accurate? It's not just for one of the

207
00:20:31,380 --> 00:20:37,080
points in our pattern analysis the problem is that we're going to compare things as in

208
00:20:37,080 --> 00:20:42,080
Peru where we compared killings committed by the peruvian army against killings committed

209
00:20:42,080 --> 00:20:49,080
by the ?? We found there that in fact we knew very little about what the ?? had done. Whereas

210
00:20:51,940 --> 00:20:57,000
we know almost everything what the peruvian army had done. This is called the coverage

211
00:20:57,000 --> 00:21:03,190
rate. The rate between what we know and what we don't know and raw data however big does

212
00:21:03,190 --> 00:21:10,190
not get us to ?? And here is a bunch of kinds of raw data that I've used in and that I really

213
00:21:10,590 --> 00:21:15,240
enjoy using. You know - truth commission testimonies, ?? investigations, press articles, SMS messages

214
00:21:15,240 --> 00:21:19,679
crowdsourcing and your documentation, social media Feeds, perpetrator records, government

215
00:21:19,679 --> 00:21:22,399
archives, state agency registries - I know those sound all the same but they actually

216
00:21:22,399 --> 00:21:26,770
turn out to be slightly different. Happy to talk ?? to tell. Refugee camp records any

217
00:21:26,770 --> 00:21:32,460
non random sample. All of those are gonna take some kind of probability model and we

218
00:21:32,460 --> 00:21:38,700
don't have that many probability models to use. So raw ? data is great for cases but

219
00:21:38,700 --> 00:21:44,010
it doesn't get you to patterns and patterns again patterns are the thing that allow us

220
00:21:44,010 --> 00:21:49,549
to do analysis. They are the thing, the patterns of what get us to something that we can use

221
00:21:49,549 --> 00:21:56,549
to help prosecutors, advocates and the, and the victims themselves. I gave a version of

222
00:21:58,120 --> 00:22:03,250
this talk, a earlier version of this talk several years ago in meeting in Medellín

223
00:22:03,250 --> 00:22:06,970
Columbia. I've worked a lot in Columbia, it's really, it's a great place to work on. There's

224
00:22:06,970 --> 00:22:13,970
really terrific victims rights groups there and a woman from a township smaller than a

225
00:22:14,270 --> 00:22:19,029
county near too Medellín came up to me after the talk and she said: you know a lot

226
00:22:19,029 --> 00:22:22,980
of people you know .. I'm a human rights activist my job is to collect data, I tell stories

227
00:22:22,980 --> 00:22:28,890
about people who have suffered But there are people in my village I know who have had people

228
00:22:28,890 --> 00:22:33,580
in their families disappeared and they're never gonna talk about ever. Never going to

229
00:22:33,580 --> 00:22:40,580
be able to use their names, because they are afraid. Can't name the victims at least we

230
00:22:41,149 --> 00:22:48,149
better count them. So about that counting there's three ways to do it right: You can

231
00:22:49,570 --> 00:22:54,620
have a perfect census, you can have all the data. Yeah it's nice good luck with that.

232
00:22:54,620 --> 00:23:00,860
You can have a random sample of the Population. That's hard. Sometimes doable but very hard

233
00:23:00,860 --> 00:23:07,610
in my experience we rarely interview victims of homicide very rarely. *Laughing* And that

234
00:23:07,610 --> 00:23:11,520
means there's a complicated probability relationship between the person you sampled the interview

235
00:23:11,520 --> 00:23:15,669
and the depth that they talk to you about. Or you can do some kind of mysterious modeling

236
00:23:15,669 --> 00:23:19,980
of the sampling process which is the which is the essence of what I proposed in the earlier

237
00:23:19,980 --> 00:23:26,980
slide. So what can we do with raw data guys? We can say that a case exists. Ok, - that's

238
00:23:28,169 --> 00:23:32,940
actually important! We can say something happened with raw data. We can say we know something

239
00:23:32,940 --> 00:23:38,320
about that case. We can say there were hundred victims in that case or at least a hundred

240
00:23:38,320 --> 00:23:43,860
victims in that case, if we can name a hundred people but we can't do comparisons this is

241
00:23:43,860 --> 00:23:48,520
the biggest massacre this year. We don't really know because we don't know about that massacres

242
00:23:48,520 --> 00:23:54,470
we don't know about. No Patterns. Don't talk about the hot spots of violence. No we don't

243
00:23:54,470 --> 00:24:01,470
know that. Happy to talk more about that if we gather after but i wanna come to a close

244
00:24:01,559 --> 00:24:08,559
here with the importance of getting it right. I've talked about one case today this is another

245
00:24:09,080 --> 00:24:15,419
case the case of this man: Edgar Fernando Garcia. Mister Garcia was a student labor

246
00:24:15,419 --> 00:24:21,019
leader in guatemala early in the 1980s. He left his office in February 1984 did not come

247
00:24:21,019 --> 00:24:26,409
home. People reported later that they saw someone shoving mister Garcia into a vehicle

248
00:24:26,409 --> 00:24:33,409
and driving away. His widow became a very important human rights activist in Guatemala

249
00:24:34,260 --> 00:24:40,100
and now she's a very important and in my opinion impressive politician and there's her infant

250
00:24:40,100 --> 00:24:44,990
daughter. She continued to struggle to find out what had happened to mister Garcia for

251
00:24:44,990 --> 00:24:50,850
decades and in 2006 documents came to light in the national archives of the.. excuse me

252
00:24:50,850 --> 00:24:55,769
of the historical archives of the national Police, showing that the police had realized

253
00:24:55,769 --> 00:25:01,110
an operation in the area of mister Garcia's office and it was very likely that they had

254
00:25:01,110 --> 00:25:07,159
disappeared him. These two guys up here in the upper right were police officers in that

255
00:25:07,159 --> 00:25:13,140
area, they were arrested charged with the disappearance of mister Garcia and convicted.

256
00:25:13,140 --> 00:25:16,419
Part of the evidence used to convict them was communications meta data showing that

257
00:25:16,419 --> 00:25:22,919
documents flowed through the archive. I mean paper communications. We coded it by hand

258
00:25:22,919 --> 00:25:29,919
we went through and read the from and to lines from every Memo and they were convicted in

259
00:25:30,549 --> 00:25:37,549
2010 and after that conviction mister Garcias infant daughter - now a grown woman - was

260
00:25:37,590 --> 00:25:42,990
clearly joyful. Justice brings closure to a family that never knows when to start talking

261
00:25:42,990 --> 00:25:49,570
about someone in the past tense. Perhaps even more powerfully: those guys grand boss their

262
00:25:49,570 --> 00:25:56,570
bosses boss kernel Héctor Bol de la Cruz, this man right here, was convicted of mister

263
00:25:57,380 --> 00:26:04,380
Garcias disappearance in september this year. *Applause* I don't know if any of you have

264
00:26:08,779 --> 00:26:12,899
ever been dissident students but if you've been dissidents students demonstrating in

265
00:26:12,899 --> 00:26:19,600
the streets think about how you would feel if your friends and comrades were disappeared

266
00:26:19,600 --> 00:26:23,890
and take a long look at kernel de la Cruz. Here is the rest of the stuff that we will

267
00:26:23,890 --> 00:26:27,140
talk about if we gather afterwards. Thank you very much for your attention I really

268
00:26:27,140 --> 00:26:28,450
have enjoyed CCC *Applause*
