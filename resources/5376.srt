1
00:00:10,667 --> 00:00:14,926
Host: So the first speaker for this conference is lizvlx.

2
00:00:17,587 --> 00:00:24,198
She runs ubermorgen.com she is really interested in software art, photography and net art.

3
00:00:24,791 --> 00:00:30,701
And if you heard about Vote-Auction that was her.

4
00:00:32,734 --> 00:00:39,467
This talk will be about anti terrorist laws implications on free thinking and art.

5
00:00:39,467 --> 00:00:41,762
Thank you very much.

6
00:00:43,149 --> 00:00:46,442
<i>applause</i>

7
00:00:50,487 --> 00:00:53,744
lizvlx: Well hello, sorry that took a little bit.

8
00:00:53,744 --> 00:00:58,658
I'm using the wrong stuff apparently not Microsoft so I'm really sorry.

9
00:00:59,303 --> 00:01:03,393
"Do you think that's funny" that's the title of this talk

10
00:01:03,393 --> 00:01:07,060
because this was a question we got asked quite a few years ago

11
00:01:07,060 --> 00:01:11,844
by a reporter wanting to discuss a project that we did

12
00:01:11,844 --> 00:01:17,132
and I thought it was the stupidest question I have ever been asked

13
00:01:17,132 --> 00:01:22,628
and I have loved it ever since because obviously this shit is not funny at all.

14
00:01:22,628 --> 00:01:27,309
So because it's about art-practice under anti-terror laws.

15
00:01:27,309 --> 00:01:33,845
I'm not going to give this theoretical talk now, so you're fine it's about practice.

16
00:01:33,845 --> 00:01:45,166
I'm gonna show you a set projects and focus on kind of the implications of anti-terror laws on the project,

17
00:01:45,166 --> 00:01:47,800
on the production of the project most and foremost

18
00:01:47,800 --> 00:01:50,419
and also some other projects obviously have been produced

19
00:01:50,419 --> 00:01:56,191
before all this crazy ass laws have come into effect.
So let's just get started.

20
00:01:56,191 --> 00:02:00,614
And of course in the end I'll be doing a Q&A.

21
00:02:00,614 --> 00:02:10,713
So a short introduction for us, I mean talking us means ubermorgen.com so that's me and my partner Hans.

22
00:02:10,713 --> 00:02:17,665
So we started in the 90s and in the 90s it was really great,

23
00:02:17,665 --> 00:02:19,961
I really love that because there was nothing happening

24
00:02:19,961 --> 00:02:25,545
on the net really except for the crazy boring shit and we all thought it was so cool.

25
00:02:25,545 --> 00:02:29,504
It was just really nerdy but nothing else.

26
00:02:29,504 --> 00:02:37,680
Obviously I started working in net-art, in 1995 that was a really great time

27
00:02:37,680 --> 00:02:40,611
because nobody cared what you did on the net,

28
00:02:40,611 --> 00:02:46,091
not because there was no audience there was a huge audience.

29
00:02:48,361 --> 00:02:55,537
The net was still not a dot com rearm so it was fine, you could just do whatever you wanted to

30
00:02:55,537 --> 00:03:02,754
So there's a little footnote there because we did you know few problems but it was nothing major.

31
00:03:02,754 --> 00:03:09,832
Also because there was no laws in place the really had an effect onto European citizen

32
00:03:09,832 --> 00:03:20,822
being kinda hassled by the CIA, basically local police
didn't really want to help out the CIA so we were fine at the time.

33
00:03:20,822 --> 00:03:29,032
In the early 2000s dot com had totally run over the net

34
00:03:29,032 --> 00:03:41,639
which is fine obviously but not only and we have to get used to lots of people bugging us for doing what we did.

35
00:03:41,639 --> 00:03:51,096
But it was just small stuff as you can see especially the lawyers letters those are always very creative

36
00:03:51,096 --> 00:03:59,047
and always a good base for a new project. And also at the time there were all these people coming at you

37
00:03:59,047 --> 00:04:04,363
for doing projects and its not necessary you call them art projects it could be anything

38
00:04:04,363 --> 00:04:13,605
that is not commercial. Were you just go like why shouldn't I do it. But all these things that got thrown

39
00:04:13,605 --> 00:04:19,347
at you, were always really valuable pieces of information on how the system works.

40
00:04:19,347 --> 00:04:25,511
You could work with that. You could work your way around it, you sometimes may pay a small legal fine

41
00:04:25,511 --> 00:04:33,059
but you would be fine. That was not true for our Vote-Auction project which cost us an enormous amount of money

42
00:04:33,059 --> 00:04:40,611
but it was worth it.
So in about 2006 to 2008 I kind of noticed,

43
00:04:40,611 --> 00:04:49,518
I started doing a project back than it was called <i>superenhanced</i> and I very much noticed that I was

44
00:04:49,518 --> 00:04:56,209
paying more and more attention to laws that were in place, not that I was thinking that

45
00:04:56,209 --> 00:05:03,039
if I do this project now I would get into trouble a, b, c and d but it was in the back of my head.

46
00:05:03,039 --> 00:05:09,723
Which is not really helping when you're trying to freely create a new project or a thinking space.

47
00:05:09,723 --> 00:05:26,447
As it is now I feel that all these anti-terror-laws totally make us common to this kind of cluster

48
00:05:26,447 --> 00:05:32,749
were we start selfcensoring ourselves. Most of all you see that when you're on facebook.

49
00:05:33,087 --> 00:05:38,415
And don't tell me you're not on facebook obviously it's a bullshit platform but still its really interesting

50
00:05:38,548 --> 00:05:43,514
same is true for twitter. I mean how many times will you say <i>fuck</i> on ether of them.

51
00:05:44,855 --> 00:05:52,048
You're not going to post that picture of that naked butt or the bare-breasted lady or whatever,

52
00:05:52,615 --> 00:05:56,405
because you're like ok I have been using this account for a really long time

53
00:05:56,405 --> 00:06:03,756
I don't want to get turned of now, because I'm using imagery now that might be not legal in the facebook sense.

54
00:06:03,756 --> 00:06:09,268
But this selfcensorship is going on in your head. And it happens online.

55
00:06:09,268 --> 00:06:18,095
As long as it happens online it would be fine, you know it's just facebook. But this creeps into offline life as well.

56
00:06:18,095 --> 00:06:26,372
I'm very sure and I have noticed that with lots of people that the conversations you have in real life mirror the ones online

57
00:06:26,372 --> 00:06:31,993
because you're getting used to this kind of speak, this facebook speak or twitter speak.

58
00:06:31,993 --> 00:06:34,980
And it just censors out lots of stuff.

59
00:06:34,980 --> 00:06:41,737
Our strategies in working are moustly afirmative1

60
99:59:59,999 --> 99:59:59,999
We're not the ones
