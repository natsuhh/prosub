1
99:59:59,999 --> 99:59:59,999
Hey everybody good morning.

2
99:59:59,999 --> 99:59:59,999
Just a quick remark before starting.

3
99:59:59,999 --> 99:59:59,999
Parallel to this talk in Saal 2 there will be Jakob Applebaums second part of <i>Sysadmins of the world, unite!</i>

4
99:59:59,999 --> 99:59:59,999
There have been some problems with the schedule, so it is not going to be displayed.

5
99:59:59,999 --> 99:59:59,999
But in this room instead we are going to have <i>Hacking the Czech Parliament via SMS</i>

6
99:59:59,999 --> 99:59:59,999
done by this art collective which in collaboration with some security experts hacked the government.

7
99:59:59,999 --> 99:59:59,999
The stage is yours thank you.

8
99:59:59,999 --> 99:59:59,999
<i>applause</i>
