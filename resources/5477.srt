1
00:00:09,067 --> 00:00:12,606
So, I have already been introduced

2
00:00:12,606 --> 00:00:15,145
My name is Stefan Widmann

3
00:00:15,145 --> 00:00:26,405
and maybe I can have my slides?

4
00:00:26,405 --> 00:00:34,489
Hello, my slides?

5
00:00:34,489 --> 00:00:40,348
[laughter]

6
00:00:40,348 --> 00:00:47,599
Ich bin am [illegible]

7
00:00:47,599 --> 00:00:52,342
[hums]

8
00:00:52,342 --> 00:00:57,899
Yeah, um...

9
00:00:57,899 --> 00:01:10,568
[mumbling]

10
00:01:10,568 --> 00:01:15,231
Okay, so while we're waiting until my slides appear

11
00:01:15,231 --> 00:01:17,455
somehow.

12
00:01:17,455 --> 00:01:19,630
Who has seen the incredible talk about

13
00:01:19,630 --> 00:01:22,314
hacking the VoIP phones from Cisco, last year

14
00:01:22,314 --> 00:01:25,599
either live or on video?

15
00:01:25,599 --> 00:01:27,394
Okay some of you.

16
00:01:27,394 --> 00:01:30,528
When we think about this talk

17
00:01:30,528 --> 00:01:34,945
The Cisco VoIP phones have an embeddd

18
00:01:34,945 --> 00:01:37,889
Linux operating system, but they did not only

19
00:01:37,889 --> 00:01:41,609
have to deal with the linux OS, but also with

20
00:01:41,609 --> 00:01:44,372
the firmware of the DSP.

21
00:01:44,372 --> 00:01:50,031
So I want to tell you there's not only one system,

22
00:01:50,031 --> 00:01:57,006
but several systems. Several sub-systems containing firmware

23
00:01:57,006 --> 00:02:06,940
Slides would be nice, we can start without slides, there's no problem.

24
00:02:06,940 --> 00:02:09,899
So what are we going to talk about today?

25
00:02:09,899 --> 00:02:15,497
First we are going to talk about motivation, why should we do firmware analysis.

26
00:02:15,497 --> 00:02:25,545
Then we need to be able to do it, so we have some prerequisites to bring with us.

27
00:02:25,545 --> 00:02:33,812
Then we'll dig deep into the topics, We will try to look at how we obtain a firmware,

28
00:02:33,812 --> 00:02:41,191
how can we analyze it, and how can we modify it.

29
00:02:41,191 --> 00:02:55,111
Hmm. We are sorry for the brief hiccup, we're working on that. It's the second talk. I'm sorry.

30
00:02:55,111 --> 00:03:33,154
[speaks german]

31
00:03:33,154 --> 00:03:37,658
Hmm

32
00:03:37,658 --> 00:03:42,884
Okay, so we can do without slides, it's okay.

33
00:03:42,884 --> 00:03:48,902
Let's start with the motication, why do we want to do firmware analysis.

34
00:03:48,902 --> 00:03:57,914
When talking to my lawyer I learned that I had to clean up 90% of my motivation slide.

35
00:03:57,914 --> 00:04:06,568
And left is, you can do it if you want to gain interoperability.

36
00:04:06,568 --> 00:04:10,011
[laughter]

37
00:04:10,011 --> 00:04:18,531
You can do it if you want to get rid of errors, and the manufacturer does not want to or is unable

38
00:04:18,531 --> 00:04:30,830
And one interesting point under discussion is, what about forensics, taking a look in those thousands of devices in everyday life

39
00:04:30,830 --> 00:04:35,073
Do they only do what they are supposed to do?

40
00:04:35,073 --> 00:04:51,051
Yes, we are still hunting for a Video Angel to sort this little problem out. We should have one here within 1 minute

41
00:04:51,051 --> 00:05:06,473
Again, very sorry.

42
00:05:06,473 --> 00:05:15,246
We now have Nick Farr on stage, our certified Powerpoint specialist.

43
00:05:15,246 --> 00:05:17,494
[applause]

44
00:05:17,494 --> 00:06:06,331
It can only take minutes now.

45
00:06:06,331 --> 00:06:14,324
Maybe we can continue. So I will just tell you something about prerequisites you should bring when starting to analyse

46
00:06:14,324 --> 00:06:21,361
You should at least have a good knowledge of embedded system architecture.

47
00:06:21,361 --> 00:06:29,493
You should have dealt with peripherals, bus interfaces and so on

48
00:06:29,493 --> 00:06:34,125
You should be able to read and write assembler.

49
00:06:34,125 --> 00:06:37,916
Some might say I have a very good decompiler, which is fine.

50
00:06:37,916 --> 00:06:45,518
If it works for you, okay, but don't rely on the availability of a decompiler

51
00:06:45,518 --> 00:06:49,191
for the architecture you're going to be working on

52
00:06:49,191 --> 00:06:55,675
Especially if you are going to work low down in the register stuff.

53
00:06:55,675 --> 00:07:03,664
And in my opinion, a decompiler output will confuse you more than help you.

54
00:07:03,664 --> 00:07:12,542
You will go to disassemble maybe, C runtime libraries, optimized to be as small as possible

55
00:07:12,542 --> 00:07:18,368
That can be really hard in decompiler output.

56
00:07:18,368 --> 00:07:28,726
If you want to practice how embedded systems are working, then it might a good idea to fetch

57
00:07:28,726 --> 00:07:36,034
your arduino or whatever. You write some little C code, handling some hardware stuff.

58
00:07:36,034 --> 00:07:45,497
Then you just compile it and take a look, what the disassembly looks like.

59
00:07:45,497 --> 00:07:55,767
Very nice to have is a device reader or programmer, like galib. The problem is they are expensive.

60
00:07:55,767 --> 00:08:03,931
If you think we are going to do firmware analysis, it may be a valuable investment for your hackerspace.

61
00:08:03,931 --> 00:08:09,330
And last but not least, what you need most is time.

62
00:08:09,330 --> 00:08:16,511
Time, time, time. It may take hours, days without any progress, so please be patient

63
00:08:16,511 --> 00:08:29,680
[laughter]

64
00:08:29,680 --> 00:08:35,888
Any volunteers to make up some slides here?

65
00:08:35,888 --> 00:08:50,974
I swear, it worked perfectly okay with my external monitor.

66
00:08:50,974 --> 00:09:03,896
Yes? [illegible]

67
00:09:03,896 --> 00:09:08,305
So I'll have to fetch my USB stick, wait a moment.

68
00:09:08,305 --> 00:09:38,764
No problem, we're flexible.

69
00:09:38,764 --> 00:10:09,571
[whistling]

70
00:10:09,571 --> 00:10:15,257
So is there anyone who knows their way around a computer around here?

71
00:10:15,257 --> 00:10:24,783
While we figure that out, it might be a good possibility to remind you all that we are still looking for some Angels.

72
00:10:24,783 --> 00:10:28,765
You could do video angels, which are in high demand right now.

73
00:10:28,765 --> 00:10:35,449
Or you could just do any other work you'd like to. You can do one or two shifts, that's fine.

74
00:10:35,449 --> 00:10:44,072
It would be greatly appreciated because we require volunteer work for this event.

75
00:10:44,072 --> 00:10:52,132
Also if you brought any beverages in here, it'd be awesome if you could take them out with you.

76
00:10:52,132 --> 00:10:59,931
and put them into the little storage cases located all around the building.

77
00:10:59,931 --> 00:11:08,104
Trust me when we are finished, you'll be able to do this announcement.

78
00:11:08,104 --> 00:11:11,428
Are we good? No, not really.

79
00:11:11,428 --> 00:11:13,359
[laughter]

80
00:11:13,359 --> 00:11:15,812
It looks good on the laptop.

81
00:11:15,812 --> 00:11:19,184
You want to give a quick intro to the new Ubuntu desktop?

82
00:11:19,184 --> 00:11:27,888
Because I did not get that at all.

83
00:11:27,888 --> 00:11:33,403
Yeah, mirror displays man.

84
00:11:33,403 --> 00:11:39,708
4 zur 3 folien.

85
00:11:39,708 --> 00:11:41,821
Now we are making progress

86
00:11:41,821 --> 00:11:45,502
[applause]

87
00:11:45,502 --> 00:11:47,813
Enjoy the talk!

88
00:11:47,813 --> 00:11:51,747
Okay, perfect, now with slides.

89
00:11:51,747 --> 00:11:56,981
One small announcement, there will be 5 more minutes of extra talk at the end.

90
00:11:56,981 --> 00:12:04,203
We'll do that. So just please ignore the yellow bars.

91
00:12:04,203 --> 00:12:07,805
[laughter]

92
00:12:07,805 --> 00:12:18,308
Not really no.

93
00:12:18,308 --> 00:12:28,563
High level devices. Big complexity. YES!

94
00:12:28,563 --> 00:12:31,121
Perfect, thank you, without yellow bars, thank you.

95
00:12:31,121 --> 00:12:41,075
Okay. So we already talked about prerequisites, so now we're going deep into the topics.

96
00:12:41,075 --> 00:12:49,849
First we need to obtain a firmware. We will go from non-invasive to invasive.

97
00:12:49,849 --> 00:12:57,506
Because first thing we want to try is getting the firmware without opening the device.

98
00:12:57,506 --> 00:13:03,262
We will first try to download a plain binary from the manufacturer.

99
00:13:03,262 --> 00:13:12,147
or maybe someone else have extracted a binary and placed it on the internet.

100
00:13:12,147 --> 00:13:19,061
You can try to download a bootdisk, USB, CD-ROM, bootimage, whatever the manuf. provides.

101
00:13:19,061 --> 00:13:25,064
and extract it using, for example, WinRAR on windows or just mount it on linux.

102
00:13:25,064 --> 00:13:37,721
Search for files named .bin, .hex, .s19, .mot. like motorola, .rom or .raw.

103
00:13:37,721 --> 00:13:42,439
Most times binary, that means .bin, .rom or .raw files are already real binary files.

104
00:13:42,439 --> 00:13:50,691
Non-binary files should be converted to bin files, e.g with converters like hex2bin.

105
00:13:50,691 --> 00:13:55,876
If this doesn't work out, maybe we get an updater from the manufacturer

106
00:13:55,876 --> 00:14:00,461
Normally they're .exe files built for windows.

107
00:14:00,461 --> 00:14:07,429
There are different updater types. First the self-extracting archives.

108
00:14:07,429 --> 00:14:12,417
It might be an installer too, like Installshield or whatever.

109
00:14:12,417 --> 00:14:18,973
It might be an updater, simple .exe file without any installation, just containing the image.

110
00:14:18,973 --> 00:14:25,323
It might be an updater that is downloading an image, or it might be some of the others,

111
00:14:25,323 --> 00:14:31,675
but packed with an executable packer like UPX or PECompact.

112
00:14:31,675 --> 00:14:33,487
Let's go a little bit into detail.

113
00:14:33,487 --> 00:14:43,053
So if it's a self-extracting archive search for signatures like RARSFX or PK.

114
00:14:43,053 --> 00:14:53,078
You can unpack them, e.g. if you rename a PK containg file to .zip you can unzip it.

115
00:14:53,078 --> 00:14:59,223
If it is an installer, like Installshield, there are special unpackers, but the problem is they are

116
00:14:59,223 --> 00:15:06,153
very hard to use and extremely version specific. It might work, it might not.

117
00:15:06,153 --> 00:15:16,504
The best way is to just let it install and search in the installed files for a plain image or updater.

118
00:15:16,504 --> 00:15:25,152
If it's an updater containing the firmware image, we can search for the image in the executable

119
00:15:25,152 --> 00:15:28,041
using your favorite hex-editor.

120
00:15:28,041 --> 00:15:33,678
Maybe the updater is writing the data to a file, a temporary file in most cases, and deleting

121
00:15:33,678 --> 00:15:40,735
it after usage. You can use ProcessMonitor which is like strace but on Windows

122
00:15:40,735 --> 00:15:49,403
and you can take a look at what files are written to disk can try to capture it before it's deleted.

123
00:15:49,403 --> 00:15:51,031
Maybe the updater is just checking your device, so its just a little downloader.

124
00:15:51,031 --> 00:15:55,767
Checking your device type, take a look on the ftp-site of the manufacturer and is downloading

125
00:15:55,767 --> 00:16:02,519
an image if there's one available.

126
00:16:02,519 --> 00:16:08,583
So if it's downloading the image to a file, use ProcessMonitor again.

127
00:16:08,583 --> 00:16:19,289
If it's just downloading to RAM, you have to go for a debugger, and dump it from memory.

128
00:16:19,289 --> 00:16:27,297
If you have a packed updater, which of course is only done to save size.

129
00:16:27,297 --> 00:16:36,095
If it's standard UPX, you can download UPX and use UPX -d to unpack it.

130
00:16:36,095 --> 00:16:46,445
Sometimes the manufacturer violate the license of UPX and modify UPX by removing vital file information

131
00:16:46,445 --> 00:16:51,504
to make it un-depackable. So you would need a special unpacker.

132
00:16:51,504 --> 00:17:00,730
Other executable packers are most times designed not to be uncompressed.

133
00:17:00,730 --> 00:17:05,377
So you would need special unpackers too.

134
00:17:05,377 --> 00:17:12,243
One challenge that awaits us is, maybe the updaters contain compressed images.

135
00:17:12,243 --> 00:17:20,714
They are normally unpacked before the image is written to the device, so we can just watch

136
00:17:20,714 --> 00:17:27,245
the process memory with a debugger and dump it.

137
00:17:27,245 --> 00:17:34,205
What's a bit more challenging is when the firmware is sent compressed to the device.

138
00:17:34,205 --> 00:17:41,117
So we have to use invasive techniques we will talk about later.

139
00:17:41,117 --> 00:17:47,752
It's a good idea to get a sniffer ready when you first connect your device to your PC.

140
00:17:47,752 --> 00:17:56,092
Maybe the favourite bloatware coming with the device wants to update it instantly.

141
00:17:56,092 --> 00:18:05,247
What can you do to sniff the transfers? On Windows XP and I'm sorry it's only XP, there's

142
00:18:05,247 --> 00:18:14,313
TraceSPTI, a fantastic tool tracing SPTI; SCSI PassThrough Interface.

143
00:18:14,313 --> 00:18:22,573
So you might think SCSI? I do not have any SCSI devices, but very much communication is done using

144
00:18:22,573 --> 00:18:31,024
this protocol on Windows. to identify S/ATA USB devices, especially if they are ATAPI.

145
00:18:31,024 --> 00:18:43,972
On the linux side you might use Wireshark to trace the communication, because Wireshark on linux can trace and sniff USB.

146
00:18:43,972 --> 00:18:49,468
There are various other tools like Bushound and so on to watch communication on buses.

147
00:18:49,468 --> 00:18:55,152
But the problem is they are normally very expensive.

148
00:18:55,152 --> 00:19:00,767
A problem you'll have if you're trying to sniff the update transfer and reconstruct the image is

149
00:19:00,767 --> 00:19:13,610
that it's like a puzzle. You don't know how to build the image, and if you're doing it right or not.

150
00:19:13,610 --> 00:19:20,643
If we do not have a firmware yet, it might get invasive now.

151
00:19:20,643 --> 00:19:28,791
We'll search for serial interfaces, sometimes they are accesible without opening the device, sometimes not.

152
00:19:28,791 --> 00:19:33,542
Do we have an embedded linux system? Yes, we can search for a serial console.

153
00:19:33,542 --> 00:19:42,221
Maybe we have to use JTAG, there was a very good talk on 27C3 about JTAG, serial flash and so on,

154
00:19:42,221 --> 00:19:47,511
so I've included a link here.

155
00:19:47,511 --> 00:19:53,195
So, still no firmware? Get your screwdriver, let's void warranties.

156
00:19:53,195 --> 00:19:58,555
We open the device and you search for memory devices on the PCB.

157
00:19:58,555 --> 00:20:07,385
If you have a very old device, maybe you'll encounter EPROMS or even PROMS, 27-somethings

158
00:20:07,385 --> 00:20:15,233
If it's a little bit newer, you might see EEPROMS and flash. 28, 29, 39, 49 something and

159
00:20:15,233 --> 00:20:25,947
and the big flash devices with 48-pins for example with various other names.

160
00:20:25,947 --> 00:20:39,497
Very nice to see is that serial flashes, those 8-pin devices 25..., sometimes 24... are more and more becoming the standard

161
00:20:39,497 --> 00:20:47,819
They are easy to de-solder, easy to re-solder and there are cheap readers and programmers available.

162
00:20:47,819 --> 00:20:55,469
But please, even if some say we can do it in system without desoldering the chip, please don't do it.

163
00:20:55,469 --> 00:21:02,327
It can lead to very big problems.

164
00:21:02,327 --> 00:21:07,632
To make it a little bit harder, firmware can be contained in chip-internal memories.

165
00:21:07,632 --> 00:21:17,143
You can try to use proprietary programming interfaces to read the firmware, of course JTAG.

166
00:21:17,143 --> 00:21:24,389
Some devices do have bootloaders in a mask ROM. You can try to use them.

167
00:21:24,389 --> 00:21:29,623
If none of these approaches succeed, you can try microprobing.

168
00:21:29,623 --> 00:21:41,550
There was a talk on last years congress about low-cost chip microprobing, I've included a link here.

169
00:21:41,550 --> 00:21:47,370
So just for matter of completeness I've mentioned CPLDs and FPGAs.

170
00:21:47,370 --> 00:21:53,855
You know CPLDs are built up using internal EEPROMs.

171
00:21:53,855 --> 00:22:04,111
FPGAs, Field Programmable Gate Arrays have internal SRAM and an external serial configuration flash.

172
00:22:04,111 --> 00:22:12,411
Some years ago they were marketed as being reverse-engineer proof, okay. Yeah, maybe.

173
00:22:12,411 --> 00:22:22,960
There's a talk tomorrow, same time I think, in Saal 2, about taking a closer look at FPGAs.

174
00:22:22,960 --> 00:22:29,013
Yeah, congratulations, we've done it, we have our firmware, perfect.

175
00:22:29,013 --> 00:22:33,658
So what's next, now we have to analyze it.

176
00:22:33,658 --> 00:22:40,872
The problem is what processor is used. We don't know which disassembler to use.

177
00:22:40,872 --> 00:22:47,081
So we are searching the web for any datasheets, can we get any information.

178
00:22:47,081 --> 00:22:57,622
Can we find out what processor is in use? The problem is in many cases you won't get the datasheets.

179
00:22:57,622 --> 00:23:08,378
The manufacturer says, you buy 1 million devices a year, and you sign an NDA, you get the datasheets.

180
00:23:08,378 --> 00:23:16,716
Now you have to be really patient, now it gets to trial and error, trying different disassemblers.

181
00:23:16,716 --> 00:23:26,617
You can use specific disassemblers, they are only built for one architecture.

182
00:23:26,617 --> 00:23:30,156
You can use a very good tool, the Interactive Disassembler, IDA. There's a freeware version.

183
00:23:30,156 --> 00:23:37,902
I've included a link in the link section of this talk, but the freeware only has a little set of architectures.

184
00:23:37,902 --> 00:23:46,423
If you want the full set, it gets very expensive. But there is a new tool that I really like.

185
00:23:46,423 --> 00:23:55,070
It's ODA, the Online Disassembler, supporting thirty something architectures, and it's free.

186
00:23:55,070 --> 00:24:01,477
You can upload binary files, you can upload code, and try different architectures,

187
00:24:01,477 --> 00:24:07,504
and find out what might be the correct one, and we'll do that now.

188
00:24:07,504 --> 00:24:12,097
So I've prepared some binary code.

189
00:24:12,097 --> 00:24:17,521
I know which architecture this has been written for, because I did it.

190
00:24:17,521 --> 00:24:24,727
I put this code to Online Disassembler, and I chose different architectures, and now lets look

191
00:24:24,727 --> 00:24:28,283
at what the disassembly looks like.

192
00:24:28,283 --> 00:24:36,501
Let's first start with former Hitachi, now Renesas, H8S. I hope you can read it.

193
00:24:36,501 --> 00:24:42,298
Take some time and please raise your hand if you think this is valid disassembly and we have found

194
00:24:42,298 --> 00:24:48,299
our architecture.

195
00:24:48,299 --> 00:24:53,334
I see one hand.

196
00:24:53,334 --> 00:24:59,589
Okay, I have to disappoint you, I'm sorry, it's not valid disassembly, we can see it in the second line.

197
00:24:59,589 --> 00:25:06,223
The disassembler was not able to disassemble the data and it's just an undefined instruction.

198
00:25:06,223 --> 00:25:12,875
There are several .word's in the code. It's not H8S.

199
00:25:12,875 --> 00:25:22,251
Let's try MIPS. Again take some time and raise your hand if you think that's valid.

200
00:25:22,251 --> 00:25:27,206
[laughter] again?

201
00:25:27,206 --> 00:25:39,480
It's invalid too. We can see it in the second line, because there's a dword that's not disassembled.

202
00:25:39,480 --> 00:25:50,690
What about Panasonic MN103 family? The same hand again? Oh I see another hand.

203
00:25:50,690 --> 00:25:57,127
Ok, several hands now. Yeah, OK, thank you.

204
00:25:57,127 --> 00:26:01,220
So the problem is, it's not valid. I have to disappoint you.

205
00:26:01,220 --> 00:26:06,786
The problem is in this case, it looks really good and you have to dig deeper.

206
00:26:06,786 --> 00:26:14,095
You will have to look at, are all subroutines correct. Do they make sense?

207
00:26:14,095 --> 00:26:17,079
Are there subroutine calls at all and so on.

208
00:26:17,079 --> 00:26:23,144
And you will see something strange. Ok last try.

209
00:26:23,144 --> 00:26:28,813
What about Texas Instruments MSP430?

210
00:26:28,813 --> 00:26:33,588
And again, please raise your hands.

211
00:26:33,588 --> 00:26:40,895
Okay? Yeah, this time it is MSP430!

212
00:26:40,895 --> 00:26:48,172
We have found our architecture, perfect, eureka, bingo, we have it.

213
00:26:48,172 --> 00:27:01,026
But what's next? The offset in the file, of the firmware file we loaded is often not the offset in address space.

214
00:27:01,026 --> 00:27:07,004
This is no real problem when the architecture is using relative adressing.

215
00:27:07,004 --> 00:27:15,073
Relative adressing means we have register content and whatever we want to access is based on some

216
00:27:15,073 --> 00:27:20,069
registers content. Location independent code.

217
00:27:20,069 --> 00:27:30,298
But we have a big problem when absolute adressing is being used, and even architectures supporting

218
00:27:30,298 --> 00:27:36,617
relative adressing do have some absolute adressing, somewhere on some accesses.

219
00:27:36,617 --> 00:27:40,893
We would not know, where's the entry point. Where should we start?

220
00:27:40,893 --> 00:27:47,551
Interrupt vectors might be decoded completely wrong, subroutine calls do not make any sense.

221
00:27:47,551 --> 00:27:54,376
They go to [addresses] outside of our firmware for example, or in the middle of instructions.

222
00:27:54,376 --> 00:28:01,227
So the load offset has to be found.

223
00:28:01,227 --> 00:28:08,909
I'll now show a method I call "call distance search".

224
00:28:08,909 --> 00:28:15,572
We will select closely located subroutine adresses, and we'll have to decide either to use

225
00:28:15,572 --> 00:28:24,629
preceding return instructions in front of the subroutines, or the start of the function entry sequence.

226
00:28:24,629 --> 00:28:31,319
We build a search string containing wildcards, and then we search.

227
00:28:31,319 --> 00:28:36,284
Now we'll do that together, I've prepared an example.

228
00:28:36,284 --> 00:28:45,771
This is 8051 code. The 8051 core is very old, it's an 8-bit controller, but it's still widely used in the field

229
00:28:45,771 --> 00:28:53,654
because it's cheap as dirt and you can implement it wherever you want.

230
00:28:53,654 --> 00:29:01,725
In the left column we see the addresses of our example, from 0x00 to 0x13 hex.

231
00:29:01,725 --> 00:29:13,430
We see four subroutines, with the first being the root subroutine, calling the other three subroutines.

232
00:29:13,430 --> 00:29:23,443
We can see the first call to 0x100 is outside our example, we do not have 0x100 in this example.

233
00:29:23,443 --> 00:29:30,472
So what we do is take the three subroutine adresses and sort them.

234
00:29:30,472 --> 00:29:41,188
So we're getting 0x100, 0x103, and 0x107. We calculate the difference to figure out the length of the subroutines.

235
00:29:41,188 --> 00:29:53,443
We get 3 bytes and 4 bytes. Now we look at how subroutines are built in this specific architecture.

236
00:29:53,443 --> 00:30:01,818
On x86 you will mostly find it, not on the 64-bit platforms, but on the 32-bit and 16-bit platforms,

237
00:30:01,818 --> 00:30:11,500
You will find a stack-frame entry sequence in every function, like push bp or push ebp 0x55

238
00:30:11,500 --> 00:30:15,229
So you can trigger on that one.

239
00:30:15,229 --> 00:30:24,548
On 8051 it's not possible. Take a look at address 0x0A It's 0xE0.

240
00:30:24,548 --> 00:30:28,530
Take a look at address 0x0D , it's 44, and 0x11 is 7B.

241
00:30:28,530 --> 00:30:32,108
The are not equal, it does not help us.

242
00:30:32,108 --> 00:30:41,036
So we look at the preceding returns, and yes there are returns in front of every subroutine.

243
00:30:41,036 --> 00:30:45,483
So we take the 0x22 [ret] as our anchor.

244
00:30:45,483 --> 00:30:55,119
Our search string will look like this; We start with the 0x22, we have a subroutine with a length of 3 bytes,

245
00:30:55,119 --> 00:30:59,563
, so we have 0x22 [ret], two wildcards and again a return.

246
00:30:59,563 --> 00:31:07,203
The second part of the search string encodes the second subroutine with 4 bytes.

247
00:31:07,203 --> 00:31:13,244
So we have wildcard, wildcard, wildcard and again a return [0x22]

248
00:31:13,244 --> 00:31:22,383
In this simple example we get only one hit, perfect. We get a hit at address 0x09.

249
00:31:22,383 --> 00:31:27,888
But we do not want the address of the return, we want the address of the subroutine,

250
00:31:27,888 --> 00:31:33,447
so we are not using the 0x09, we are using the 0x08.

251
00:31:33,447 --> 00:31:40,662
What we do is we take the original destination address 0x0100, we subtract 0x0A

252
00:31:40,662 --> 00:31:49,411
and we get the base address of our code example, which is 0xF6

253
00:31:49,411 --> 00:31:58,392
If we apply this newly found out load offset to the code and we adjust the offset

254
00:31:58,392 --> 00:32:06,043
starting now at 0x00F6 in the left column we see that all three subroutines now match.

255
00:32:06,043 --> 00:32:14,706
The call to 0x0100, the call to 0x0107 and the call to 0x0103.

256
00:32:14,706 --> 00:32:23,116
Ok, I think this was hard, so let's repeat what we have already done.

257
00:32:23,116 --> 00:32:30,647
So we have obtained our image, we have successfully found the processor architecture,

258
00:32:30,647 --> 00:32:35,143
we have found a disassembler to disassemble the firmware,

259
00:32:35,143 --> 00:32:42,446
and we have hopefully found the original load offset. So what's next

260
00:32:42,446 --> 00:32:49,358
Maybe the question arises, is there additional firmware in this device?

261
00:32:49,358 --> 00:32:58,864
I see jumps and calls outside of firmware we already know, although we have adjusted the load offset.

262
00:32:58,864 --> 00:33:04,336
Is it chip internal? We can see it on the figure, maybe we have only firmware part A.

263
00:33:04,336 --> 00:33:10,647
And maybe it's using a library or chip internal part B.

264
00:33:10,647 --> 00:33:19,026
So we will have to see what we can do using a modification of the firmware.

265
00:33:19,026 --> 00:33:26,843
Now having done that, we can start with normal reverse engineering of the code.

266
00:33:26,843 --> 00:33:31,537
We search for strings, we search for references to the strings,

267
00:33:31,537 --> 00:33:39,353
but as we are in a very low end embedded system, maybe we can search for very specialized,

268
00:33:39,353 --> 00:33:48,580
data references and operands. Search for USB descriptor fields, you have extracted with /bin/lsusb

269
00:33:48,580 --> 00:33:59,558
Take a look for USB magics like USBC and USBS, you know these two dwords are used in usb communications.

270
00:33:59,558 --> 00:34:11,847
Take a look for IDE, SATA and ATAPI ID strings, saying "I'm a OCZ SSD device" for instance.

271
00:34:11,847 --> 00:34:19,951
When you've sniffed the device communication you've already found some typical datablocks.

272
00:34:19,951 --> 00:34:22,790
You can try to find them [in the binary]

273
00:34:22,790 --> 00:34:28,145
Last, but not least, maybe the device provides some error codes, and you can search for strings,

274
00:34:28,145 --> 00:34:33,887
or for operands in the opcodes.

275
00:34:33,887 --> 00:34:44,404
It's very interesting to find hidden firmware update sequences, because they would allow non-invasive modifications.

276
00:34:44,404 --> 00:34:51,089
For example search for chip erase and programming commands, you can take the appropriate commands

277
00:34:51,089 --> 00:34:59,043
from the datasheet if there's any external memory device available.

278
00:34:59,043 --> 00:35:04,109
We've done it, we have analysed it and we've learnt a lot about the device.

279
00:35:04,109 --> 00:35:08,387
Now we are going to modify it.

280
00:35:08,387 --> 00:35:15,046
First, we have to think about, If we are going to modify the firmware, we have to prepare

281
00:35:15,046 --> 00:35:19,687
to brick our device.

282
00:35:19,687 --> 00:35:25,452
Manufacturers implement several integrity checks, and why do they do that?

283
00:35:25,452 --> 00:35:34,766
The do it because firmware is stored to flash, which is prone to aging, especially if heat is involved.

284
00:35:34,766 --> 00:35:42,409
So they do checksums. There are softwarebased checksum calculations, CRC for example.

285
00:35:42,409 --> 00:35:48,957
There are even hardwarebased checksums where some HW peripheral will do the job for us.

286
00:35:48,957 --> 00:35:57,179
So what you see in the code is maybe the start offset, the end offset, and if you're lucky the polynomial.

287
00:35:57,179 --> 00:36:03,656
It might be hardcoded in the peripheral too, so you won't see anything.

288
00:36:03,656 --> 00:36:13,168
It can be a combination of both, being done only on startup or cyclically in the background.

289
00:36:13,168 --> 00:36:17,799
What we have to do to modify the firmware is either correct those checksums,

290
00:36:17,799 --> 00:36:26,014
or we have to patch those checksum algorithms not to trigger.

291
00:36:26,014 --> 00:36:32,309
What are the goals of our modification, of course we heard it in our motivation section.

292
00:36:32,309 --> 00:36:45,141
We are about to correct errors, and maybe the errors are contained in another part of firmware we are not

293
00:36:45,141 --> 00:36:51,369
having right now. Maybe we have to dump additional memory regions.

294
00:36:51,369 --> 00:37:02,249
That's what they did in the Cisco VoIP hack. They tried to find a memcpy routine and use it.

295
00:37:02,249 --> 00:37:08,551
If you don't find a memcpy routine maybe you can implement your own. Why not?

296
00:37:08,551 --> 00:37:15,892
You could dump code from other memory regions to output buffers.

297
00:37:15,892 --> 00:37:24,557
If you have space in an external memory device, why not program it to the device and read it from the device.

298
00:37:24,557 --> 00:37:29,918
It can be very interesting to gather more device internal information.

299
00:37:29,918 --> 00:37:45,331
For example doing a RAM dump, because during static analysis, you always wonder what may be in RAM at this and that address.

300
00:37:45,331 --> 00:37:51,185
Now as we have modified the firmware, we can inject it back to the device.

301
00:37:51,185 --> 00:37:54,026
For example using the original updater.

302
00:37:54,026 --> 00:37:58,437
It might contain the next checksum check, who knows.

303
00:37:58,437 --> 00:38:06,598
We can try to re-program it to the external memory device if available, or to the processor.

304
00:38:06,598 --> 00:38:14,163
This might be done using a serial interface, either JTAG or proprietary.

305
00:38:14,163 --> 00:38:17,967
That's it. Thank you very much.

306
00:38:17,967 --> 00:38:25,766
[applause]

307
00:38:25,766 --> 00:38:42,573
Angel: If you have any questions, please line up at the room microphones, there are four here.

308
00:38:42,573 --> 00:38:47,125
Angel: Are there any questions? Microphone 1 please;

309
00:38:47,125 --> 00:38:54,427
Question: Not a question, but a tip. If you need some binary dump or some left over files on windows,

310
00:38:54,427 --> 00:39:06,456
Q: you can deny the delete right, so the install or updater program is unable to delete its tempfiles.

311
00:39:06,456 --> 00:39:08,858
Q: so they are left over after reprogramming the device.

312
00:39:08,858 --> 00:39:10,733
A: Do you have tip what to use in that case?

313
00:39:10,733 --> 00:39:11,723
Q: Sorry?

314
00:39:11,723 --> 00:39:14,262
A: Do you have a tip, is there a special tool?

315
00:39:14,262 --> 00:39:18,902
Q: It's not necessary, windows has the function already built in.

316
00:39:18,902 --> 00:39:20,422
A: OK.

317
00:39:20,422 --> 00:39:25,212
Q:And I don't know the word.

318
00:39:25,212 --> 00:39:27,826
A: OK

319
00:39:27,826 --> 00:39:34,288
Q: But you are able to revoke rights completely from a directory, there's a special right for deleting.

320
00:39:34,288 --> 00:39:37,407
A: OK, thank you.

321
00:39:37,407 --> 00:39:46,603
Angel: Are there any more questions?

322
00:39:46,603 --> 00:39:50,631
Angel: Doesn't look like it, please give a warm round of applause to our speaker Stephan Widmann.

323
00:39:50,631 --> 00:39:54,821
[applause] 
A: On [microphone 2]

324
00:39:54,821 --> 00:39:57,465
Angel: There is one more question... No?

325
00:39:57,465 --> 00:39:59,546
A: OK

326
00:39:59,546 --> 00:40:02,268
Angel: If you're leaving please do take your...

327
00:40:02,268 --> 99:59:59,999

