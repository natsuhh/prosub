1
99:59:59,999 --> 99:59:59,999
Hello everyone. Physical key are getting replaced more and more by electronic alternatives.

2
99:59:59,999 --> 99:59:59,999
Also using RFIDs. And there are many pitfalls in RFID key access systems.

3
99:59:59,999 --> 99:59:59,999
Our next speaker will share a bit of a light into what you can do wrong and how you can exploit it.

4
99:59:59,999 --> 99:59:59,999
Please give a warm round of applause to Adrian Dabrowski.

5
99:59:59,999 --> 99:59:59,999
[applause]

6
99:59:59,999 --> 99:59:59,999
So, welcome to the RFID treehouse of horror.

7
99:59:59,999 --> 99:59:59,999
Well, first a short overview. I will show you some centralized key systems used in Vienna, Austria.

8
99:59:59,999 --> 99:59:59,999
There are apparently a lot of key systems in Vienna.

9
99:59:59,999 --> 99:59:59,999
But probably in other countries too.

10
99:59:59,999 --> 99:59:59,999
Then I will take a short tour on explaining what RFID is and how it works and the I tell you about my ... how to hack a city-wide access control systems.

11
99:59:59,999 --> 99:59:59,999
So let's start. Apparently I think in other cities it's the same but in Austria we have a lot of these centralized keys.

12
99:59:59,999 --> 99:59:59,999
For example the WEZ-2000. The Wiener Einheitszylinder 2000.

13
99:59:59,999 --> 99:59:59,999
[applause]

14
99:59:59,999 --> 99:59:59,999
So it's the venice unified cylinder 2000.

15
99:59:59,999 --> 99:59:59,999
Apparently from a time where everything sounds cool when you add 2000 to it.

16
99:59:59,999 --> 99:59:59,999
So this is actually the key used by the fire brigade to open all the backyards, to open all the posts and gates and bars and parking spaces.

17
99:59:59,999 --> 99:59:59,999
If you need a parking space or deliver something this is the key you get.

18
99:59:59,999 --> 99:59:59,999
There are other interesting keys.

19
99:59:59,999 --> 99:59:59,999
Yes, actually, the regulations on this key. You are not forced to use a padlock that accepts this key.

20
99:59:59,999 --> 99:59:59,999
But if this service need excess they will break your padlock open and you will not get ...

21
99:59:59,999 --> 99:59:59,999
Also, you are not allowed to use a padlock that has a security level that is too high.

22
99:59:59,999 --> 99:59:59,999
Basically everyone uses this one.

23
99:59:59,999 --> 99:59:59,999
The other interesting keys, for example this one. It's for electrical cabinets but then, like with other systems there is some time of application creep, similar too permission creep,

24
99:59:59,999 --> 99:59:59,999
where someone aggregates permissions over time, keys or actually all kinds of solutions tend to aggregate additional use cases.

25
99:59:59,999 --> 99:59:59,999
So for example this key was actually specified in the 60ties only for authorized personal for electrical installations.

26
99:59:59,999 --> 99:59:59,999
And its used indoors and outdoors and meantime it is also used for circuit-breakers, for gas, for heat, for water, for everything.

27
99:59:59,999 --> 99:59:59,999
So basically, some power grid companies started to use another key now.

28
99:59:59,999 --> 99:59:59,999
Since 2009, it is a magnetic key lock, its very complicated but actually every party in the house can get a key to access only circuit breakers for there level and not the other ones.

29
99:59:59,999 --> 99:59:59,999
But basically for all other buildings in Vienna there are just using this key.

30
99:59:59,999 --> 99:59:59,999
And there is a third very interesting key that when i talked to people abroad the didn't believe me.

31
99:59:59,999 --> 99:59:59,999
Its the Z/BG.

32
99:59:59,999 --> 99:59:59,999
And this key actually is very interesting.

33
99:59:59,999 --> 99:59:59,999
It allows you to enter almost all residential buildings in Vienna.

34
99:59:59,999 --> 99:59:59,999
The story behind this is like this.

35
99:59:59,999 --> 99:59:59,999
There was a time where caretaker need to be available at daytime and when for example the post

36
99:59:59,999 --> 99:59:59,999
want to deliver the mail, then the caretaker would open them.

37
99:59:59,999 --> 99:59:59,999
This obligation was canceled and it was also the time where all the door intercom systems where installed.

38
99:59:59,999 --> 99:59:59,999
So someone had the brilliant idea let's put a small cylinder that bypass the door intercom system and just opens the door.

39
99:59:59,999 --> 99:59:59,999
So also this key is used for garbage collections.

40
99:59:59,999 --> 99:59:59,999
Because in vienna al garbage is collected in small rooms that are accessible either from the staircase or the outside.

41
99:59:59,999 --> 99:59:59,999
So, actually this key in the mid-nineteeth when I first came across this key it costed like 150 or 200 Euro on the black market.

42
99:59:59,999 --> 99:59:59,999
However in recent years the price has fallen to something like 10, 15 or 20 Euros.

43
99:59:59,999 --> 99:59:59,999
So, basically, it became very popular actually.

44
99:59:59,999 --> 99:59:59,999
Because after the post delivery service used the key.

45
99:59:59,999 --> 99:59:59,999
Then additional services like in the case before got this key.

46
99:59:59,999 --> 99:59:59,999
Some are official users like fire brigades and emergency services or garbage collection.
