1
99:59:59,999 --> 99:59:59,999
Der Vortragende ist Norbert Schaefers

2
99:59:59,999 --> 99:59:59,999
Er ist Politikwissenschaftler arbeitet unter anderem fuer die rosa luxemburg stifutung

3
99:59:59,999 --> 99:59:59,999
und will euch ueber drohnenkrieg etwas euch erzaeheln

4
99:59:59,999 --> 99:59:59,999
ein phaenomen das die weiterschreibung des kriegs gegen den terror ist

5
99:59:59,999 --> 99:59:59,999
soweit ich das verstanden habe

6
99:59:59,999 --> 99:59:59,999
viel spass

7
99:59:59,999 --> 99:59:59,999
ja herrzlichen dank

8
99:59:59,999 --> 99:59:59,999
bin ich gut zu hoeren?

9
99:59:59,999 --> 99:59:59,999
okay guten aBEND AUCH VON MIR ICH FREUD mich das es auch trotz julian assange doch viele dne weg hierer gefunden haben

10
99:59:59,999 --> 99:59:59,999
mein thema ist ne einfuehrung in das thema der drohennkriege das ist

11
99:59:59,999 --> 99:59:59,999
die konzeption gewesen uz sagen es gibt ne kriegsfuehrung

12
99:59:59,999 --> 99:59:59,999
die etabliert ist mittlerweile als ne eigene form

13
99:59:59,999 --> 99:59:59,999
vom kriegsfuehrung und ich moechter erlaeutern was das eigentlich ist ein paar begriffle klarheiten schaffen

14
99:59:59,999 --> 99:59:59,999
vielleicht schaffen und klarmachen warum gibt es das, das politisch einordnen

15
99:59:59,999 --> 99:59:59,999
und das ist eben einleitend der krieg den terreror

16
99:59:59,999 --> 99:59:59,999
der seit 9/11 gefuehrt wird

17
99:59:59,999 --> 99:59:59,999
ich weiss das es natuerlich auch vorher und anchher und jenseits dieser offiziellen terrorfuehrung auch wafffeneinsaetze gibt

18
99:59:59,999 --> 99:59:59,999
mit politischer ermordung etc ich versuche einfach mal klarzumachen es gibt nen bestimmtes phaenomen was ein ein trend und der trend fuer zu neuem wettruesten

19
99:59:59,999 --> 99:59:59,999
mit richtung autonomer und teilautonomer waffen

20
99:59:59,999 --> 99:59:59,999
genau also erstmal fuer mich persoenlich ist es etwas unklar wieviel technisches wissen hier da ist wieviel politisches wissen

21
99:59:59,999 --> 99:59:59,999
ich versuch das einfach durchzuziehen

22
99:59:59,999 --> 99:59:59,999
wenns ganz nebenher ist kann man auch intervenieren

23
99:59:59,999 --> 99:59:59,999
it is possible to do interventions in english, but we do the talk in german

24
99:59:59,999 --> 99:59:59,999
okay, drohenn, drohnen ist das schlagwort fuer unbenannten flugkoeper, die verngesteuert sind, das sind keine roboter

25
99:59:59,999 --> 99:59:59,999
die eben ne eigene teilweise autonome intelligenz haben oder quasi intelligenz

26
99:59:59,999 --> 99:59:59,999
die selbst handlungen durchfuehren koennen, sondern die drohnen die im kampf gegen den terror bekannt geworden sind

27
99:59:59,999 --> 99:59:59,999
sind eben wirklich dinger die weitestgehend wirklich jede bewegung durch nen piloten der ferngesteuert zum teil halb um de globus rum

28
99:59:59,999 --> 99:59:59,999
die aehm, also das oder diese handlungen von den piloten ausgeloest werden

29
99:59:59,999 --> 99:59:59,999
und das ist eben der unterschied zu dem wo man auch drueber redet

30
99:59:59,999 --> 99:59:59,999
und was in der entwicklung ist das man eben zukuenftige waffensystem hat, die eben roboter

31
99:59:59,999 --> 99:59:59,999
die eben teilweise autonom einzelne handlungen durchfuehren koennen bis hin zu

32
99:59:59,999 --> 99:59:59,999
toetungen die dann von dem system entschieden werden

33
99:59:59,999 --> 99:59:59,999
die unterscheidung ist wichtig da eben oft gesagt wird toeten per joystick

34
99:59:59,999 --> 99:59:59,999
das meint eben genau das da leute sitzen die nicht mehr in kampfgebieten sind

35
99:59:59,999 --> 99:59:59,999
es ist aber noch nicht das worueber eben geredet und was eben der trend der zukunft ist um den es mir auch geht

36
99:59:59,999 --> 99:59:59,999
es sind noch nicht die roboter die jetzt in der entwikclung sind

37
99:59:59,999 --> 99:59:59,999
das sind jetzt wirklich ferngesteuerte geschichten die da fliegen

38
99:59:59,999 --> 99:59:59,999
ich beginne immer genr mir diesem bild

39
99:59:59,999 --> 99:59:59,999
das ist das haeufitgste bild wenn man dorhen auf englisch googlet

40
99:59:59,999 --> 99:59:59,999
und es ist garnkein bild was so gemacht wurde, sondern es ist ne photoshop montage

41
99:59:59,999 --> 99:59:59,999
weil ne den drohennkrieg den gibts in der form seit 2003

42
99:59:59,999 --> 99:59:59,999
da wurde zum ersten mal eine hinrichtungsaktion mit genau so einer drohne wie hier

43
99:59:59,999 --> 99:59:59,999
mit so ner hellfire raktet dran durchgefuehrt im jemen

44
99:59:59,999 --> 99:59:59,999
es war voellig verdeckt es wurde behauptet das das jemenitische militaer das war

45
99:59:59,999 --> 99:59:59,999
und das ist charakterischtisch fuer den groesten teil der zeit wos diese art von kriegsfuehrung gibt

46
99:59:59,999 --> 99:59:59,999
das man darueber nicht redet, das wenn lokale quellen das aufdecken

47
99:59:59,999 --> 99:59:59,999
und das war in jemen der fall, das lokale jounralisten das feunden haben

48
99:59:59,999 --> 99:59:59,999
das ist garnicht jemenistisches militaer gewesen

49
99:59:59,999 --> 99:59:59,999
das das ueberhaupt weder noch dementiert noch kommentiert wurde von dem us militaer was eben eng in jemen mit den

50
99:59:59,999 --> 99:59:59,999
offiziellen sicherheitskraeften zusammenarbeiten

51
99:59:59,999 --> 99:59:59,999
so haben ja mehrere jahre einen schattenkrieg und in den letzten 3 jahren ueberhaupt die frage des dorhnenkriegs

52
99:59:59,999 --> 99:59:59,999
in das licht der oeffentlichkeit gerataen und der ausloeser ist tatsaechlcih zum einen die toetung eines us buergers

53
99:59:59,999 --> 99:59:59,999
im jemen abvlka zum anderen der us wahlkampf in dem ab nem bestimmeten piunmlkt das von obama thematisiert wurde

54
99:59:59,999 --> 99:59:59,999
das man das macht, aber immer unter dem was dann auch im laufe dieses jahres im mai gesagt wurde

55
99:59:59,999 --> 99:59:59,999
bei ner grossen politischen rede man hat da ne kriegsfuehrung die man unter strenge aufsicht stellt

56
99:59:59,999 --> 99:59:59,999
die sehr praezise ist und das man eben auch nicht extensiv einsetzt und man genau gucken wuerde

57
99:59:59,999 --> 99:59:59,999
da komm ich nochmal drauf zurueck, aber sie ahnen shcon, ihr ahnt schon

58
99:59:59,999 --> 99:59:59,999
das das eben nocht so gloreich ist wie das generell gesehen wird

59
99:59:59,999 --> 99:59:59,999
drohnen selbst sind waffensystem

60
99:59:59,999 --> 99:59:59,999
das sinsd kein waffen selbst sondern die schiessen rakten ab

61
99:59:59,999 --> 99:59:59,999
und ich zeig gleich nen paar von den aktuellen flugkoerpern

62
99:59:59,999 --> 99:59:59,999
aber das hier was wir hier gesehen haben ist eben das haeufigste

63
99:59:59,999 --> 99:59:59,999
ich werd auch noch eingehen auf die realitatet der drohnenkriege

64
99:59:59,999 --> 99:59:59,999
das ist eben keine chirurgische kriegsfuehrunst ist

65
99:59:59,999 --> 99:59:59,999
koennen wir ganz einfach zeigen und die beiden einordnungen die wir eingangs sachte

66
99:59:59,999 --> 99:59:59,999
die drohenn im globalen krieg gegen den terror und den beignn des wttreusten fuer den rkieg mit den robotern

67
99:59:59,999 --> 99:59:59,999
das sind die drei kompleze die ich heute abend versuche unterzubringen

68
99:59:59,999 --> 99:59:59,999
also nochmal die merkamale unbemannt ferngesteuert

69
99:59:59,999 --> 99:59:59,999
ich zeig gleich nen kurzen video wo man das nochmal aehen kann, wie das funktionier

70
99:59:59,999 --> 99:59:59,999
das sind im priznip kontainer die man im kriegsgebietaufstellen

71
99:59:59,999 --> 99:59:59,999
aber die eigentlichen grossen dorhnenkontrollzentren sind auf dem gebiet der usa

72
99:59:59,999 --> 99:59:59,999
grossbrittanien und wie wir jetzt in den lezten wochen festgestellt haben durch diverse leaks auch auf dem boden von

73
99:59:59,999 --> 99:59:59,999
deutschland moeglicherweise auch italien

74
99:59:59,999 --> 99:59:59,999
vorgeschichte zu den drohnan als jetzt hardware

75
99:59:59,999 --> 99:59:59,999
wenn man sich das jetzt anguckt

76
99:59:59,999 --> 99:59:59,999
sind nen paar aktuelle modelle

77
99:59:59,999 --> 99:59:59,999
sind in den 70er jahren im vietnamkrieg als auflkaerungsflieger eingesetzt worden

78
99:59:59,999 --> 99:59:59,999
mit nem relativen erfolg

79
99:59:59,999 --> 99:59:59,999
man hat sie zumt eil dann auch zu testzwecken verwendet

80
99:59:59,999 --> 99:59:59,999
aber nen wirklichen durchbruch hatte die drohnentechnologie als unbemannte flugkoerper in den 90-ern als auflkaerungsgeraet im nachen osten

81
99:59:59,999 --> 99:59:59,999
durch die israelische mitliater und streitkraefte und auf der anderen seiten eben durch die nato im spezleln gb usa

82
99:59:59,999 --> 99:59:59,999
die idee der bewaffnung kam in den neunsichern auf also die aufklaerungsdorhen eben mit raketen zu bestuecken ist tatsaechlich dann ab 2001 erfolgt

83
99:59:59,999 --> 99:59:59,999
krude vorlaefer jetzt das man unbemannte flugkorper hat die verngefsteuert sind mit draht das gibts schon seit dem zweiten weltkrieg

84
99:59:59,999 --> 99:59:59,999
das wir dasgen wir haben massenhaften einsatz von dingern die dafuer gebaut werden sowoehl um eben aufklaerung zu betreiben zowies kampfeinsatz und die unbemannt sind, das ist eben neueres phaenomen

85
99:59:59,999 --> 99:59:59,999
si wir sehen hier ein paar aktuelle typen, den predator das ist das urspruengliche haeufigste modell

86
99:59:59,999 --> 99:59:59,999
das is auch das was zum ersten mal eingesetzt wurde, der mensch der das vorgeschlagen hat die dinger zu bewaffnen

87
99:59:59,999 --> 99:59:59,999
also zumindest von dem was man weiss, vielleicht gibts die idee auch schon laenger, aber der hiess coal von black der war der chef der einheit im us militaer oder im geheimdienst

88
99:59:59,999 --> 99:59:59,999
die den bin laden gejagt haben vor 911 und tragischerweise hat er eben genau das modell gehabt weil man das so schlecht bekommt in entgegenen gegenden

89
99:59:59,999 --> 99:59:59,999
das man so ne drohne mit ner hellfire rakete ausruestet und ihn jagt, da kann ich auch gleich nochmal drauf eingehen, aber das ist genau nen punkt gewesen

90
99:59:59,999 --> 99:59:59,999
wo man vor solchen einsaetzen noch zurueckgeschreckt hat in den 90er jahren

91
99:59:59,999 --> 99:59:59,999
und was dann eben mit dieser zaesur 911 sich geaendert hat

92
99:59:59,999 --> 99:59:59,999
wo es den willen gab genau solchen toetungsoperationen durchzufuehren im groessen masse als bisher

93
99:59:59,999 --> 99:59:59,999
ich zeig ein kurzes video vom hersteller von northwhip gunmann

94
99:59:59,999 --> 99:59:59,999
jetzt war ich zu schnell tschuldigung

95
99:59:59,999 --> 99:59:59,999
kurzer eindruck reicht

96
99:59:59,999 --> 99:59:59,999
geht darum dass sind propellerflugzeuge das sind auch keine duesenjaeger

97
99:59:59,999 --> 99:59:59,999
das ist nicht besonders advanced technology in dem sinne wir haben da science fiction modelle rumfliegen

98
99:59:59,999 --> 99:59:59,999
das ist natuerlich fortgeschrittene technologie von allem was da drin ist

99
99:59:59,999 --> 99:59:59,999
also das ist das beste was es an ueberwachungstechnik gibt und wir sehen die kuppel unterhalb

100
99:59:59,999 --> 99:59:59,999
der reaper drohne

101
99:59:59,999 --> 99:59:59,999
da ist nen kamerasystem innen drin

102
99:59:59,999 --> 99:59:59,999
was eben relativ hochaufloesend ist

103
99:59:59,999 --> 99:59:59,999
da sind verschiedene kameras und dafuer haben die so gorgon stare als markennamen gepraegt

104
99:59:59,999 --> 99:59:59,999
man hat damit eben so ne weitwinkel perspektive und kann sehr genau runtergucken

105
99:59:59,999 --> 99:59:59,999
die fliegen halt nicht so besonders hoch in den einsatzgebieten

106
99:59:59,999 --> 99:59:59,999
und filmen und filmen und filmen

107
99:59:59,999 --> 99:59:59,999
die kombination ist jetzt seitdem sie bewaffnet sind ist wirklich die fliegen auch erkundungseinsaetze in bestimmen gegenden die sozusagen hotspots des terrors sind

108
99:59:59,999 --> 99:59:59,999
bestimmte gegenden ehm... regionen in jemen, somalia und

109
99:59:59,999 --> 99:59:59,999
wir haben dann zum teil eben einsaetze, die aus dieser ueberwachungssituation heraus erfolgen

110
99:59:59,999 --> 99:59:59,999
das ist irgendwie auch ne besondere lage fuer die zivilbevoelkerung

111
99:59:59,999 --> 99:59:59,999
da wo die eben in grosser menge patroullienfluege machen und ueber

112
99:59:59,999 --> 99:59:59,999
ueberwachung das ist eben auch pakistan bekannt geworden

113
99:59:59,999 --> 99:59:59,999
also nordpakistan vasiristan, da gibts so ne stammeszone, die vom westen abgeschirmt is

114
99:59:59,999 --> 99:59:59,999
und vom rest des landes

115
99:59:59,999 --> 99:59:59,999
rueckzugsgebiet der taliban da fliegen die taeglich rum

116
99:59:59,999 --> 99:59:59,999
ueber vielen staedten

117
99:59:59,999 --> 99:59:59,999
und dann hoert man eben das sirren dieser propellerund irgendwann wird geschossen

118
99:59:59,999 --> 99:59:59,999
das ist halt wirklich auch ne form von kriegsfuehrung

119
99:59:59,999 --> 99:59:59,999
die dann auch ohne das geschossen wird, terror fuer die zivilbevoelkerung darstellt

120
99:59:59,999 --> 99:59:59,999
wir haben noch son hersteller video

121
99:59:59,999 --> 99:59:59,999
die sind eben auch gut zu hoeren, wenn die dann ueber der stadt kreisen

122
99:59:59,999 --> 99:59:59,999
das ist die reaper wie gesagt das ist die weiterentwicklung der predator drohne

123
99:59:59,999 --> 99:59:59,999
die namen sind eben auch schon angsteinjagend wie die waffenhersteller das manchmal gerne machen

124
99:59:59,999 --> 99:59:59,999
reaper heisst ehm.. das ist der sensenmann

125
99:59:59,999 --> 99:59:59,999
ja und das hat der sozusagen hier realisiert mit vier hellfire raketen

126
99:59:59,999 --> 99:59:59,999
kurz erklaert: hellfire raketen die sind urspruenglich entwickelt worden um zb von hubschraubern auf bodenziele zu schiessen

127
99:59:59,999 --> 99:59:59,999
und zwar die wirklich panzer vernichten sollen

128
99:59:59,999 --> 99:59:59,999
damit wir dann auf zivile ziele geschossen auf terrorverdaechtige

129
99:59:59,999 --> 99:59:59,999
das ist das was die drohnenschlaege ausmacht

130
99:59:59,999 --> 99:59:59,999
damit wird auf menschen geschossen, die eben nicht in kampfsituationen sich befinden

131
99:59:59,999 --> 99:59:59,999
sondern meinetwegen in irgendnem konvoi mit irgendwelchen trucks uebers land fahren

132
99:59:59,999 --> 99:59:59,999
oder es wird eben auf menschen geschossen wo man sagt

133
99:59:59,999 --> 99:59:59,999
den haben wir langegenug hinterher spioniert der ist irgendwie nummer 22 der operatives von al quaida im jemen

134
99:59:59,999 --> 99:59:59,999
und den wollen wir weghaben

135
99:59:59,999 --> 99:59:59,999
das sind die beiden grossen einsatzgebiete

136
99:59:59,999 --> 99:59:59,999
das video hatten wir schon

137
99:59:59,999 --> 99:59:59,999
das hier macht nochmal deutlich das die auch hersteller propaganda

138
99:59:59,999 --> 99:59:59,999
macht nochmal deutlich wie das funktioniert

139
99:59:59,999 --> 99:59:59,999
um das nochmal klarzumachen ich bin kein apologet einer drohnenkriegsfuehrung

140
99:59:59,999 --> 99:59:59,999
da kann man sich mal fragen warum zeigt man diese bilder

141
99:59:59,999 --> 99:59:59,999
das ist genau das image was in den usa zb fuer nen relativen erfolg dieser kriegsfuehrung gesorgt hat

142
99:59:59,999 --> 99:59:59,999
bis zuletzt wo eben auch mehr kritische stimmen laut geworden sind

143
99:59:59,999 --> 99:59:59,999
sogar noch frueher als in europa

144
99:59:59,999 --> 99:59:59,999
das bild was vermittelt ist

145
99:59:59,999 --> 99:59:59,999
wir haben hier ne hochtechnologische waffe

146
99:59:59,999 --> 99:59:59,999
wir brauchen keine leute mehr auf dem boden zu bringen

147
99:59:59,999 --> 99:59:59,999
wir koennen praezises einpeilen, man hat eben diese lenkwaffenbilder auch vorher schon

148
99:59:59,999 --> 99:59:59,999
10 jahre vorher im irak krieg nummer 1 gehabt

149
99:59:59,999 --> 99:59:59,999
wo ein bild vermittelt wurde, wir haben hier ganz gute waffensysteme und

150
99:59:59,999 --> 99:59:59,999
die menschlichen schaeden sind minimiert

151
99:59:59,999 --> 99:59:59,999
bei drohnenkriegsfuehrung ist es eben so, dass das hauptargument warum das gut reinlaeuft

152
99:59:59,999 --> 99:59:59,999
es werden keine eigenen leben mehr in gefahrt gesetzt

153
99:59:59,999 --> 99:59:59,999
die operateure die koennen entweder in solchen kontainern wie hier gezeigt wurde sitzen

154
99:59:59,999 --> 99:59:59,999
in der naehe des kriegsgebietes

155
99:59:59,999 --> 99:59:59,999
gibt so fotodokumentationen wo man sieht das die afghanischen flughafen in kabul sitzen

156
99:59:59,999 --> 99:59:59,999
und die drohnen koennen irgendwo starten

157
99:59:59,999 --> 99:59:59,999
das kann das gleiche flugfeld sein

158
99:59:59,999 --> 99:59:59,999
das kann aber eben um die halbe welt rum sein

159
99:59:59,999 --> 99:59:59,999
der haupteil des kriegs gegen den terror der mit den drohnen gefuehrt wird

160
99:59:59,999 --> 99:59:59,999
wird eben so gefuehrt das meinetwegen von djibuto einem grossen flughafen fuer drohnen

161
99:59:59,999 --> 99:59:59,999
die da starten und dann eben in die umliegenden regionen wo der krieg gefuehrt wird

162
99:59:59,999 --> 99:59:59,999
hinfliegen

163
99:59:59,999 --> 99:59:59,999
und aehm der drohneneinsatz wie vorhin schon erwaehnt dann von den usa aus her gelenkt wird

164
99:59:59,999 --> 99:59:59,999
das heisst die frage das steuern dieser flugzeuge und die konkrete wartung oder ueberhaupt nur diese flugzeuge zu sehen

165
99:59:59,999 --> 99:59:59,999
ist voellig voneinander getrennt

166
99:59:59,999 --> 99:59:59,999
ne, die leute auf diesen flugfeldern sehen das normalerweise nicht

167
99:59:59,999 --> 99:59:59,999
und aehm man hat dann da auch ne diskussion gehabt wo die leute sachten das macht den krieg leichter

168
99:59:59,999 --> 99:59:59,999
wir haben da oehm ja, flieger, die sitzen zu hause, die gehen aehm garnicht mehr ins kriegsgebiet, die haben schichtbetrieb, die gehen danach nach hause und haben familienleben

169
99:59:59,999 --> 99:59:59,999
da gibts eine befuerchtung die sagt das macht es sehr leicht das toeten

170
99:59:59,999 --> 99:59:59,999
weil das eben fernbedienung ist das ist weit weg

171
99:59:59,999 --> 99:59:59,999
und das hat sich aber eben als falsch herausgestellt

172
99:59:59,999 --> 99:59:59,999
alos natuerlich gibt es effekte das das einfacher macht wenn ich nicht im kriegsgebiet bin

173
99:59:59,999 --> 99:59:59,999
in der entfernung zu haben auch von den opfern

174
99:59:59,999 --> 99:59:59,999
gefuehlsmaessig

175
99:59:59,999 --> 99:59:59,999
was aber der fakt ist ich habe ja gesagt da wir auch ueberwachung mit ausgeuebt mit diesen drohnen

176
99:59:59,999 --> 99:59:59,999
das die leute die das machen, die piloten, ne, mit zum teil langen schichten

177
99:59:59,999 --> 99:59:59,999
die gucken tagelang ihren zukunftigen opfern zu bevor sie diese drohnenschlaege machen

178
99:59:59,999 --> 99:59:59,999
und die haben massenhaft stresstraumata die flieger

179
99:59:59,999 --> 99:59:59,999
ne, also, das is aehm also an der stelle

180
99:59:59,999 --> 99:59:59,999
muessen wir uns auch noch dem aufwand zuwenden nicht so sauber und so einfach und geschmiert diese kriegsmaschinerie die damit errichtet wird

181
99:59:59,999 --> 99:59:59,999
ich zeig noch kurz nen paar andere drohnenmodelle

182
99:59:59,999 --> 99:59:59,999
der global hawk da kann ich gleich nochmal zum schluss reingehen, das ist das zentrale projekt gewesen wo dieses jahr drueber geredet wurde

183
99:59:59,999 --> 99:59:59,999
warum das gescheitert ist

184
99:59:59,999 --> 99:59:59,999
kann erstmal vielleicht fuer die frage was haben wir da fuer ne form von kriegsfuehrung nich so interessant

185
99:59:59,999 --> 99:59:59,999
das ist erstmal nen beschaffungsskandal gewesen

186
99:59:59,999 --> 99:59:59,999
was daran interessant ist, aber es ist ja im laufe der ermittlungen, zb eben auch durch anfragen der linksfraktion im bundestag rausgekommen

187
99:59:59,999 --> 99:59:59,999
dass die das zum zeitpunkt auch wussten wo sie trotzdem noch investiert haben und weiterentwickelt haben

188
99:59:59,999 --> 99:59:59,999
da ging es eben darum, dass man die hardware , den eigentlichen flieger als lizens kauft.

189
99:59:59,999 --> 99:59:59,999
das ueberwachungssystem aber selbst entwickeln laesst durch eads. ... heisst das

190
99:59:59,999 --> 99:59:59,999
und das ueberwachungssystem ist der zentrale wert der beschaffung gewesen ehmm.. warum den ruestungsplanern das wichtig war

191
99:59:59,999 --> 99:59:59,999
ehmm und das ding wird eben jetzt in ne andere drohne eingebaut werden

192
99:59:59,999 --> 99:59:59,999
das deswegen wurde es offensichtlich auch weiterbetrieben zum zeitpunkt als es klar war das es in der form nicht anwendbar fuer .. europaeischen luftraum

193
99:59:59,999 --> 99:59:59,999
das ist versenktes geld

194
99:59:59,999 --> 99:59:59,999
ne, aber man kann es nur so interpretieren das es darum ging diese eigene spionage ehm.. system da zu entwickeln

195
99:59:59,999 --> 99:59:59,999
das das der zentrale wunsch war

196
99:59:59,999 --> 99:59:59,999
die bundeswehr setzt diese heron drohnen ein, die sind eben aus israel gekauft oder geleased

197
99:59:59,999 --> 99:59:59,999
die werden aber nur unbewaffnet hier verwendet, weil die hersteller sowohl israelische wie auch die us hersteller idr die um die lizensen unbewaffnet also lieber verkaufen

198
99:59:59,999 --> 99:59:59,999
nur nach langwierigen verhandlungen auch bewaffnete modelle rausruecken

199
99:59:59,999 --> 99:59:59,999
und nur um das mal zu zeigen, es gibt hier auch nen helikopter der hat ne rakete ist aber eigentlich auch ein ueberwachungsgeraet
