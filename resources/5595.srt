1
00:00:11,600 --> 00:00:16,943
This is Bob and he will talk about ArduinoGuitar

2
00:00:16,943 --> 00:00:20,580
It is his project and it's about a guitar

3
00:00:20,580 --> 00:00:22,940
that has no other controls than its strings.

4
00:00:22,940 --> 00:00:22,940
And I think it's a quite interesting project.

5
00:00:22,940 --> 00:00:22,940
Thank you.

6
00:00:22,940 --> 00:00:29,540
Thank you.

7
00:00:33,646 --> 00:00:34,844
Can everyone hear me.

8
00:00:35,460 --> 00:00:39,140
So, I will talk about the project itself

9
00:00:39,140 --> 00:00:41,980
and everything that came before the project

10
00:00:41,980 --> 00:00:45,660
because I didn't know much about any of this stuff

11
00:00:45,660 --> 00:00:48,540
when I started playing with it, so I will tell you a little bit

12
00:00:48,540 --> 00:00:53,940
about myself and how this whole thing happened.

13
00:00:53,940 --> 00:00:57,740
There's a lot of things that I might mention, that you might not know about, but you can use Google, right.

14
00:00:57,740 --> 00:00:59,940
Everyone can use Google.

15
00:00:59,940 --> 00:01:02,380
Alright, so, there's two things that you might wanna know about me.

16
00:01:02,380 --> 00:01:04,380
There are two things that are really characterize me.

17
00:01:04,380 --> 00:01:07,540
I have a lot of dreams. I spend a lot of time just sitting around and dreaming.

18
00:01:07,540 --> 00:01:11,304
And I used to have very little money all my life, you know.

19
00:01:11,304 --> 00:01:12,940
Practically no money.

20
00:01:12,940 --> 00:01:16,620
So with dreams and no money you learn to scrounge. You really, you learn to scrounge.

21
00:01:16,620 --> 00:01:19,260
And when you find something you learn to fix it.

22
00:01:19,260 --> 00:01:24,383
And when you fixed it you don't want it to break anymore. You got attached to things you got.

23
00:01:24,383 --> 00:01:30,220
That's me. You develop an ability to fix anything, which I think I have today.

24
00:01:30,220 --> 00:01:35,100
Practically anything if I really want. But on the other hand once I fixed it I get attached to it and I don't wanna it to break.

25
00:01:35,100 --> 00:01:37,780
So this fear of losing it.

26
00:01:37,780 --> 00:01:44,260
You know, poverty, dreams and stuff becomes sort of a mixed blessing when you have this.

27
00:01:44,260 --> 00:01:46,980
You have stuff and then you're afraid to lose it. A song says

28
00:01:46,980 --> 00:01:48,900
'When you got nothing, you got nothing to lose'

29
00:01:48,900 --> 00:01:52,820
But it's not really true. It's when you got nothing you can not afford to lose anything.

30
00:01:52,820 --> 00:01:56,139
That's really how I looked,lived most of my life.

31
00:01:56,139 --> 00:01:58,420
And there's another aspect that I'm very tenacious.

32
00:01:58,420 --> 00:02:02,420
So I have this sort of never give up attitude.

33
99:59:59,999 --> 99:59:59,999
That's also a mixed blessing in a lot of ways.

34
99:59:59,999 --> 99:59:59,999
And one last thing about me.

35
99:59:59,999 --> 99:59:59,999
I usually like stuff that nobody else likes.

36
99:59:59,999 --> 99:59:59,999
I mean, I'm the only one who buys the one little thing in the shop that has been there on the shelf

37
99:59:59,999 --> 99:59:59,999
for hundred years and nobody wants it because they don't know what it's for.

38
99:59:59,999 --> 99:59:59,999
Because they just don't like it.

39
99:59:59,999 --> 99:59:59,999
So, this whole project started. Does this work. I guess not. How do you change slides.

40
99:59:59,999 --> 99:59:59,999
There we go. Electric guitar.

41
99:59:59,999 --> 99:59:59,999
So this whole project started. I was as a kid I always been a hippie. I'm still a hippie.

42
99:59:59,999 --> 99:59:59,999
Living in the sixties and all that. Play electric guitar, want to play the guitar,

43
99:59:59,999 --> 99:59:59,999
couldn't play the electric guitar, couldn't afford an electric guitar.

44
99:59:59,999 --> 99:59:59,999
Get a lot of junk electric guitars, have to fix them.

45
99:59:59,999 --> 99:59:59,999
Don't know how to fix them. Anyways, I had this idea about electric guitars.

46
99:59:59,999 --> 99:59:59,999
And so first what is an electric guitar really.

47
99:59:59,999 --> 99:59:59,999
It's got these things in the middle here. This is one of my guitars.

48
99:59:59,999 --> 99:59:59,999
It's a guitar that propably no one's every seen before.

49
99:59:59,999 --> 99:59:59,999
There's only two of them in Belgium.

50
99:59:59,999 --> 99:59:59,999
It's made in Canada.

51
99:59:59,999 --> 99:59:59,999
This one has two, they call them 'pickups'

52
99:59:59,999 --> 99:59:59,999
That's where it captures the vibrations of the strings to turn it into an electrical signal.

53
99:59:59,999 --> 99:59:59,999
And then here you have a switch which basically which roots the signal from the pickups to the volume and tone controls

54
99:59:59,999 --> 99:59:59,999
which are here on this guitar. This has one volume and one tone.

55
99:59:59,999 --> 99:59:59,999
And then it all goes out through the jack to the amplifier.

56
99:59:59,999 --> 99:59:59,999
It's pretty simple. I mean it seems pretty simple.

57
99:59:59,999 --> 99:59:59,999
Although it took me a long time to understand.

58
99:59:59,999 --> 99:59:59,999
So that's what a guitar is.

59
99:59:59,999 --> 99:59:59,999
Now here is what I know about electronics.

60
99:59:59,999 --> 99:59:59,999
That's it.

61
99:59:59,999 --> 99:59:59,999
You're laughing. You're laughing, but I didn't find it very funny.

62
99:59:59,999 --> 99:59:59,999
I spent a lot of time, even that, it seems so simple and it is simple, but it took me a long time to understand things like

63
99:59:59,999 --> 99:59:59,999
a voltage divider. What is this?

64
99:59:59,999 --> 99:59:59,999
I'm still not sure if I really understand.

65
99:59:59,999 --> 99:59:59,999
Anyways, so that was way I was for like thirty years of my life.

66
99:59:59,999 --> 99:59:59,999
I had guitars.

67
99:59:59,999 --> 99:59:59,999
They never worked. And I got a job, a real job, a serious job.

68
99:59:59,999 --> 99:59:59,999
And I had some money.

69
99:59:59,999 --> 99:59:59,999
And so I decided to learn to play the guitar at last. I'm an old man now.

70
99:59:59,999 --> 99:59:59,999
Old hippie.

71
99:59:59,999 --> 99:59:59,999
So I got a serious guitar. And a serious guitar requires serious adjustment.

72
99:59:59,999 --> 99:59:59,999
You know I can fix anything.

73
99:59:59,999 --> 99:59:59,999
Things that break. Things that are expensive. You don't wanna just tweak with.

74
99:59:59,999 --> 99:59:59,999
So I brought it to a luthier, a guitar shop.

75
99:59:59,999 --> 99:59:59,999
Luthier is a fancy word for guitar maker.

76
99:59:59,999 --> 99:59:59,999
And these guys adjusted the guitar and I learned a lot from these guys about guitars,

77
99:59:59,999 --> 99:59:59,999
about technology. And I learned that basically everybody wants the same thing that Jimmy Hendrix played

78
99:59:59,999 --> 99:59:59,999
That's all there is. The whole music industry is based on Jimmy Hendrix played a left handed stratocaster

79
99:59:59,999 --> 99:59:59,999
Not everybody is left-handed, but everybody wants a stratocaster.

80
99:59:59,999 --> 99:59:59,999
So, that's it.

81
99:59:59,999 --> 99:59:59,999
That's the number one selling guitar.

82
99:59:59,999 --> 99:59:59,999
And that's it. The technology hasn't changed. 1950s. They invented all the stuff

83
99:59:59,999 --> 99:59:59,999
and it hasn't changed. And nobody wants it to change

84
99:59:59,999 --> 99:59:59,999
because everybody wants to be Jimmy Hendrix.

85
99:59:59,999 --> 99:59:59,999
And the mechanical potentiometer and the capacitor and a little mechanical switch. That's it.

86
99:59:59,999 --> 99:59:59,999
That was from the 1950s and it hasn't changed.

87
99:59:59,999 --> 99:59:59,999
And even now a guitar is in five figure-prices, I'm saying a twenty thousand or a thirty thousand

88
99:59:59,999 --> 99:59:59,999
dollar guitar. It's still the same guts inside. Nothing special.

89
99:59:59,999 --> 99:59:59,999
Ok, maybe the pickups are little bit more sophisticated. Maybe the wiring is really poorly put together.

90
99:59:59,999 --> 99:59:59,999
There are twenty thousand dollar guitars that looks like they were soldered by a child.

91
99:59:59,999 --> 99:59:59,999
And I've seen it. I wouldn't believe it because on one point I was thinking about buying one of those

92
99:59:59,999 --> 99:59:59,999
at a special price but when I saw the guts I was like 'Forget it, forget it!'.

93
99:59:59,999 --> 99:59:59,999
So, you know the 60s. I love the 60s, because of hippies and because of peace and love

94
99:59:59,999 --> 99:59:59,999
and all that kind of stuff. It was great and I, for long time I regret it, but then came along Internet

95
99:59:59,999 --> 99:59:59,999
and the 21st century and I found the 21st century is not too bad either.

96
99:59:59,999 --> 99:59:59,999
And in fact it's really cool, especially the long tail. Living on the long tail is a cool place to live.

97
99:59:59,999 --> 99:59:59,999
And everybody here, we are all on the long tail whether we know it or not.

98
99:59:59,999 --> 99:59:59,999
So, talking to these guys and I started thinking why has the guitar has to be so old fashioned.

99
99:59:59,999 --> 99:59:59,999
Why can't it be in 21st century, you know.

100
99:59:59,999 --> 99:59:59,999
Why does it have to have these mechanical things and you know there is all kinds of stuff.

101
99:59:59,999 --> 99:59:59,999
For example the guitar I showed you. It does some cool stuff. It's not just an ordinary guitar.

102
99:59:59,999 --> 99:59:59,999
It does some pretty cool stuff.

103
99:59:59,999 --> 99:59:59,999
So, basically you have you know the two pickups you have either one or the other or both.

104
99:59:59,999 --> 99:59:59,999
So, both in parallel, both in series. Series is already an advance, I mean this only something that became

105
99:59:59,999 --> 99:59:59,999
imagined later, much later.

106
99:59:59,999 --> 99:59:59,999
But this guitar has all of that. And those are very, very sophisticated pickups, which were only invented I guess

107
99:59:59,999 --> 99:59:59,999
ten, twelve years ago.

108
99:59:59,999 --> 99:59:59,999
They are very sophisticated. It's a very sophisticated guitar.

109
99:59:59,999 --> 99:59:59,999
But it offers both the neck, the bridge, the neck is the top one, the bridge is the bottom one,

110
99:59:59,999 --> 99:59:59,999
so you can have the neck, you can have the bridge, you can have the neck and the bridge is series

111
99:59:59,999 --> 99:59:59,999
the neck and bridge in parallel. So that's already pretty sophisticated for a musician.

112
99:59:59,999 --> 99:59:59,999
It's already incomprehensible for most musicians.

113
99:59:59,999 --> 99:59:59,999
My guitar teacher when I tell him he says 'What do you mean, what are you talking about?'

114
99:59:59,999 --> 99:59:59,999
See, well, I was just saying 'Jazz and Rock' - 'Oh, I get it, I get it...'

115
99:59:59,999 --> 99:59:59,999
So, but even that, so what does that mean? That means, you have this little switch has four positions.

116
99:59:59,999 --> 99:59:59,999
There is one for each thing. And if you want to start turning things on, turning things off.

117
99:59:59,999 --> 99:59:59,999
Everything you need the mechanical switch and you want to have individual tone controls, individual volume controls.

118
99:59:59,999 --> 99:59:59,999
Everything means more mechanical stuff. And pretty soon you have your guitar just covered with buttons.

119
99:59:59,999 --> 99:59:59,999
And nobody knows how it works. Even yourself. Even if you built it yourself.

120
99:59:59,999 --> 99:59:59,999
You still can't rememeber which button to push to get this sound. It becomes just a mess.

121
99:59:59,999 --> 99:59:59,999
So, I thought, my vision was lets get rid of all this electromechanical crap and put a screen on it or

122
99:59:59,999 --> 99:59:59,999
make it have a curcuit in there and have it a screen where you can have it synthetic and you can do all kinds of stuff.

123
99:59:59,999 --> 99:59:59,999
So, why not, why can't that be.

124
99:59:59,999 --> 99:59:59,999
So that was my vision. It was to have some kind of curcuit on an Android phone an Bluetooth and in there

125
99:59:59,999 --> 99:59:59,999
In there could some kind of thing, I don't know what, an Adruino it turned out to be.

126
99:59:59,999 --> 99:59:59,999
That was my idea and it seemed easy enough. Modern technology and and all that stuff.

127
99:59:59,999 --> 99:59:59,999
[Vehicles, IR], it's good enough.

128
99:59:59,999 --> 99:59:59,999
And why didn't it already exist? Well, I have a lot of friends or friends of friends in the music industry.

129
99:59:59,999 --> 99:59:59,999
And they all came up with the same answers.

130
99:59:59,999 --> 99:59:59,999
They is two reasons. First of all it's just unbelievably complicated. You need a team of engineers.

131
99:59:59,999 --> 99:59:59,999
Hundreds of years of development and it's just really complicated. And this was from professionals, from

132
99:59:59,999 --> 99:59:59,999
engineers in the music industry. This is not just from musicians.

133
99:59:59,999 --> 99:59:59,999
And then the other thing was just nobody wants it. It's just ridiculous.

134
99:59:59,999 --> 99:59:59,999
Everybody wants to have a knob and a switch. That's what musicians want. That's it.

135
99:59:59,999 --> 99:59:59,999
That's all there is. So just don't waste your time.

136
99:59:59,999 --> 99:59:59,999
I didn't really waste a lot of my time, but I kept thinking about it.

137
99:59:59,999 --> 99:59:59,999
And since I didn't know anything about electronics it was kind of a handicap.

138
99:59:59,999 --> 99:59:59,999
You know, what to do about this and how to make progress. So this idea just bang around in my head for a while.

139
99:59:59,999 --> 99:59:59,999
And then nothing happened. And then I used to go to a lot of TED conf, I still do go to TED conferences in Brussels,

140
99:59:59,999 --> 99:59:59,999
I live in Brussels and they got the TEDx there.

141
99:59:59,999 --> 99:59:59,999
And you get a lot of inspirational guys talking and I went to one of these.

142
99:59:59,999 --> 99:59:59,999
I had a good time with that and I was thinking, you know, is it really impossible to make this?

143
99:59:59,999 --> 99:59:59,999
Or is it just people are scared of it? Let me think about Marshall McLuhan.

144
99:59:59,999 --> 99:59:59,999
Does anybody know here who Marshall McLuhan is? One person? That's it? Two? Three? Four? Five?

145
99:59:59,999 --> 99:59:59,999
Marshall McLuhan was a great thinker, he thought about mass media in the early 50s/60s and he

146
99:59:59,999 --> 99:59:59,999
predicted Internet in 1962. He predicted the arrival of Internet will be a great thing.

147
99:59:59,999 --> 99:59:59,999
He said 'In this new age the human kind will move from individualism and fragmentation to a collective

148
99:59:59,999 --> 99:59:59,999
identity. A global village.' So invented the phrase 'global village'.

149
99:59:59,999 --> 99:59:59,999
But he wasn't thinking about computers. He was thinking about the electrical network that ran

150
99:59:59,999 --> 99:59:59,999
throughout the world. He tought that was going to bring the entire planet together like electricity and the net, the electrical net.

151
99:59:59,999 --> 99:59:59,999
He was a cool guy. And he also thought about a lot of cool things like why inovation is so hard?

152
99:59:59,999 --> 99:59:59,999
And he said this second part, which is really [a proporto] to my project and probably to a lot of

153
99:59:59,999 --> 99:59:59,999
things people do here is that

154
99:59:59,999 --> 99:59:59,999
Whenever you look at something new all you references are in the past, so look at something new you just think

155
99:59:59,999 --> 99:59:59,999
'Well, is it like this or is it like that?'

156
99:59:59,999 --> 99:59:59,999
So, a Mac is like Windows, right?

157
99:59:59,999 --> 99:59:59,999
Macintosh is like Windows?

158
99:59:59,999 --> 99:59:59,999
Everything is referring to the immediate past so that makes it hard to understand something that is

159
99:59:59,999 --> 99:59:59,999
radically new. And it puts people off in general from things that are radically new.

160
99:59:59,999 --> 99:59:59,999
So, I was thinking all this people have told me that nobody wants this stuff and it's impossible to do.

161
99:59:59,999 --> 99:59:59,999
Is that just fear and misunderstanding or is there more to it?

162
99:59:59,999 --> 99:59:59,999
But I had no way of knowing what that meant what was going to happen.

163
99:59:59,999 --> 99:59:59,999
I always think of this story, the Henry Ford story. Which is, he was interviewed.

164
99:59:59,999 --> 99:59:59,999
I don't think this has acutally happened, but he was interviewed and was asked

165
99:59:59,999 --> 99:59:59,999
'How did you know people would like your car? Did you go and ask them before? Use your survey?'

166
99:59:59,999 --> 99:59:59,999
And he said 'If I had asked people what they wanted, they would have said faster horses.' And, you know,

167
99:59:59,999 --> 99:59:59,999
he wasn't interested in making faster horses.

168
99:59:59,999 --> 99:59:59,999
So, all this things were going on in my mind and then I went to last years TEDx in Brussels and Mitch Altman was speaking there.

169
99:59:59,999 --> 99:59:59,999
Is Mitch by any chance in the room? No, well, he was there and he gave a very inspiring speach about

170
99:59:59,999 --> 99:59:59,999
hackerspaces and all that kind of stuff.

171
99:59:59,999 --> 99:59:59,999
So, I nearly jumped on him because he's an electronics guy and I said 'Here's my project.'

172
99:59:59,999 --> 99:59:59,999
'What do you think?' - 'Of course, of course, it's so easy. Of course, you could do it. It's a great idea.'

173
99:59:59,999 --> 99:59:59,999
'Go and do it!'

174
99:59:59,999 --> 99:59:59,999
That was enough, one guy. Never seen before in my life, convinced me that I should do it.

175
99:59:59,999 --> 99:59:59,999
Fine. All these experts told me 'Forget it, forget it, forget it.'

176
99:59:59,999 --> 99:59:59,999
And so, it's a shame he's not in the room, but he gets a slide.

177
99:59:59,999 --> 99:59:59,999
So that's you know, he hook me. He said contact the hackerspace community.

178
99:59:59,999 --> 99:59:59,999
I didn't even know there was one in Brussels. I mean, 'Brussels'.

179
99:59:59,999 --> 99:59:59,999
Anyway, there's a huge hackerspace community in Belgium and Brussels and I was amazed about this.

180
99:59:59,999 --> 99:59:59,999
So that's the meta part of the project. Thank you.

181
99:59:59,999 --> 99:59:59,999
Now we can talk about the technology and the stuff which is maybe interesting maybe not.

182
99:59:59,999 --> 99:59:59,999
So, that's a potentiometer, right?

183
99:59:59,999 --> 99:59:59,999
Most people know what it is. You got the thing you turn and it moves the wiper, the 'W' up or down.

184
99:59:59,999 --> 99:59:59,999
And means that means, that one resistance gets bigger and one get smaller, right?

185
99:59:59,999 --> 99:59:59,999
It's really simple. So, then we are having an analogue signal, a sinusoid, coming from the pickups.

186
99:59:59,999 --> 99:59:59,999
The question was how could you make that controllable digitally, somehow.

187
99:59:59,999 --> 99:59:59,999
I thought about putting servos, motors all kind of stupid things. I had lot of stupid ideas.

188
99:59:59,999 --> 99:59:59,999
But I put this out on a hackerspace mailing list and people had all kinds of answers,

189
99:59:59,999 --> 99:59:59,999
but it all seemed to be like they were, it wasn't criticism, but they didn't know what I wanted.

190
99:59:59,999 --> 99:59:59,999
And I felt like there was real some kind of, you know, did you see 'Cool Hand Luke'?

191
99:59:59,999 --> 99:59:59,999
'What we got here is failure to communicate.' And then they whack him over the head. Great movie.

192
99:59:59,999 --> 99:59:59,999
Paul Newmans masterpiece.

193
99:59:59,999 --> 99:59:59,999
Anyway, it was a failure to communicate and that is something I learnt a lot in this project.

194
99:59:59,999 --> 99:59:59,999
You ask questions and they make perfect sense to you, but nobody else can understand what you are

195
99:59:59,999 --> 99:59:59,999
talking about. You ask a question in a forum, you say 'I got this thing and this thing.'

196
99:59:59,999 --> 99:59:59,999
And people don't answer or they ask you with crazy things. And you realize, that they don't understand.

197
99:59:59,999 --> 99:59:59,999
And they did not understand.

198
99:59:59,999 --> 99:59:59,999
So, finally though after a lot of questions being tenacious, you remember tenacity, we got to an

199
99:59:59,999 --> 99:59:59,999
answer by Johannes Tallman who's a brilliant scientist. Is he not here either?

200
99:59:59,999 --> 99:59:59,999
Well, he's in the [?] space. And he just said 'Why don't you do it with a LED LDR?'

201
99:59:59,999 --> 99:59:59,999
I didn't know if that was a word or if that was acronyms. I said 'Ok, great. Tell me more about was that is.'

202
99:59:59,999 --> 99:59:59,999
So, actually a LED LDR is really a LED, a light, and a light depended resistor.

203
99:59:59,999 --> 99:59:59,999
So you have something that shines light and then you have something that changes resistance when you shine light on it.

204
99:59:59,999 --> 99:59:59,999
And you put those two together and that makes a digitally controllable resistance.

205
99:59:59,999 --> 99:59:59,999
Because you can shine light more or less. You can pulse it with PWM. So now you have a

206
99:59:59,999 --> 99:59:59,999
digitally controllable resistance. And if you put two of them together like in a picture you get something very

207
99:59:59,999 --> 99:59:59,999
similar to a potentiometer. And all you have to do is manage the current. And in the mean time I've been

208
99:59:59,999 --> 99:59:59,999
playing with, I got get inside with Arduino, I saw Banzi on TED and I got the book and I bought an Arduino

209
99:59:59,999 --> 99:59:59,999
and I could make the lights blink and it was great, everything was great. All the problems were solved.

210
99:59:59,999 --> 99:59:59,999
It seems so simple. Cause like. 'What's wrong with you people? This is so easy. This is so unbelivably easy.'

211
99:59:59,999 --> 99:59:59,999
So, I started doing it, but somebody on the mailing list said 'You know', I quote this

212
99:59:59,999 --> 99:59:59,999
'You certainly worked to some extend and even if the result is to noisy or bad quality to be used in music

213
99:59:59,999 --> 99:59:59,999
it will still be huge fun to make.' And I thought to myself 'What? Noisy? Where is noise coming from?'

214
99:59:59,999 --> 99:59:59,999
'How can? Vehicles IR! Where is noise in that equation? I don't get it.'

215
99:59:59,999 --> 99:59:59,999
But that was kind of staying in the back of my mind, you know, who cares?

216
99:59:59,999 --> 99:59:59,999
Went on. By the way, LED LDR is, actually in the industry they're called 'Vactrols'.

217
99:59:59,999 --> 99:59:59,999
There is actual a component, that has the whole thing build in one and they are made by different companys.

218
99:59:59,999 --> 99:59:59,999
The best company is Perkin Elmer. If you buy them just put my name in the [?] I get them on my commission, ok.

219
99:59:59,999 --> 99:59:59,999
So, that was easy. So now I had the fundamental problems solved and I just had to learn how to do it,

220
99:59:59,999 --> 99:59:59,999
I read 'Making things talk' by Tom Igoe, later corresponded with and found out he didn't test

221
99:59:59,999 --> 99:59:59,999
all the code in his book. So here it is. So the idea was really you have a guitar, an Arduino in there,

222
99:59:59,999 --> 99:59:59,999
a bunch of vactrols and then you have some kind of Bluetooth and some kind of device, a computer or

223
99:59:59,999 --> 99:59:59,999
Android. I've never seen an Android phone in my life at the time, but I figured that would do it and it will be really easy.

224
99:59:59,999 --> 99:59:59,999
Yeah and it was. So here is the really geeky part. This the schematic. I only made it because the person

225
99:59:59,999 --> 99:59:59,999
revising my presentation here told me to make it more technical because this is a technical conference.

226
99:59:59,999 --> 99:59:59,999
Alright, so here is the technical stuff. You wanted it, you get it.

227
99:59:59,999 --> 99:59:59,999
In the upper left, there, you can barely see it, but there is three, I put three pickups, this guitar has

228
99:59:59,999 --> 99:59:59,999
three pickups and you see there is one sort of double pickup, which is exactly what it is.

229
99:59:59,999 --> 99:59:59,999
So this three pickups. They come in.
