1
99:59:59,999 --> 99:59:59,999
Hi everyone. Thank you all for coming here at this late hour of the day.

2
99:59:59,999 --> 99:59:59,999
My name is Dal Diskin and as was said, I will be presenting virtualization security.

3
99:59:59,999 --> 99:59:59,999
The purpose of my talk is to introduce the low level mechanism that is related to the infrastructure that makes virtualization secure.

4
99:59:59,999 --> 99:59:59,999
and basically allows us to use virtualization securely.

5
99:59:59,999 --> 99:59:59,999
So who am I and why am I talking about this?

6
99:59:59,999 --> 99:59:59,999
The key point that is related to virtualization here is that I used to work eight years for Intel doing a lot of the infrastructure work on virtualization security.

7
99:59:59,999 --> 99:59:59,999
So lets move on.

8
99:59:59,999 --> 99:59:59,999
I want first to give a few thanks. I want to thank my wife xxx who has been taking good care of me while I'm doing the research.

9
99:59:59,999 --> 99:59:59,999
I want to thank everyone at Cyvera and especially the people that reviewed the talk for me.

10
99:59:59,999 --> 99:59:59,999
And all the security people at Intel, greeting to you all

11
99:59:59,999 --> 99:59:59,999
You know how it is now from the other side.

12
99:59:59,999 --> 99:59:59,999
So, what am I going to talk about today.

13
99:59:59,999 --> 99:59:59,999
I am going to talk about hardware assisted virtualization.

14
99:59:59,999 --> 99:59:59,999
I am going to talk about the software stacks in different virtualization approaces and weaknesses of each approach.

15
99:59:59,999 --> 99:59:59,999
I am going to talk about the complexity of memory management and related weaknesses that it affects on virtualization

16
99:59:59,999 --> 99:59:59,999
and finally, I am going to talk about the computer platform internals and why those lead to certain weaknesses in the implementation of virtualization platforms

17
99:59:59,999 --> 99:59:59,999
and there is a special bonus that I may show, a small potential VM escape that might work and might not.

18
99:59:59,999 --> 99:59:59,999
no demo, sorry

19
99:59:59,999 --> 99:59:59,999
I kind of broke down my computer when I tried to do it.

20
99:59:59,999 --> 99:59:59,999
anyway, so what is virtualization

21
99:59:59,999 --> 99:59:59,999
in the context of this talk, virtualization is replacing the real hardware that you have,

22
99:59:59,999 --> 99:59:59,999
the CPU and the computer platform with a virtual environment

23
99:59:59,999 --> 99:59:59,999
historically, we can trace the roots of virtualization even to Alan Turing and the universal turing machine concept

24
99:59:59,999 --> 99:59:59,999
which was basically a machine that could emulate any other machine.

25
99:59:59,999 --> 99:59:59,999
After that in the late 70s, xxx, came up with virtualization requirements for computer architectures.

26
99:59:59,999 --> 99:59:59,999
So that's what we start from, a bit of terminology just so that we are all speaking the same language.

27
99:59:59,999 --> 99:59:59,999
A virtual machine manager or VMM is software virtualizing privileges instructions and the hardware.

28
99:59:59,999 --> 99:59:59,999
Basically, this is the core of the virtualization system. A virtual machine is the software stack running under a VMM.

29
99:59:59,999 --> 99:59:59,999
A guest operating system is the operating system of the virtual machine and host operating system is the operating system that is controlling the VMM.

30
99:59:59,999 --> 99:59:59,999
And root operating is when you execute within the VMM.

31
99:59:59,999 --> 99:59:59,999
So the first question we need to ask ourself

32
99:59:59,999 --> 99:59:59,999
"This guy is talk to us about secure virtualization but there are many definitions of secure so what is secure virtualization?"

33
99:59:59,999 --> 99:59:59,999
The security goals

34
99:59:59,999 --> 99:59:59,999
We need to define what are our security goals when we want to secure our virtualization platform

35
99:59:59,999 --> 99:59:59,999
So first goal is preventing modification of the host operating system by guests. This makes sense.

36
99:59:59,999 --> 99:59:59,999
And natually also the VMM. This has to be in order for this to even work.

37
99:59:59,999 --> 99:59:59,999
You need to prevent one guest from changing another guest

38
99:59:59,999 --> 99:59:59,999
if you have two virtual machines I don't want one to attack the other or vice versa.

39
99:59:59,999 --> 99:59:59,999
You need to prevent the guest hardware, the virtual machine, from actually subverting the hardware or firmware of the system.

40
99:59:59,999 --> 99:59:59,999
You need to prevent the guest from stealing date from other guests or from the host operating system or from the VMM.

41
99:59:59,999 --> 99:59:59,999
You need to prevent denial of service by guest OS getting an unfair share of resources relative to other guests.

42
99:59:59,999 --> 99:59:59,999
And you need to keep the guest OS secure, you don't want to downgrade the normal security mechanisms of the guest operating system.

43
99:59:59,999 --> 99:59:59,999
Now, this may not be equal in every platform

44
99:59:59,999 --> 99:59:59,999
for example .. virtualization usually gives access to the guest directly to the hardware

45
99:59:59,999 --> 99:59:59,999
which is a very bad idea in security terms but this is what they do because they want the virtualization capabilities

46
99:59:59,999 --> 99:59:59,999
for monitoring
