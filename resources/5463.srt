1
00:00:08,567 --> 00:00:11,210
<i>applaus</i>

2
00:00:12,405 --> 00:00:16,065
First I need to apologize for typesetting this in Open Office.

3
00:00:16,065 --> 00:00:22,972
I know that the text looks like a ransome note, but that's what's happens when you don't use Latex.

4
00:00:22,972 --> 00:00:27,435
I I'd also like to give a shoutout call if Mallnarf? is here

5
00:00:27,435 --> 00:00:35,814
and our dinosaur rock band. We are a Christian rock band - we are called Jesus lives in the ISS

6
00:00:38,634 --> 00:00:47,404
and we know he's always watching us but we think that it's easier for him to hear our prayers

7
00:00:47,404 --> 00:00:51,611
when his you know in an orbit that the passes over us

8
00:00:51,611 --> 00:00:54,863
so we need is orbital tracking know when to pray

9
00:00:57,773 --> 00:01:01,867
as I'm shure you can gess that we are not recognized as a legal minority religion in Germany

10
00:01:04,117 --> 00:01:08,147
I also like to thank Skytee and Fabienne

11
00:01:08,147 --> 00:01:14,779
and Adami Lori and Jim for some prior satellite tracking work

12
00:01:14,779 --> 00:01:19,189
and the skuby crew at Dartmouth College

13
00:01:19,189 --> 00:01:21,708
for all sorts of fun whenever I bounce out there

14
00:01:21,708 --> 00:01:26,629
This is the mission patch of the southern appalachians space agency

15
00:01:26,629 --> 00:01:34,034
<i>applaus</i>

16
00:01:34,034 --> 00:01:38,410
This was drawn by Scot Biben and there are a few pieces of my people's native culture that I need to point out here.

17
00:01:38,410 --> 00:01:44,798
On the right the little dinosaur type with thing with it's finger going out.

18
00:01:44,798 --> 00:01:48,463
You might call him E.T. but we call this things buggers,

19
00:01:48,869 --> 00:01:50,974
they're like this tall and they're green

20
00:01:50,984 --> 00:01:54,090
and that's why the man on the left has a shotgun.

21
00:01:55,720 --> 00:01:57,713
Because he doesn't want to be inducted.

22
00:01:57,713 --> 00:02:01,713
Is's got satellite dish in the middle

23
00:02:01,713 --> 00:02:18,474


24
00:02:18,474 --> 00:02:23,576
It looks like there are snowpeaks, but our mountains aren't high enough to have snow

25
00:02:29,210 --> 00:02:30,556
..

26
00:02:32,856 --> 00:02:37,176
in the top in space you can see the ISS and you can see a banana

27
00:02:48,440 --> 00:02:50,017
this is to signify space trash. You know' it's symbolism that matters in these things.

28
00:02:50,017 --> 00:02:52,464


29
00:02:52,464 --> 00:02:58,148
In Berlinsides in 2012, I did a talk about reverse engineering the spot connect.

30
00:02:58,148 --> 00:03:07,768
The spot connect is a litte hockey pot type thing – this is what it looks like

31
00:03:07,768 --> 00:03:14,095
These things are great, it weighs a bit more than your phone, it runs off a batteries,

32
00:03:14,095 --> 00:03:20,611
It connects to your phone via bluetooth. Originally these were emergency locator beacons.

33
00:03:20,611 --> 00:03:27,127
Has any body seen the movie where the guy has to cut off his arm with a dull knife

34
00:03:27,127 --> 00:03:33,645
If you're hiking and you don't want the same experience, you buy one of these things,

35
99:59:59,999 --> 99:59:59,999
there's an emergency button you can push that transmits your GPS coordinates by satellite to rescue workers

36
99:59:59,999 --> 99:59:59,999
but that was boring so they had to add social media. So in addition to keeping you from chewing of your own arm

37
00:03:34,009 --> 00:03:38,009
this device will also allow you to tweet and make facebook posts.

38
99:59:59,999 --> 99:59:59,999
The idea is as you're running – here I'm crossing the Schuylkill River in Philadelphia and the Android phone the left is making a post

39
99:59:59,999 --> 99:59:59,999
and i did an article on reverse engineering the bluetooth side of these things

40
99:59:59,999 --> 99:59:59,999
because I use a weird brand of phones that Microsoft decided to cut off and I'm quite bitter about it.

41
99:59:59,999 --> 99:59:59,999
But I also figured out the physical layer and that's to this diagram shows.

42
99:59:59,999 --> 99:59:59,999
This transmits it 1.126125 GHz and it send a pseudo random stream so each one of the zeros

43
99:59:59,999 --> 99:59:59,999
is along chunk where it's bouncing back and forth between two different frequencies

44
99:59:59,999 --> 99:59:59,999
The same for the ones, but the way that the pattern works, is that it switches the signal whenever it is going from 0 to 1.

45
99:59:59,999 --> 99:59:59,999
Internally there these little pops that you can actually identify on a software defined radio recording

46
99:59:59,999 --> 99:59:59,999
This is how you can reverse engineer the signal that the Spot Connect sends up to the satellite network.

47
99:59:59,999 --> 99:59:59,999
Everything is clear text on this and it's completely unencrypted.

48
99:59:59,999 --> 99:59:59,999
It just has your gym you serial number, your GPS coordinates and a bit as of ASCII text

49
99:59:59,999 --> 99:59:59,999
If you listen on this frequenzy and have the correct recording software

50
99:59:59,999 --> 99:59:59,999
you can actually watch all the spot connect messages that are transmitting up from your location

51
99:59:59,999 --> 99:59:59,999
this be great except that this is designed for hiking in areas where there's no cell phone service

52
99:59:59,999 --> 99:59:59,999
So having an antenna on the uplink freq is kind of useless.

53
99:59:59,999 --> 99:59:59,999
you know you actually have to go out to a national park find some guy who is about to chew his arm and

54
99:59:59,999 --> 99:59:59,999
then you could listen to his uplink where he is liked reading hey I'm gonna chew my arm of you know <i>laughing</i>

55
99:59:59,999 --> 99:59:59,999
So that's great as a proof of concept, but it's not really anything practical.

56
99:59:59,999 --> 99:59:59,999
the current stated that was that I knew the protocol and I could sniff the uplinks, but I wanted to sniff the downlinks.

57
99:59:59,999 --> 99:59:59,999
It's easy to get the thing that goes up to the satellite, but I wanted to get that what comes down from the satellite.

58
99:59:59,999 --> 99:59:59,999
and that requires a a satellite dish

59
99:59:59,999 --> 99:59:59,999
but a geostationary dish isn't good enough

60
99:59:59,999 --> 99:59:59,999
because the satellites that run this network there are a lot of them

61
99:59:59,999 --> 99:59:59,999
– it's called the Globalstar network –

62
99:59:59,999 --> 99:59:59,999
They fly really low across the earth, and they fly on very tight and fast orbits

63
99:59:59,999 --> 99:59:59,999
they move from horizon to horizon in 15 to 20 minutes

64
99:59:59,999 --> 99:59:59,999
which means that you either need like a sweatshop army of kids trying aim the satellite dishes is going across

65
99:59:59,999 --> 99:59:59,999
or you have to make it computer controlled.

66
99:59:59,999 --> 99:59:59,999
Stepping back from the SC a little, Adam laurie made some work on geostationary satellites

67
99:59:59,999 --> 99:59:59,999
that stay in one position in the sky

68
99:59:59,999 --> 99:59:59,999
he gave two sets of talks – one in 2008 and the second in 2010

69
99:59:59,999 --> 99:59:59,999
He used a DVB-S card connected to a sat dish with a diseqc motor

70
99:59:59,999 --> 99:59:59,999
so they could move the satellite dish in order to scan a region of the horizon.

71
99:59:59,999 --> 99:59:59,999
His tool is publicly available at satmap you can grab it at this URL

72
99:59:59,999 --> 99:59:59,999
And then after he finds a signal, he has a feed scannner.

73
99:59:59,999 --> 99:59:59,999
Normally when you have sat TV, you provider gives you a listing of the frequencies

74
99:59:59,999 --> 99:59:59,999
and your provider gives you an exact orbital position to aim your satellite dish at

75
99:59:59,999 --> 99:59:59,999
But adam's tool allows you to scan to see which frequencys are in use

76
99:59:59,999 --> 99:59:59,999
and which protocols are in use once you've correctly aimed your dish

77
99:59:59,999 --> 99:59:59,999
he also describes a technique from moving your dish left and right while doing this in order to identify where the satellites are

78
99:59:59,999 --> 99:59:59,999
This recording here is from reimplementation and I made as Adams work in order to catch up with it

79
99:59:59,999 --> 99:59:59,999
In this diagram the x-axis shows the azimuth, this shows how much left or right my sat dish has moved.

80
99:59:59,999 --> 99:59:59,999
the y-axis shows the frequency and all these dots are strong signals

81
99:59:59,999 --> 99:59:59,999
Every vertical bar in which you see chunks of frequencies, that's a satellite.

82
99:59:59,999 --> 99:59:59,999
but the stay in the same position so it's easy for me to repeat this experiment its easy for me to rerun it

83
99:59:59,999 --> 99:59:59,999
and to find the same satellites in the same position. It's easy to debug this.

84
99:59:59,999 --> 99:59:59,999
But it can't move in elevation. This diagram is just a small slice of the sky.

85
99:59:59,999 --> 99:59:59,999
We're looking at a single line maybe 10 degrees across. Maybe only five degrees across.

86
99:59:59,999 --> 99:59:59,999
Hacking KU-band – the television satellites – has the advantage that you can use cheap standardized hardware.

87
99:59:59,999 --> 99:59:59,999
I bought one one of this DVB-S cards in Mauerpark, Berlin for 3 euros.

88
99:59:59,999 --> 99:59:59,999
You can use standardized disecq motors, you can but them at a satellite TV shop.

89
99:59:59,999 --> 99:59:59,999
TV signals come with video feeds, so you can actually see pictures.

90
99:59:59,999 --> 99:59:59,999
There was a scandal a couple of years ago, where you could actually see drone feeds bouncing off satellites.

91
99:59:59,999 --> 99:59:59,999
I in the the nineties it was very popular to listen to the sorta unedited sections of interviews

92
99:59:59,999 --> 99:59:59,999
when people would be interviewed over a satellite before Skype and such things became options

93
99:59:59,999 --> 99:59:59,999
and and the there also networking signals here using TCP IP packets

94
99:59:59,999 --> 99:59:59,999
So you can actually turn your DVB-S card as promiscuous ethernet adapter.

95
99:59:59,999 --> 99:59:59,999
and start sniffing all the traffic that comes across

96
99:59:59,999 --> 99:59:59,999
this is also a great way to get free down link bandwidth because you can just fled packets at an address

97
99:59:59,999 --> 99:59:59,999
that you know will be routed to you or several addresses and then used if it out as the legitimate receiver ignores them

98
99:59:59,999 --> 99:59:59,999
But it also has some disadavntages. It only works with geostationary satellites. If the satellite moves, you can't track it.

99
99:59:59,999 --> 99:59:59,999
you dish awesome is very slowly and it only moves left and right it won't move up and down

100
99:59:59,999 --> 99:59:59,999
You're limited on standardized signals. While it's great that you get video and his TCP/IP

101
99:59:59,999 --> 99:59:59,999
you're never going to get anything weird – you not gonna get any the mobile data

102
99:59:59,999 --> 99:59:59,999
are you not going to get brazilian truck-drivers – we will get to those in a bit

103
99:59:59,999 --> 99:59:59,999
I misspoke, you will actually get brazilian truck-drivers in this.

104
99:59:59,999 --> 99:59:59,999
I bought a satellite dish – one of the best things about living in america is that you can buy

105
99:59:59,999 --> 99:59:59,999
an industrial hardware cheap as dirt on e-bay

106
99:59:59,999 --> 99:59:59,999
I know things are likely used to be XXXX in human children anymore

107
99:59:59,999 --> 99:59:59,999
This sat dish here on the left – the one in the radome – that's my dish.

108
99:59:59,999 --> 99:59:59,999
And to the right, that's the boat it came from. <i>applaus</i>

109
99:59:59,999 --> 99:59:59,999
This came from a military ship. But the dish itself is also available to civilians for very large yachts.

110
99:59:59,999 --> 99:59:59,999
the dish itself as a fellcom 81 and it was intended for use with the network called in Inmarsat

111
99:59:59,999 --> 99:59:59,999
Imarsat allows for telephone connections and also data connections when you're on a boat.

112
99:59:59,999 --> 99:59:59,999
If the crew wants to call home or wants to go to AOL keywords

113
99:59:59,999 --> 99:59:59,999
or whatever was popular back when this was common they could do that

114
99:59:59,999 --> 99:59:59,999
Teh dish was desgined to be at the very top of a ships' mast.

115
99:59:59,999 --> 99:59:59,999
The reason why is that at the top of the mast there aren't any obstructions – it has a clear view of the sky in all directions.

116
99:59:59,999 --> 99:59:59,999
But there's a complication for being on the top of the mast.

117
99:59:59,999 --> 99:59:59,999
which is that the ship is rocking beneath you and you're moving more than the rest the ship

118
99:59:59,999 --> 99:59:59,999
So they have stepper-motors for azimuth elevation and tilt and they have spinning gyroscopes.

119
99:59:59,999 --> 99:59:59,999
back before the iPhone there is this dark dark time whens gyroscopes actually spun

120
99:59:59,999 --> 99:59:59,999
this is the sort of gyros that it has – actually four of them so that it can measure its movement

121
99:59:59,999 --> 99:59:59,999
and than it has a control computer. The idea is that the dish itself can be moved while remaining absolutely stable with regard to the gyroscopes

122
99:59:59,999 --> 99:59:59,999
it compensates for the rocking of the ship beneath it as it's targeting a stationary satellite

123
99:59:59,999 --> 99:59:59,999
In america this costs two 250 dollars

124
99:59:59,999 --> 99:59:59,999
but its electronics equipment so while you think that would only be a 180 euro it's more like 2500

125
99:59:59,999 --> 99:59:59,999
that's before import duties and it being impounded

126
99:59:59,999 --> 99:59:59,999
we also have this lovely culture in which people love excuses to use their trucks so the guy that I but as from offered to deliver it to my home from the two hundred dollars it was an 11 hours drive

127
99:59:59,999 --> 99:59:59,999
But if you wanted this, you'd have to carry this in your carry-on luggage and it could be awkward.

128
99:59:59,999 --> 99:59:59,999
I got this dish and I decided I had to do something with it site created the southern appalachians space agency

129
99:59:59,999 --> 99:59:59,999
I'm from the state of Tennessee formerly known as the State of Franklin and till north carolina invaded us

130
99:59:59,999 --> 99:59:59,999
It's ok, I know europeans suck at history. <i>laughing</i>

131
99:59:59,999 --> 99:59:59,999
now I'm trying to think you've had a show you on a map for Tennessee is without having a map but gonna its okay in a sec a jogger finisher get it send

132
99:59:59,999 --> 99:59:59,999
Texas is our first colony but it's actually a decent drive to the east dewey's you don't actually have to go anyways

133
99:59:59,999 --> 99:59:59,999
I took these motors which were designed to be able to move the a satellite dish to compensate for the rocking the ship

134
99:59:59,999 --> 99:59:59,999
and repurposed them to track through the sky while the ground is stable

135
99:59:59,999 --> 99:59:59,999
we don't have very many earthquakes in Tennessee – the last one that we had made rivers run the wrong direction but its okay – it's a geography thing

136
99:59:59,999 --> 99:59:59,999
This allows me to track things that are moving through the sky, but it doesn't actually matter where they're moving, because that's just a software problem.

137
99:59:59,999 --> 99:59:59,999
so in addition to tracking objects that are in low Earth orbit by a software patch I can also track
things that are in deep space it's not much harder to track and deep space probes or stars than it is to track items in low-earth orbit

138
99:59:59,999 --> 99:59:59,999
And then i added an software defined radio which allows me to record a signal now and demodulate it later. Which is necssary when you want to reverse engineer a signal.

139
99:59:59,999 --> 99:59:59,999
because a lot of the downlink of the satellites are completely non completely undocumented and being able to tune in to the right frequency is only half that you also need

140
99:59:59,999 --> 99:59:59,999
You also need a recording of sufficent quality to reverse engineer later on.

141
99:59:59,999 --> 99:59:59,999
We are sort of spoiled by software defined radios. When doing software defined radio work we usually have a very good signal to work from

142
99:59:59,999 --> 99:59:59,999
having high quality signals for later reverse engineering is necessary.

143
99:59:59,999 --> 99:59:59,999
I really wanted to be able to identify undocumented downlinks for low earth orbit in the same way that we already do this for and geo stationary orbit using tools like the ones that Adam Loria and Jin XXX made,

144
99:59:59,999 --> 99:59:59,999
I built a software framework as a collection of python daemons.

145
99:59:59,999 --> 99:59:59,999
..

146
99:59:59,999 --> 99:59:59,999


147
99:59:59,999 --> 99:59:59,999
there's a beagle board inside the radome, and there's a server in my home.

148
99:59:59,999 --> 99:59:59,999
..

149
99:59:59,999 --> 99:59:59,999
For maintenance, i can make my laptop pretend to be my dish, and can have steppers on my desk

150
99:59:59,999 --> 99:59:59,999
..

151
99:59:59,999 --> 99:59:59,999
Voyager 2 doesn't acutally come into the sky because of my position in the northern hemisphere.

152
99:59:59,999 --> 99:59:59,999
..

153
99:59:59,999 --> 99:59:59,999


154
99:59:59,999 --> 99:59:59,999
..

155
99:59:59,999 --> 99:59:59,999
This isn't accurate enough to target the dish, so

156
99:59:59,999 --> 99:59:59,999
..

157
99:59:59,999 --> 99:59:59,999
This is skytee helping out with the dish. He's zip-tying it because we know everything about duct-taping wehre i come from, but we know nothing about zip ties, so I had to bring in a german engineer.

158
99:59:59,999 --> 99:59:59,999
..

159
99:59:59,999 --> 99:59:59,999


160
99:59:59,999 --> 99:59:59,999
..

161
99:59:59,999 --> 99:59:59,999
As this thing spins around, by original design there's a ring connector where all the signals go through.

162
99:59:59,999 --> 99:59:59,999
..

163
99:59:59,999 --> 99:59:59,999
And that worked in the 90s because it had no reason to send anything faster than 9600 baud.

164
99:59:59,999 --> 99:59:59,999
..

165
99:59:59,999 --> 99:59:59,999
It can only move 400 degrees around,

166
99:59:59,999 --> 99:59:59,999
..

167
99:59:59,999 --> 99:59:59,999
We've got hte beagle board on the left, a usb-hub on the right and a

168
99:59:59,999 --> 99:59:59,999
..

169
99:59:59,999 --> 99:59:59,999
it also takes care of updating the motor position

170
99:59:59,999 --> 99:59:59,999
..

171
99:59:59,999 --> 99:59:59,999
The stepper motors themselves are the originals that the dish was designed with. They run into an EggBot-Board, which was designed to

172
99:59:59,999 --> 99:59:59,999
..

173
99:59:59,999 --> 99:59:59,999
so you can actually aim a satellite dish that's taller than you with technology easier than what's needed for a 3d printer.

174
99:59:59,999 --> 99:59:59,999
..

175
99:59:59,999 --> 99:59:59,999
The satellite dish sits in Tennessee,

176
99:59:59,999 --> 99:59:59,999
..

177
99:59:59,999 --> 99:59:59,999


178
99:59:59,999 --> 99:59:59,999
..

179
99:59:59,999 --> 99:59:59,999
So instead we took the radomeâthat's frank, that's my catâgive him cheers.

180
99:59:59,999 --> 99:59:59,999
..

181
99:59:59,999 --> 99:59:59,999
We took tape and we ran tape down the edges of the radome and then marked it.

182
99:59:59,999 --> 99:59:59,999
..

183
99:59:59,999 --> 99:59:59,999
And then you can sort of scan the sky for a stationary

184
99:59:59,999 --> 99:59:59,999
..

185
99:59:59,999 --> 99:59:59,999
and you can recover your position.

186
99:59:59,999 --> 99:59:59,999
..

187
99:59:59,999 --> 99:59:59,999


188
99:59:59,999 --> 99:59:59,999
..

189
99:59:59,999 --> 99:59:59,999
But I can also arrange it as a polar plot, which gives me a plot of what the radome is seeing.

190
99:59:59,999 --> 99:59:59,999
..

191
99:59:59,999 --> 99:59:59,999
[applause] A significant portion of the gui client was written while i was stuck on the U-Bahn connected using 3g

192
99:59:59,999 --> 99:59:59,999
..

193
99:59:59,999 --> 99:59:59,999
You can take the data out of this and run it through scientific software

194
99:59:59,999 --> 99:59:59,999
..

195
99:59:59,999 --> 99:59:59,999


196
99:59:59,999 --> 99:59:59,999
..

197
99:59:59,999 --> 99:59:59,999
The daemons that build this up, you need a norbit prediction daemon.

198
99:59:59,999 --> 99:59:59,999
..

199
99:59:59,999 --> 99:59:59,999
You need to update the orbits themselves.

200
99:59:59,999 --> 99:59:59,999
..

201
99:59:59,999 --> 99:59:59,999


202
99:59:59,999 --> 99:59:59,999
..

203
99:59:59,999 --> 99:59:59,999
But this format isn't incredibly accurate for satellites that correct their orbit.

204
99:59:59,999 --> 99:59:59,999
..

205
99:59:59,999 --> 99:59:59,999
So you need a daemon that grounds the new files from spacetrack and this is just a matter of a recursive

206
99:59:59,999 --> 99:59:59,999
..

207
99:59:59,999 --> 99:59:59,999
you also need motor control because you need to move the dish physically to

208
99:59:59,999 --> 99:59:59,999
..

209
99:59:59,999 --> 99:59:59,999
and then you need radio daemons to

210
99:59:59,999 --> 99:59:59,999
..

211
99:59:59,999 --> 99:59:59,999
and then after that you start to take software recorderings of that

212
99:59:59,999 --> 99:59:59,999
..

213
99:59:59,999 --> 99:59:59,999
So for orbit prediction i began with a DOS program that had been ported to Unix called predict. This works but it's garbage. It only supports 20 stars

214
99:59:59,999 --> 99:59:59,999
..

215
99:59:59,999 --> 99:59:59,999
because it's designed for astronomy photographers that want to take pictures of things

216
99:59:59,999 --> 99:59:59,999
..

217
99:59:59,999 --> 99:59:59,999
because otherwise you have to set an alarm clock for the half-hour pass where you can record them.

218
99:59:59,999 --> 99:59:59,999
..

219
99:59:59,999 --> 99:59:59,999


220
99:59:59,999 --> 99:59:59,999
..

221
99:59:59,999 --> 99:59:59,999
So i managed to track every single item in geostat orbit this thick ring here is the clarke-bell of all geostationary satellites as viewed from my northern hemisphere [?]

222
99:59:59,999 --> 99:59:59,999
..

223
99:59:59,999 --> 99:59:59,999
All IPC is running through this PostreSQL

224
99:59:59,999 --> 99:59:59,999
..

225
99:59:59,999 --> 99:59:59,999
you then send it simple commands, like SM,3000,500,-400

226
99:59:59,999 --> 99:59:59,999
..

227
99:59:59,999 --> 99:59:59,999
And then it will count that out, and send me back an OK. If i want to disable the motors, i'll send them em,0,0

228
99:59:59,999 --> 99:59:59,999
..

229
99:59:59,999 --> 99:59:59,999
EM,1,1 will enable both motors in 1/16s

230
99:59:59,999 --> 99:59:59,999
..

231
99:59:59,999 --> 99:59:59,999
You can see the motors themselves with the belts and the geartrains. This thing on the right would probably be illegal for me to turn on

232
99:59:59,999 --> 99:59:59,999
..

233
99:59:59,999 --> 99:59:59,999
The belts and stuff need to be measured to figure out what the reduction is

234
99:59:59,999 --> 99:59:59,999
..

235
99:59:59,999 --> 99:59:59,999
the IMU unit , this vectornav vn100 is a

236
99:59:59,999 --> 99:59:59,999
..

237
99:59:59,999 --> 99:59:59,999
it costs 500$ which was more than all of the other components together.

238
99:59:59,999 --> 99:59:59,999
..

239
99:59:59,999 --> 99:59:59,999
Now for position calculation, the elevation itself comes from the IMU. The azimuth

240
99:59:59,999 --> 99:59:59,999
..

241
99:59:59,999 --> 99:59:59,999
so the accelerometer will drift while the compass will be confused by the magnetic fields while the

242
99:59:59,999 --> 99:59:59,999
..

243
99:59:59,999 --> 99:59:59,999
and the IMU will be come of a backup how to make it reliable, but at the moment the position

244
99:59:59,999 --> 99:59:59,999
..

245
99:59:59,999 --> 99:59:59,999


246
99:59:59,999 --> 99:59:59,999
..

247
99:59:59,999 --> 99:59:59,999
The radio daomens. The first is a spectrum analyzer. It just measures the strength of the frequency

248
99:59:59,999 --> 99:59:59,999
..

249
99:59:59,999 --> 99:59:59,999
the downlink recorder dumps the IQ values

250
99:59:59,999 --> 99:59:59,999
..

251
99:59:59,999 --> 99:59:59,999
directly to an NFS share.

252
99:59:59,999 --> 99:59:59,999
..

253
99:59:59,999 --> 99:59:59,999
Client GUI is PyGame

254
99:59:59,999 --> 99:59:59,999
..

255
99:59:59,999 --> 99:59:59,999
Also notes these faint blue lines are positions where i saw particularly strong signals

256
99:59:59,999 --> 99:59:59,999
..

257
99:59:59,999 --> 99:59:59,999
I'm running out of time by these markers. does this mean we skip Q&amp;A or that I get kickd off of stage?

258
99:59:59,999 --> 99:59:59,999
..

259
99:59:59,999 --> 99:59:59,999
It takes SDR, it can provide maps of used different satellites in the sky.

260
99:59:59,999 --> 99:59:59,999
..

261
99:59:59,999 --> 99:59:59,999
I'd also like to make other ground stations. The software that I wrote should be portable

262
99:59:59,999 --> 99:59:59,999
..

263
99:59:59,999 --> 99:59:59,999
Another way that you can do it, the way that it's traditionally done to track stationary satellites is with a YAGI antenna

264
99:59:59,999 --> 99:59:59,999
..

265
99:59:59,999 --> 99:59:59,999
This is my van, my van is amazing. <i>applause</i>

266
99:59:59,999 --> 99:59:59,999
Thanks to nick farr. I had a bit to much too drink in

267
99:59:59,999 --> 99:59:59,999
But you want a news-van. And I said Hell yes, I want a news van!

268
99:59:59,999 --> 99:59:59,999
..

269
99:59:59,999 --> 99:59:59,999
But most importantly, it does SECAM

270
99:59:59,999 --> 99:59:59,999
..

271
99:59:59,999 --> 99:59:59,999
This is the control panel, and that's my talk! [applause]
