
from urllib.parse import urlencode
from urllib.request import urlopen
from urllib.request import Request
from collections import Counter

import difflib
import re
import urllib
import xml.etree.ElementTree as ET


def parseXML(xml_string):

    class TokenFromMary:
        original = ''
        normalized = ''
        sentence_id = 0
        def __init__(data, original, normalized):
            data.original = original
            data.normalized = normalized
        def __repr__(data):
            return "('{}' => '{}')".format(data.original,data.normalized)

    class Data:
        #list of resulting tokens: [TokenFromMary(), TokenFromMary(), ...]
        tokens = []
        paragraphs = []
        sentences = []
        sentence_ends = []

    def getNormalized(el):
        m = re.search(r'^(:?\{.*\})?(.*)$', el.tag or '')
        tag = m.group(2)
        #print(tag)
        if tag != 't':
            words = [getNormalized(child) for child in el]
        else:
            if el.get('sounds_like'):
                words=[]
                words.append(el.get('sounds_like'))
            else:
                words = [word.strip() for word in el.itertext()]
        return ' '.join(word for word in words if len(word) > 0)


    def parseEl(data, el):#parse one xml element and all sub-elements
        start = len(data.tokens)
        m = re.search(r'^(:?\{.*\})?(.*)$', el.tag or '')
        tag = m.group(2)
        if tag == 't':
            text = el.text or ''
            text = text.strip()
            normalized_text = el.get('sounds_like') or text
            if len(text) or len(normalized_text):
                data.tokens.append(TokenFromMary( text, normalized_text ))
            pos = el.get('pos')
            if pos == "$PUNCT":
                data.sentence_ends.append(len(data.tokens))

        if tag == 'mtu':
            original_text = el.get('orig')
            normalized_text = getNormalized(el)
            data.tokens.append(TokenFromMary( original_text, normalized_text ))
        else:
            for child in el:
                parseEl(data, child)

        end = len(data.tokens)
        if tag == 's':
            data.sentences.append((start,end))
            data.sentence_ends.append(end)
        elif tag == 'p':
            data.paragraphs.append((start,end))

    data = Data()
    root = ET.fromstring(xml_string)
    parseEl(data, root)

    data.sentence_ends.append(len(data.tokens))
    sentece_start = 0

    data.sentences = []
    for sentence_end in data.sentence_ends:
        if sentence_end > sentece_start:
            data.sentences.append((sentece_start, sentence_end))
        sentece_start = sentence_end

    return data;


def requestXML(text, output_type, language, url):

    values = {'INPUT_TEXT' : text,
              'INPUT_TYPE' : 'TEXT',
              'OUTPUT_TYPE' : output_type,
              'LOCALE' : language,
              'AUDIO' : '' }

    data = urlencode(values).encode('UTF-8')
    request = Request(url, data)
    request.add_header("Content-Type","application/x-www-form-urlencoded;charset=utf-8")
    response = urlopen(request)
    #print(response.geturl())
    error = None
    try:
        error = response.status != 200
    except AttributeError: pass

    if error:
        raise Exception("Error while requesting '{}' {}".format(url, response))

    return response.read().decode('UTF-8')


def refine_replace_ops(diff_ops, input_text, mary_output_text):
    diff_ops2 = []
    for op in diff_ops:
        operation, a_start, a_end, b_start, b_end = op
        if operation == 'replace': # Dieser rekursive Schritt kommt mir komisch vor, das sollte der differ sowieso machen
            differ2 = difflib.SequenceMatcher(lambda letter: False,
                                              input_text[a_start:a_end], mary_output_text[b_start:b_end],
                                              autojunk = False)
            #ops = differ2.get_opcodes()
            #print("replaced: '"+input_text[a_start:a_end]+ "' *with* '" +mary_output_text[b_start:b_end]+"'")
            #print(ops)
            for operation2, a_start2, a_end2, b_start2, b_end2 in differ2.get_opcodes():
                diff_ops2.append((
                    operation2,
                    a_start+a_start2, a_start+a_end2,
                    b_start+b_start2, b_start+b_end2))
        else:
            diff_ops2.append(op)
    return diff_ops2


def tokens_to_text_and_tokenization(tokens, add_space = False):
    """returns a string of the original text in the tokens and
    a list with of token ids, one for each character in the text"""
    text = ''
    text_ids = [] # token id for each character in text
    for i, token in enumerate(tokens):
        appended_text = token.original
        if add_space:
            appended_text += ' '
        text += appended_text
        for letter in appended_text:
            text_ids.append(i)
    return text, text_ids


def text_and_tokenization_to_tokens(text, tokenization):
    class Token:
        normalized = ""
        original = ""

    tokens = []
    id = tokenization[0]
    current_token = Token()
    
    for i in range(len(text)):
        if not tokenization[i] == id:
            tokens.append(current_token)
            id = tokenization[i]
            current_token = Token()
        current_token.original += text[i]

    return tokens


def match(mary_output, input_text, log):
    """mary_output: 'Data'-object, has tokens
    input_text: string
    log: logging function"""
    mary_output_text, mary_output_text_ids = tokens_to_text_and_tokenization(mary_output.tokens, True)

    def is_junk(letter):
        any_not_alpha = r'[\W\d_]' # non-word-character; digit; _
        return ('äöüßÄÜÖ'.find(letter) >= 0 or re.match(any_not_alpha, letter))
    
    differ = difflib.SequenceMatcher(is_junk, input_text, mary_output_text, autojunk = False)
    diff_ops = refine_replace_ops(differ.get_opcodes(), input_text, mary_output_text)
    # possible operations: replace, delete, insert, equal
    #diff_ops = Levenshtein.editops(input_text, mary_output_text)
    #diff_ops = Levenshtein.opcodes(diff_ops, input_text, mary_output_text)
    
    original_text_end = [-1 for each in mary_output.tokens]
    original_text_start = [len(input_text)+2 for each in mary_output.tokens]
    def register_match(token_id, start, end):
        # token-"range" can only be expanded
        if original_text_start[token_id] > start:
            original_text_start[token_id] = start
        if original_text_end[token_id] < end:
            original_text_end[token_id] = end

    for operation, a_start, a_end, b_start, b_end in diff_ops:
        if a_end - a_start == b_end - b_start:
            # equal sequences and simple replacements (op is either 'replace' or 'equal'):
            # There is a match for each letter!
            for i in range(a_end - a_start):
                original_pos = a_start + i
                token_id = mary_output_text_ids[b_start + i]
                register_match(token_id, original_pos, original_pos+1)
        elif len(re.sub(r'\s', '', input_text[a_start:a_end])) == len(re.sub(r'\s', '', mary_output_text[b_start:b_end])):
            i_a = a_start
            i_b = b_start
            while i_a < a_end and i_b < b_end:
                #skip spaces:
                while i_a < a_end and re.match(r'\s', input_text[i_a]):
                    i_a += 1
                while i_b < b_end and re.match(r'\s', mary_output_text[i_b]):
                    i_b += 1

                if i_a < a_end and i_b < b_end:
                    token_id = mary_output_text_ids[i_b]
                    register_match(token_id, i_a, i_a+1) # muss hier nicht vllt i_b hin?
                    i_a += 1
                    i_b += 1

        elif b_end > b_start and a_end > a_start and mary_output_text_ids[b_start] == mary_output_text_ids[b_end-1]:
            # sequences have different length, but belong to one token only:
            token_id = mary_output_text_ids[b_start]
            register_match(token_id, a_start, a_end)
        else:
            log(operation + ": '" + input_text[a_start:a_end] + "' to '" + mary_output_text[b_start:b_end] + "'")

    #previous_start = 0
    #for start in original_text_start:
    #    if start < prev, 'de_DE'ious_start:
    #        print("ERROR: overlap at "+str(start)+" is bigger than "+str(previous_start))
    #    previous_start = start

    return original_text_start, original_text_end


def attach_match(orig_in_tokens, orig_norm_tokens, log):
    """(a, b), (b, c) -> (a, c)
    lists can have different lengths"""
    def clean_word(word):
        return re.sub(r'[^\w]', '', word or '')
    in_words = [clean_word(t.normalized) for t in orig_in_tokens]
    norm_words = [clean_word(t.original) for t in orig_norm_tokens]
    differ = difflib.SequenceMatcher(None, in_words, norm_words, False)
    codes = differ.get_opcodes()

    for op, a_start, a_end, b_start, b_end in codes:
        if op == 'equal' or (op == 'replace' and a_end - a_start == b_end - b_start): # simple 1:1 mapping
            for i in range(a_end - a_start):
                orig_in_tokens[a_start + i].normalized = orig_norm_tokens[b_start + i].normalized
        elif op == 'replace' and a_end - a_start == 1:
            orig_in_tokens[a_start].normalized = " ".join(t.normalized for t in orig_norm_tokens[b_start:b_end])
        else:
            log("ERROR: {}".format((op, a_start, a_end, b_start, b_end)), level="info")
            


# km => Kilometer
# km² => Quadratkilometer
# \d m => $1 Meter
# /s => pro Sekunde
# km/h => Stundenkilometer  k m h
# m² => Quadratmeter
# m³ => Kubikmeter
def preprocess(input_text, language):
    # finden wo das segment anfängt. wie lang es ist. ersetzen.
    text = input_text
    if language.startswith("de"):
        text = re.sub(r'\\cdot', ' mal ', text)
        text = re.sub(r'(\w+)\^\{(.+)\}', r' \1 hoch \2 ', text)
        text = re.sub(r'(\w+)\^(\S+)', r' \1 hoch \2 ', text)
        text = re.sub(r'(?:^|(?<!\w))(1[6789])00(?:$|(?![\w,]))', r' \1 Hundert ', text)
        text = re.sub(r'(?:^|(?<!\w))(1[6789])0(\d)(?:$|(?![\w,]))', r' \1 Hundert \2 ', text)
        text = re.sub(r'(?:^|(?<!\w))(1[6789])(\d\d)(?:$|(?![\w,]))', r' \1 Hundert \2 ', text)
        text = re.sub(r'(?<=[\W\d])α(?=[\W\d])', ' alpha ', text)                   
        text = re.sub(r'(?<=[\W\d])β(?=[\W\d])', ' beta ', text)                    
        text = re.sub(r'(?<=[\W\d])ɣ(?=[\W\d])', ' gamma ', text)                   
        text = re.sub(r'(?i)(?<=[\W\d])km/h(?!\w)', ' Stundenkilometer', text)      
        text = re.sub(r'(?i)(?<=[\W\d])km(?!\w)', ' Kilometer', text)               
        text = re.sub(r'(?i)(?<=[\W\d])km²(?!\w)', ' Quadratkilometer', text)       
        text = re.sub(r'(?i)(?<=[\W\d])km³(?!\w)', ' Kubikkilometer', text)         
        text = re.sub(r'(?<=[\W\d])mm(?!\w)', 'milli meter', text)                  
        text = re.sub(r'(?i)(?<=[\W\d])µm(?!\w)', 'mikro meter', text)              
        text = re.sub(r'(?i)(?<=[\W\d])nm(?!\w)', 'nano meter', text)               
        text = re.sub(r'(?i)(?<=\d)\s*m(?!\w)', ' Meter', text)                     
        text = re.sub(r'(?i)(?<=[\W\d])m²(?!\w)', ' Quadratmeter', text)            
        text = re.sub(r'(?i)(?<=[\W\d])m³(?!\w)', ' Kubikmeter', text)              
        text = re.sub(r'/s(?!\w)', ' pro Sekunde ', text)
    elif language.startswith("en"):
        text = re.sub(r'\\cdot', ' times ', text)
        text = re.sub(r'(\w+)\^\{(.+)\}', r' \1 powered to \2 ', text)
        text = re.sub(r'(\w+)\^(\S+)', r' \1 powered to \2 ', text)
        text = re.sub(r'(?:^|(?<!\w))(1[6789])00(?:$|(?![\w,]))', r' \1 hundred ', text)
        text = re.sub(r'(?:^|(?<!\w))(1[6789])0(\d)(?:$|(?![\w,]))', r' \1 hundred \2 ', text)
        text = re.sub(r'(?:^|(?<!\w))(1[6789])(\d\d)(?:$|(?![\w,]))', r' \1 hundred \2 ', text)
        text = re.sub(r'(?<=[\W\d])α(?=[\W\d])', ' alpha ', text)                   
        text = re.sub(r'(?<=[\W\d])β(?=[\W\d])', ' beta ', text)                    
        text = re.sub(r'(?<=[\W\d])ɣ(?=[\W\d])', ' gamma ', text)                   

    
    text = re.sub(r'\s\s+', ' ', text)
    text = text.strip()
        
    return text


def postprocess(data, language):
    if language.startswith("de"):
        last_token = None
        is_new_sentence = [False for each in data.tokens]
        for sentence in data.sentences:
            is_new_sentence[sentence[0]] = True

        for token in data.tokens:
            normalized = token.normalized or ''
            normalized = re.sub(r'¾', ' drei Viertel ', normalized)
#            normalized = preprocess(normalized, language)
            normalized = re.sub(r'\s+', ' ', normalized)
            token.normalized = normalized.strip()

        for i,token in enumerate(data.tokens):
            if token.original.endswith('.'):
                if is_new_sentence[i]:
                    token.normalized += 'ns'
                elif last_token and re.search(r'(^|\W)(?i)(dem|zum|am|vom|unterm|überm|aufm|im)$', last_token.normalized):
                    token.normalized += 'n'
                #elif i+1 < len(data.tokens) and re.search(r'^(?i)(Januar|Februar|März|April|Mai|Juni|Juli|August|September|Oktober|November|Dezember)(\s|$)', data.tokens[i+1].normalized):
                #    token.normalized += 'r'
            last_token = token
    return data
