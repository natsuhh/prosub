#!/usr/bin/env python3
import unittest
import marytts as m


class TestPreprocess(unittest.TestCase):

    test_cases_de = [("1980", "19 Hundert 80"),
                     ("Im Jahr 1980", "Im Jahr 19 Hundert 80"),
                     ("2^2", "2 hoch 2"),
                     ("x^y", "x hoch y"),
                     ("x^{velocity}", "x hoch velocity")]

    test_cases_en = []

    def test_de(self):
        for orig, expected in self.test_cases_de:
            self.assertEqual(m.preprocess(orig, "de"), expected)

    def test_en(self):
        for orig, expected in self.test_cases_en:
            self.assertEqual(m.preprocess(orig, "en"), expected)

    

if __name__ == '__main__':
    unittest.main()
