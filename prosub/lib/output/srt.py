import io
import os
import pysrt
import subprocess

def output_srt(data, title="untitled"):
    """Generates SRT from chunks.

    Format is described here: http://en.wikipedia.org/wiki/SubRip
    """
    srt_items = []
    index = 0
    for line in chunks:
        text = line.text
        start = line.start
        stop = line.stop
        
        start = int(float(start)*1000)
        stop = int(float(stop)*1000)
        text = ''.join(text).strip()
        srt_items.append(pysrt.SubRipItem(index, start, stop, text))
        index += 1


    output = io.StringIO()
    pysrt.SubRipFile(srt_items).write_into(output)
    return output.getvalue()
