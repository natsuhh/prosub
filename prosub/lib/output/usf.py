import subprocess
import os
import xml.etree.cElementTree as ET

def output_usf(chunks, title="untitled"):
    """Generates USF XML from chunks.

    Format is described here: http://www.titlevision.com/usf.htm
    """
    el_root = ET.Element("USFSubtitles");
    el_root.set("version","1.0");
    el_metadata = ET.SubElement(el_root, "metadata")
    
    el_title = ET.SubElement(el_metadata,"title")
    el_title.text = title

    el_author = ET.SubElement(el_metadata, "author")
    el_author_name = ET.SubElement(el_metadata,"name")
    el_author_name.text = "???"

    el_language = ET.SubElement(el_metadata, "language")
    el_language.text = "English"
    el_language.set("code","eng")

    el_comment = ET.SubElement(el_metadata,"comment")
    el_comment.text = "Generated with prosub";     
    
    def formatTimespan(totalSeconds):
        hours,r = divmod(float(totalSeconds)/1000, 3600)
        minutes, seconds = divmod(r, 60)
        return '{:02.0f}:{:02.0f}:{:06.3f}'.format(hours,minutes,seconds)

    el_subtitles = ET.SubElement(el_root,"subtitles")

    for line in chunks:
        text = line.text
        start = line.start
        stop = line.stop

        el_subtitle = ET.SubElement(el_subtitles, "subtitle")
        el_text = ET.SubElement(el_subtitle, "text")
        #text_element.set("style","blubb");
        el_subtitle.set("start", formatTimespan(start))
        el_subtitle.set("stop", formatTimespan(stop))
        el_text.text = ''.join(text).strip()

    return ET.tostring(el_root, encoding="utf-8", method="xml")
