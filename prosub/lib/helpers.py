def mergedicts(a, b):
    "merges b into a"
    for key in b:
        if key in a and isinstance(a[key], dict) and isinstance(b[key], dict):
            a[key] = mergedicts(a[key], b[key])
        else:
            a[key] = b[key]
    return a


def to_ms(float_):
    if not float_: return None
    return int(float_*1000)


def to_sec(int_):
    if not int_: return None
    return int_/1000


class DotDict(dict):
    """dot.notation access to dictionary attributes"""
    def __getattr__(self, attr):
        return self.get(attr)
    __setattr__= dict.__setitem__
    __delattr__= dict.__delitem__


def mkdotdict(v={}):
    if type(v) == dict:
        return DotDict({k: mkdotdict(i) for k, i in v.items()})
    elif type(v) == list:
        return [mkdotdict(i) for i in v]
    else:
        return v

