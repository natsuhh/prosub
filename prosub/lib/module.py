from prosub.lib.helpers import mergedicts, mkdotdict
from prosub.data.data import Data

import os
import json
import sys

"""All registered module classes."""
all_modules = {}

class MetaModule(type):
    """A metaclass to register modules in the module dictionary, using their
    modulename as key.
    """
    def __new__(cls, name, bases, attrs):
        class_instance = type.__new__(cls, name, bases, attrs)

        if name != "Module":
            if not class_instance.modulename:
                raise TypeError("Module class {} needs a modulename.".format(name))

            all_modules[class_instance.modulename] = class_instance

        return class_instance


class Run(object):
    def __init__(self):
        # List of modules to run.
        self.modules = []

        #: The current data object containing all data.
        self.data = Data()

        #: Name of the current run, usually used to determine file names.
        self.name = None

        #: The global configuration.
        self.config = mkdotdict()

    def load_config(self, configfile):
        with open(configfile) as f:
            self.config = mergedicts(self.config, mkdotdict(json.loads(f.read())))
            if "parent" in self.config:
                this_config = self.config
                self.load_config(self.config.parent)
                mergedicts(self.config, this_config)

    def set_config(self, path, value):
        paths = path.split(".")
        config = self.config
        for p in paths[:-1]:
            if not p in config:
                config[p] = mkdotdict()
            config = config[p]

        if paths[-1] in config:
            if isinstance(config[paths[-1]], list):
                value = value.split(",")
            if isinstance(config[paths[-1]], bool):
                value = False if value.lower() in ("0", "no", "false", "n", "f", "off", "") else bool(value)
        config[paths[-1]] = value

    def add_module(self, module):
        self.modules.append(module)
        module.run = self

    def prepare(self):
        # Set the name and generate some filenames from it
        self.name = self.config.name
        if not self.name:
            self.log("Please provide a run name with command line flag `-n` or config option `name`.", level="error")
            sys.exit(1)

        if not "input" in self.config:
            self.config["input"] = mkdotdict()
        for key in ("audio", "transcript"):
            self.config["input"][key] = self.config.input[key].format(name=self.name)

        # Load prepared data
        data = self.config.data
        if data and isinstance(data, dict):
            self.data = Data().deserialize(data)
        elif data and isinstance(data, str):
            with open(data) as f:
                self.data = Data().deserialize(f.read())

        for step in self.config.steps:
            parts = step.split("/")
            name = parts[0]
            variant = parts[1] if len(parts) > 1 else None

            if not name in all_modules:
                self.log("Module not found: {}".format(name), level="error")
                sys.exit(1)

            ModuleClass = all_modules[name]

            module = ModuleClass()
            module.set_variant(variant)
            self.add_module(module)

    def execute(self):
        if self.config["save-steps"]:
            self.save_data(os.path.join(self.dir, "steps", "_before"))

        for module in self.modules:
            name = module.modulename

            try:
                config = self.config.modules[name]
            except KeyError:
                self.log("Please supply a default configuration for the module {} in config/default.json".format(name), level="error")
                sys.exit(1)
            self.log("Executing {}".format(name), level="info")
            module.execute(config)

            if self.config["save-steps"]:
                self.save_data(os.path.join(self.dir, "steps", name + ".data.json"))

    def save_data(self, filename):
        dirname = os.path.dirname(filename)
        if not os.path.exists(dirname):
            os.makedirs(dirname)

        with open(filename, 'w') as f:
            f.write(str(self.data))

    def save_config(self, filename):
        dirname = os.path.dirname(filename)
        if not os.path.exists(dirname):
            os.makedirs(dirname)

        with open(filename, 'w') as f:
            f.write(json.dumps(self.config))

    @property
    def tempdir(self):
        path = os.path.join(self.dir, "temp")
        if not os.path.exists(path):
            os.makedirs(path)
        return path

    @property
    def dir(self):
        return os.path.abspath(os.path.join(self.config.output, self.name))

    def log(self, *args, level="debug", **kwargs):
        LEVELS = ["all", "verbose", "debug", "info", "warning", "error"]
        if level and LEVELS.index(level) < LEVELS.index(self.config.loglevel):
            return
        print(("{:>%ds} -"%max([len(w) for w in LEVELS])).format(level.upper()), *args, **kwargs)



class Module(metaclass=MetaModule):
    """
    A module represents a step in the pipeline. Subclass this to
    implement a new step. Set the :attr:`modulename` to define
    a name for this module, which can then be used in the config
    option `steps`.
    """
    modulename = None
    variants = ()

    def __init__(self):
        self.variant = None
        self.run = None

    def set_variant(self, variant):
        if variant:
            if not variant in self.variants:
               raise ValueError("Invalid variant {} for module {}.".format(variant, self.modulename))
            self.variant = variant
        else:
            self.variant = self.variants[0] if self.variants else None

    def log(self, *args, level="debug", **kwargs):
        """Logs arguments to log output."""
        self.run.log("[{}]".format(self.modulename), *args, level=level, **kwargs)

    def execute(self, config):
        """Override this to implement custom behaviour."""
        print("Module %s has empty execute()" % self.modulename)

