
def get_sentence_words(data):
    """Extracts arrays of word objects from the data, grouped into arrays by sentence.
    """
    return [data["words"][sentence["start_word"]:sentence["end_word"]] for sentence in data["sentences"]]

def recompile_sentence(words):
    return "".join([word["original"] for word in words])

def get_sentences(data):
    return map(recompile_sentence, get_sentence_words(data))

