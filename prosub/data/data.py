from prosub.lib.helpers import mkdotdict

import json

class Dictable(object):
    """Helper class to serialize objects."""
    dictable_attributes = []

    def to_dict(self):
        d = {}
        for attr in self.dictable_attributes:
            value = getattr(self, attr)
            if value != None:
                d[attr] = value
        return d

    def from_dict(self, d):
        for attr in self.dictable_attributes:
            if attr in d:
                setattr(self, attr, d[attr])
        return self


class Data(object):
    """All the data we work with. Stored in :py:attr:`Run.data <prosub.lib.module.Run.data>`."""

    def __init__(self):
        #: List of Word objects, in order
        self.words = []

        #: Raw data, e.g. huge strings, that should be somehow saved and shared across modules
        self.raw = mkdotdict()

        #: List of Sentence objects, in order
        self.sentences = []

        #: List of Chunk objects, in order
        self.chunks = []

    @property
    def sentence_words(self):
        """Extracts arrays of word objects from the data, grouped into arrays by sentence."""
        return [self.words[sentence.start_word:sentence.end_word] for sentence in self.sentences]

    @property
    def sentence_strings(self):
        """Returns one string per sentence, its words joined together."""
        def join(words):
            return "".join([word["original"] for word in words])
        return map(join, self.sentence_words)

    def __str__(self):
        return json.dumps(dict(
            words=[word.to_dict() for word in self.words],
            raw={key: str(raw) for key, raw in self.raw.items()},
            sentences=[sentence.to_dict() for sentence in self.sentences],
            chunks=[chunk.to_dict() for chunk in self.chunks]),
            indent=4)

    serialize = __str__

    def deserialize(self, content):
        if isinstance(content, str):
            content = json.loads(content)

        if "raw" in content:
            self.raw = mkdotdict(content["raw"])

        if "words" in content:
            self.words = [Word().from_dict(word) for word in content["words"]]

        if "sentences" in content:
            self.sentences = [Sentence().from_dict(sentence) for sentence in content["sentences"]]

        if "chunks" in content:
            self.chunks = [Chunk(self).from_dict(chunk) for chunk in content["chunks"]]

        return self



class Word(Dictable):
    dictable_attributes = ["original", "index", "start", "stop", "normalized", "misspelled", "pos_tag", "syntax_parent", "syntax_parent_local", "syntax_relation"]

    def __init__(self, original=None, index=None, normalized=None, start=None, stop=None, misspelled=None, pos_tag=None, syntax_parent=None, syntax_relation=None, syntax_parent_local=None):
        #: Original word, as from the transcript, including following punctuation.
        self.original = original

        #: Index of the word in data
        self.index = index

        #: Normalized and spellcorreted version of the word, without punctuation.
        self.normalized = normalized

        #: Start time, in ms
        self.start = start

        #: End time, in ms
        self.stop = stop

        #: Normalized word before spell correction. Optional, only exists if different from `normalized`.
        self.misspelled = misspelled

        #: Part-of-speech tag
        self.pos_tag = pos_tag

        #: Parent in syntax tree
        self.syntax_parent = syntax_parent

        #: Parent in syntax tree, local to sentence
        self.syntax_parent_local = syntax_parent_local

        #: Relation to the syntax parent
        self.syntax_relation = syntax_relation


class Sentence(Dictable):
    dictable_attributes = ["start_word", "end_word"]

    def __init__(self, start_word=None, end_word=None):
        #: Index of the first word of the sentence.
        self.start_word = start_word

        #: Index of the first word that is not part of the sentence anymore.
        self.end_word = end_word


class Chunk(Dictable):
    dictable_attributes = ["start_word", "end_word", "penalty", "line_breaks", "start_time", "end_time"]

    def __init__(self, data, start_word=None, end_word=None, penalty=None, line_breaks=None, start_time=None, end_time=None):

        #: Reference to data object
        self.data = data

        #: Index of the first word of the sentence
        self.start_word = start_word

        #: Index of the first word that is not part of the sentence anymore
        self.end_word = end_word

        #: Penalty Score of the chunk as calculated by chunkify
        self.penalty = penalty

        #: Indices of words in front of which the line breaks should occur
        self.line_breaks = line_breaks

        #: Start time of the chunk, in ms
        self.start_time = start_time

        #: End time of the chunk, in ms
        self.end_time = end_time

    @property
    def text(self):
        """Joins words to the original text."""
        text = ""
        for i, word in enumerate(self.words):
            if self.line_breaks and i in self.line_breaks:
                text += "\n"
            text += word.original
        return text

    @property
    def length(self):
        return self.end_time - self.start_time

    @property
    def words(self):
        return self.data.words[self.start_word:self.end_word]
