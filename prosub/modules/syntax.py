from prosub.lib.module import Module

import os
import re
import sys

class SyntaxModule(Module):
    modulename = "syntax"

    def execute(self, config):
        # Do the import, which is magically hacky but works :)
        sys.path.append("data/TurboParser-2.2.0/python/build/lib.linux-x86_64-{major}.{minor}/".format(
            major=sys.version_info[0], minor=sys.version_info[1]))
        from ctypes import cdll
        gflags = cdll.LoadLibrary('data/TurboParser-2.2.0/deps/local/lib/libgflags.so')
        glog = cdll.LoadLibrary('data/TurboParser-2.2.0/deps/local/lib/libglog.so.0')
        import turboparser as tp

        self.log("Loading models")

        self.interface = tp.PTurboParser()
        self.parser = self.interface.create_parser()
        if not config.skip and not config["skip-tagger"]:
            self.parser.load_parser_model(config["parser-model"].encode('ascii'))

        self.tagger = self.interface.create_tagger()
        if not config.skip and not config["skip-parser"]:
            self.tagger.load_tagger_model(config["tagger-model"].encode('ascii'))

        self.tag(config)
        self.parse(config)

    def normalize(self, word):
        if word == None or not len(word) > 0:
            return None

        word = word.strip()

        replacements = {"n't": "not", "'ll": "will", "'ve": "have", "'s": "is", "'d": "would", "'m":"am", "'clock":"clock", "'re": "are"};
        if word in replacements:
            return replacements[word]

        word = re.sub(r'\s', ' ', word)

        return word

    def tag(self, config):
        self.log("Starting Tagger")
        untagged = []
        word_ids = []
        for sentence in self.run.data.sentences:
            for word_id in range(sentence.start_word, sentence.end_word):
                word = self.run.data.words[word_id]
                normalized_word = self.normalize(word.original)
                if normalized_word:
                    untagged.append("{word}\tFW".format(word=normalized_word))
                    word_ids.append(word_id)
            untagged.append("")

        tempfile_untagged = os.path.join(self.run.tempdir, "untagged")
        tempfile_tagged = os.path.join(self.run.tempdir, "tagged")

        with open(tempfile_untagged, 'w') as f:
            f.write("\n".join(untagged))

        if not config.skip and not config["skip-tagger"]:
            self.tagger.tag(tempfile_untagged.encode('ascii'), tempfile_tagged.encode('ascii'))

        # Read parsed data
        tagged = ""
        with open(tempfile_tagged, "r") as f:
            tagged = f.readlines()

        # Disregard empty lines
        tagged = [line.strip() for line in tagged if line.strip()]

        for index, line in enumerate(tagged):
            word, tag = line.split('\t')
            word_id = word_ids[index]
            self.run.data.words[word_id].pos_tag = tag

    def parse(self, config):
        self.log("Starting Parser")
        unparsed = []
        word_ids = []
        for sentence in self.run.data.sentences:
            index = 1
            for word_id in range(sentence.start_word, sentence.end_word):
                word = self.run.data.words[word_id]
                normalized_word = self.normalize(word.original)
                if normalized_word:
                    unparsed.append("{index}\t{word}\t_\t{pos_tag}\t{pos_tag}\t_\t_\t_".format(index=index, word=normalized_word, pos_tag=word.pos_tag))
                    index += 1
                    word_ids.append(word_id)
            unparsed.append("")

        tempfile_unparsed = os.path.join(self.run.tempdir, "unparsed")
        tempfile_parsed = os.path.join(self.run.tempdir, "parsed")

        with open(tempfile_unparsed, 'w') as f:
            f.write("\n".join(unparsed))

        if not config.skip and not config["skip-parser"]:
            self.parser.parse(tempfile_unparsed.encode('ascii'), tempfile_parsed.encode('ascii'))

        # Read parsed data
        parsed = ""
        with open(tempfile_parsed, "r") as f:
            parsed = f.readlines()

        # Disregard empty lines
        parsed = [line.strip() for line in parsed if line.strip()]

        sentence_start_index = 0

        for index, line in enumerate(parsed):
            word_id = word_ids[index]

            i, word, _, tag, _, _, parent, relation = line.split('\t')
            i = int(i)
            parent = int(parent)
            self.run.data.words[index].syntax_parent_local = parent
            tag = tag.strip()
            relation = relation.strip()

            # Every first word in a sentence should be remembered
            if i == 1:
                sentence_start_index = word_id

            # Set sentence root parent to None
            if parent == 0:
                parent = None
            # Convert parent ID to word index
            else:
                parent = parent - 1 + sentence_start_index

            self.run.data.words[word_id].syntax_parent = parent
            self.run.data.words[word_id].syntax_relation = relation

