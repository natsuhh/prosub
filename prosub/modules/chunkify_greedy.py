from prosub.lib.module import Module
from prosub.data.data import Chunk

import json
import os
import re
import subprocess
import sys

def execute(self, config):
    data = self.run.data
    words = data.words

    if len(words) == 0: return

    # Note: the edge case that we have *no* timing information for the current line is
    # solved with taking the end timing information from the previous line, because we need
    # some data for SRT export.

    # score the individual splits:
    # splitting_penalty[i] determines how likely a split just BEFORE the word i is
    # higher value => split less likely
    splitting_penalty = [0 for i in range(len(words)+1)]#sic one more 0 at the end

    i = 0
    penalty_after = 100.0
    for word in words:
        word = word.original
        penalty_before = penalty_after
        penalty_after = 100.0
        if re.search(r'^(and|or)[^\w]*$', word, flags=re.IGNORECASE):
            penalty_before = 60.0
        if re.search(r'^(the|a|an|on|of|in)[^\w]*$', word, flags=re.IGNORECASE):
            penalty_after = 150.0
        if re.search(r'\p\s+$',word):
            penalty_after = 50.0
        if re.search(r'\n\s*$',word):
            penalty_after = 60.0
        if re.search(r'[^\s]$', word):
            penalty_after = 200
        splitting_penalty[i] = penalty_before
        i += 1

    # Splitting in front of a sentence is good.
    for sentence in data.sentences:
        splitting_penalty[sentence.start_word] = 0

    # simple greedy algorith more finding the lines:
    # lines is an array of tuples [(line_start,line_end),(line_start_line_end),...]
    lines = []

    i = 0
    while i < len(words):
        # add words until maximal line length is reached
        # line is words[i:j]
        l = 0
        j = i
        while j < len(words):
            word = words[j].original
            l += len(word)
            if j > i and l > 46:
                break
            j += 1

        #find the minimum penalty split
        min_penalty = splitting_penalty[j]
        min_split = j
        k = j-1
        while k > i:
            if splitting_penalty[k] < min_penalty:
                min_penalty = splitting_penalty[k]
                min_split = k
            k -= 1

        # every line must include at least one word:
        if min_split <= i:
            min_split = i+1

        # line goes from [i,min_split)
        lines.append((i,min_split))
        i = min_split

    # Write data
    self.run.data.chunks = [Chunk(self.run.data, start, stop) for start, stop in lines]