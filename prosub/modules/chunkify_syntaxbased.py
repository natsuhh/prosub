from prosub.lib.module import Module
from prosub.data.data import Chunk
from pprint import pprint

import json
import os
import re
import subprocess
import sys
import queue

words = {} #???
def execute(self, config):
    self.run.data.chunks = []
    count = len(self.run.data.sentences)
    for i, sentence in enumerate(self.run.data.sentences):
        print("\rChunking {}/{}... [{:3d}%]".format(i+1, count, round((i+1)*100/count)), end="")
        s = SentenceChunkify(self.run.data, sentence, config.smart)
        s.execute()
    print("")

class SentenceChunkify(object):
    def __init__(self, data, sentence, smart):
        self.data = data
        self.sentence = sentence
        self.words = data.words[sentence.start_word:sentence.end_word]
        self.charcount = sum(len(word.original) for word in self.words)

        # charecter limit per line, assumes 2 lnes per subtitle
        self.line_limit = 34
        # how important good line breaks are for chunking. should be between 0 and 1
        # 0 means no line breaks
        self.lb_importance = 0.25
        
        #should not seperate
        self.braces = [('[', ']'), ('(', ')')]
        # stuff in brackets
        self.units = self.comp_units()
        # changes, which rules are used for chunking
        self.be_smart = 1 if smart else 0
        
    
    def comp_units(self):
        result = []
        brace_stacks = [0 for x in self.braces]
        begin = -1
        outer_type = -1
        for i, word in enumerate(self.words):
            for j, b in enumerate(self.braces):
                if b[0] in word.original and not (b[1] in word.original):
                    brace_stacks[j] += 1
                    if begin == -1 and brace_stacks[j] == 1:
                        begin = i
                        outer_type = j
                if b[1] in word.original and not (b[0] in word.original):
                    brace_stacks[j] -= 1
                    if brace_stacks[j] == 0 and begin != -1 and outer_type == j:
                        result += [(begin, i)]
                        begin = -1
                        outer_type = -1
                        brace_stacks = [0 for x in self.braces]
        return result

    ############################################
    ## Dijkstra stuff
    ############################################

    def gen_children(self, node):
        return [node + [x] for x in range(node[-1]+1, len(self.words) + 1)]

    def combined_penalty(self, splits):
        """Returns the penalty of the last split in the splits array. Combines single and chunk penalties."""
        if len(splits) <= 1:
            return 0,0.0, ['len splits <= 1'], ''

        single, descriptions=self.penalty_single(splits[-1])
        length_penalty, description_length = self.penalty_chunk(splits[-2], splits[-1])

        return single , length_penalty , descriptions, description_length

    def is_valid(self, splits):
        return splits[-1] == len(self.words)

    def execute(self):

        ## for one sentence, e.g [0,11,21,30,39]
        splits = self.compute()

        for i in range(1, len(splits)):
            # for every pair: a) compute combined penalty again, b) create Chunk-obj.

            # recompute indexes from sentence-based to global-based
            start_index = splits[i-1] + self.sentence.start_word
            end_index = splits[i] + self.sentence.start_word

            split = splits[0:i]

            # print('calling combined_penalty with', split, splits)
            single_penalties, length_penalty, descriptions, description_length = self.combined_penalty(split)

            #calculate best line_break
            line_breaks = []
            description_lb = ""
            if self.charcount > self.line_limit:

                # compute line break penalty for every possible position in given
                # chunk
                chunk_start_word_id = splits[i-1]
                chunk_end_word_id = splits[i]
                split_positions = range(chunk_start_word_id, chunk_end_word_id)

                line_break_penalties = [ (
                    self.penalty_line_break(
                        chunk_start_word_id,
                        chunk_end_word_id,
                        split_position)[0],
                    split_position)
                    for split_position in split_positions ]

                # take the tuple (penalty, split_position) which minimizes penalty.
                # then take the split_position into best_split_position
                best_split_position = min(line_break_penalties)[1]

                # we want the word id relative to the chunk start word id;
                # to be consumed by the text property of Chunk
                description_lb = self.penalty_line_break(
                        chunk_start_word_id,
                        chunk_end_word_id,
                        best_split_position)[1]
                line_breaks = [best_split_position - chunk_start_word_id]

            chunk = Chunk(self.data,
                    start_index, end_index,
                    penalty=(length_penalty, description_length, descriptions, description_lb),
                    line_breaks=line_breaks)

            self.data.chunks.append(chunk)

    def compute(self):
        ### queue contains elements of structure
        ### (akku_penalty, splits)
        ### where splits is a list of splits until now,
        ### and akku_penalty is the accumulated penalty, that is,
        ### the sum of penalties from the root to this path
        q = queue.PriorityQueue()

        q.put((0,[0]))
        while True:
            # get the optimum partial result from the queue
            penalty, current = q.get_nowait()

            # check if current is valid result, if so, it is the first one
            # and the best one
            if self.is_valid(current):
                return current

            for child in self.gen_children(current):
                single_penalties, length_penalty, _, _ = self.combined_penalty(child)
                combined_penalty = single_penalties + length_penalty

                akku_penalty = penalty + combined_penalty
                try:
                    q.put_nowait((akku_penalty, child))
                except:
                    pass


    ############################################
    ## The actual penalty functions
    ############################################

    def penalty_chunk(self, start, end):
        # 1 for size 1 chunks,
        # 0.1 for size 10 chunks,
        # 1/n for size n chunks,
        # ...
        chunk_charcount = sum(len(self.words[i].original) for i in range(start, end))
        # preferred_chunk_size = 42
        # preferred_chunk_size = 1
        x = chunk_charcount # / preferred_chunk_size
        # TODO: Refactor: some variables are redundant

        # http://www.wolframalpha.com/input/?i=plot+1+-+2+*+x+%5E+2+%2B+1.2+*+x%5E8+for+x%3D0..1.4+and+y+%3D+0..1
        # This function starts (x=0) at 1, dips down to 0 around x=0.8 and then shoots up from 1 upwards
        max_chars = 2*self.line_limit
        min_chars = 1.5*self.line_limit
        result_pen = 0
        result_text = ''
        smart_factor = 1 if self.be_smart else .5
        if x > max_chars:
            result = ((x-max_chars)/2.0)**1.2 * smart_factor
            result_pen = result
            result_text = '(({} chars - {})/2.0)**1.2'.format(int(x), int(max_chars))  #Done?: update whith max_chars
        if x < min_chars:
            result_pen = abs((x-min_chars)**1.2) * smart_factor
            result_text = 'abs({} chars -{})**1.2'.format(int(x), int(min_chars)) #Done?: update whith min_chars
        #line breaks
        best_lb = min([self.penalty_line_break(start, end, x)[0] for x in range(start, end)])
        result_pen += self.lb_importance * best_lb
        #braces/units
        for unit in self.units:
            if end > unit[0] and end < unit[1]:
                result_pen += 100
        return result_pen, result_text


    #returns a penalty for the split between two words
    # _split_cache = {}
    def penalty_single(self, split):

        # if split in self._split_cache:
        #     return self._split_cache[split]

        if split <= 0: return 0,['']
        if split >= len(self.words): return 0, ['']

        word1 = self.words[split-1]
        word2 = self.words[split]

        penalty = 0

        penalty_rules = [
            [
                lambda w1, w2: w1.syntax_relation != 'p',
                3,
                'split without punctuation'
            ],
            [
                lambda w1, w2:  \
                    (w2.start != None and
                     w1.stop != None and
                    (w2.start - w1.stop <= 4000)),
                lambda w1, w2:  (10 - (w2.start - w1.stop) * 0.0025) * self.be_smart,   #shorter pause => higher penalty; this should probibly be really high
                'split without long pauses'
            ],
            [
                lambda w1, w2: self.clause_depth(w1) == self.clause_depth(w2),
                10 * self.be_smart,
                'split inside a clause'
            ],
            [
                lambda w1, w2: w2.syntax_relation == "p",
                200,
                'split before punctuation'
            ],
            [
                lambda w1, _: w1.original in ["and ", "or "],   #this might be redundant if clause_depth includes "cc" as clause_root
                50,
                'split after and/or'
            ],
            [
                lambda w1, w2: w1.pos_tag=='NNP' and w2.pos_tag=='NNP',
                50 * self.be_smart,
                'split names'
            ],
            [
                lambda w1, w2: (w1.syntax_parent != None and w1.syntax_parent == w2.index) or \
                               (w2.syntax_parent != None and w2.syntax_parent == w1.index),
                20 * self.be_smart,
                'split parents from children'
            ],
            [
                lambda w1, w2: \
                    w1.syntax_relation in ['dep', 'compmod', 'amod', 'aux', 'expl'] or \
                    w2.syntax_relation in ['det', 'adpobj'],
                5 * self.be_smart,
                "don't split dep, det, compmod, amod, compmod, adpobj, aux, expl"
            ],
            [
                lambda w1, w2: \
                    (w1, w2) == ("kind", "of") or (w1, w2) == ("sort", "of"),
                5,
                "don't split \"kind of\""
            ]
        ]

        penalty = 0
        descriptions = []

        # for lack of a better term: one item of the aboce penalty_rules array
        # is called a "rule_compound"
        for rule_compound in penalty_rules:
            # if the second item of rule_compound is a function,
            # compute the penalty with this function
            if callable(rule_compound[1]):
                rule, penalty_function, description = rule_compound
                if rule(word1, word2):
                    penalty_score = penalty_function(word1, word2)
                    penalty += penalty_score
                    descriptions.append((penalty_score,description))
            # else just take the penalty value from the second item of rule_compund
            else:
                rule, penalty_score, description = rule_compound
                if rule(word1, word2):
                    penalty += penalty_score
                    descriptions.append((penalty_score,description))



        # self._split_cache[split] = (penalty, descriptions)
        return penalty, descriptions

    #calculates the penalty of a line break in a given chunk"
    def penalty_line_break(self, chunk_start, chunk_end, line_break):

        # if the shorter line is shorter by this amount, it is penalized by the
        # below factor
        UNEVEN_LENGTH_CUT_RATIO = 0.7
        UNEVEN_LENGTH_PENALTY_FACTOR = 35

        charcount_before = sum(len(self.words[i].original) for i in range(chunk_start, line_break))
        charcount_after = sum(len(self.words[i].original) for i in range(line_break, chunk_end))
        if charcount_before == 0 or charcount_after == 0:
            return 100, "nonsense"
        penalty, description = self.penalty_single(line_break)
        #first line should be shorter
        if charcount_before > charcount_after:
            penalty += 4
        #one line should not be less than 0.5 of the other
        ratio = charcount_before / charcount_after;
        if ratio > 1:
            ratio = 1/ratio
        if ratio < UNEVEN_LENGTH_CUT_RATIO:
            penalty += (UNEVEN_LENGTH_CUT_RATIO - ratio) * UNEVEN_LENGTH_PENALTY_FACTOR
        longer_line = max([charcount_before, charcount_after])
        penalty += abs(((self.line_limit - longer_line)/2.0)**1.2)
        return penalty, description

    def clause_depth(self, word):
        depth = 0
        clause_roots = ("ccomp", "rcmod", "advcl", "cc") # cc: conjuntions, too? that will affect enumerations
        while word.syntax_relation != "ROOT":   #equivalent to word.syntax_parent != None
            if word.syntax_relation in clause_roots:
                depth += 1
            word = self.words[word.syntax_parent - self.words[0].index]
        return depth

