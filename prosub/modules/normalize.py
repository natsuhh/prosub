from prosub.data.data import Word, Sentence
from prosub.lib.contractions import contractions
from prosub.lib.module import Module
from prosub.modules import normalize_marytts
from prosub.modules import normalize_nltk

class NormalizeModule(Module):
    modulename = "normalize"
    variants = ("marytts", "nltk")

    def execute(self, config):
    	#submodule
        if self.variant == "nltk":
            submodule = normalize_nltk
        elif self.variant == "marytts":
            submodule = normalize_marytts

        # Run the input submodule
        submodule.execute(self, config)

        self.run.data.raw.normalized = "\n".join(word.normalized for word in self.run.data.words)
