from prosub.lib.module import Module

import difflib
import json
import logging
import nltk
import os
import pickle
import re
import subprocess
import sys


class AlignModule(Module):
    modulename = "align"

    def execute(self, config):
        tempfile_normalized = os.path.join(self.run.tempdir, "normalized")
        tempfile_aligned = os.path.join(self.run.tempdir, "aligned")
        tempfile_log = os.path.join(self.run.tempdir, "sphinx-log")

        with open(tempfile_normalized, 'w') as f:
            f.write(self.run.data.raw.normalized)

        cp = os.path.join("data", "aligner", config.jar or "sphinx4-samples.jar")
        if config.cp:
            cp += ':'+config.cp

        cmd = "java -Xmx{memory} -cp {cp} {cl} {audio} {normalized} {acousticModel} {dictionary} {g2p} {options} > {aligned} 2> {log}".format(
                memory=       config.memory,
                cp=           escape_arg(cp),
                cl=           escape_arg(config.className),
                audio=        escape_arg(self.run.config.input.audio),
                normalized=   escape_arg(tempfile_normalized),
                acousticModel=escape_arg(config.acousticModel),
                dictionary=   escape_arg(config.dictionary),
                g2p=          escape_arg(config.g2p),
                options=      config.options,
                aligned=      escape_arg(tempfile_aligned),
                log=          escape_arg(tempfile_log)
            )

        self.log("Running:", cmd, level="debug")
        if not config.skip:
            errorcode = subprocess.call(cmd, shell=True)

            if config["gzip-log"] and os.path.exists(tempfile_log):
                subprocess.call("gzip -f {path}".format(path=escape_arg(tempfile_log)), shell=True)

            if errorcode:
                self.log("Logfile: {}{}".format(tempfile_log, ".gz" if config["gzip-log"] else ""), level="error")
                raise ValueError("Aligner exited with status code {}, see log for details.".format(errorcode))

        with open(tempfile_aligned, 'r') as f:
            aligned = f.read()

        self.create_word_timings_map(aligned.splitlines())

        # for i, (orig, timing) in enumerate(timings):
        #     if not timing: timing = [None, None]
        #     self.run.data.words[i].start = timing[0]
        #     self.run.data.words[i].stop = timing[1]

    def create_word_timings_map(self, lines):
        """
        Get the output from Sphinx Aligner, parse it and match to the normalized
        tokens from the preprocessing step. Create a mapping from original words to
        timing information from this matching.

        :param list lines: A list of lines from the sphinx aligner output

        >>> lines = '''one                       [850:1080]
        ... zero                      [1080:1500]
        ... zero                      [1500:1920]
        ... '''.splitlines()
        >>> tokens_map = [('One ', 'one'), ('Zero ', 'zero'), ('Zero. ', 'zero')]
        >>> create_word_timings_map(lines, tokens_map)
        [('One ', (0.85, 1.08)), ('Zero ', (1.08, 1.5)), ('Zero. ', (1.5, 1.92))]
        """

        words = self.run.data.words
        #tokens_map = [(word.original, word.normalized) for word in words]

        lines_split = [line.split() for line in lines]
        # logging.info(pformat(lines))

        alinger_output_line_regex = r'^\s*(?P<symbol>[+-])?\s*(?P<token>.+?)(\s*\[(?P<start>\d+):(?P<stop>\d+)\])?\s*$'

        aligner_words = []
        aligner_timings = []
        for line in lines:
            match = re.match(alinger_output_line_regex, line)
            if not match:
                print("Line not matched: " + line)
                continue
            symbol, token, _, start, stop = match.groups()
            timing = None

            # we don't care about the added words
            #if symbol == "+": continue

            # tokens that couldnt be found by sphinx get mapped to timing info 'None'
            if symbol != '-':
                timing = (int(start), int(stop))

            aligner_words.append(token)
            aligner_timings.append(timing)


        # generate word list from preprocessed tokens
        # ["eigtthy-nine", "a b c"] -> ["eighty", "nine", "a", "b", "c"]
        #                     token_ids  [ 0           0    1     1    1]

        normalized_words = []
        preprocessed_word_token_ids = []
        for i, word in enumerate(words):
            # one word can be multiple word parts ("eighty-five" -> "eighty" "five")
            for wordpart in re.split(' |-', word.normalized):
                normalized_words.append(wordpart)
                preprocessed_word_token_ids.append(i)

        # debug outputs:
        # let us use the logging module: just use logging.{debug, info, warning, error}(foo) and read the output with tail -f log.log.
        # that way we don't have to remove or comment out all those print statements
        #self.log(list((aligner_words[i], aligner_timings[i]) for i in range(len(aligner_words))))
        #self.log(list((normalized_words[i], preprocessed_word_token_ids[i]) for i in range(len(normalized_words))))

        # compute a diff
        differ = difflib.SequenceMatcher(lambda x: 0, normalized_words, aligner_words)
        diff_ops = differ.get_opcodes()

        #function used to compute the timing when multiple words belong to one token
        def merge_timings(a, b):
            if a == None:
                return b
            elif b == None:
                return a
            else:
                return (min(a[0],b[0]), max(a[1],b[1]))

        def addCorrespondingTiming(word_id_a, word_id_b):
            # find timing and token id
            final_token_id = preprocessed_word_token_ids[word_id_a]
            timing = aligner_timings[word_id_b]
            # adjust the timing of the token with that id
            word = words[final_token_id]
            timing = merge_timings(timing, (word.start, word.stop) if (word.start != None and word.stop != None) else None)
            word.start, word.stop = timing[0] if timing else None, timing[1] if timing else None

        def join_words(word_list):
            text = ''
            ids = []
            first = True
            i = 0
            for word in word_list:
                if first:
                    first = False
                else:
                    text += ' '
                    ids.append(-1)
                for c in word:
                    text += c
                    ids.append(i)
                i += 1
            return (text, ids)

        for operation, a_start, a_end, b_start, b_end in diff_ops:
            # a is normalized_words and b is aligner_words
            if operation == 'equal':
                assert b_end - b_start == a_end - a_start, "equal sequences must have equal length"
                for i in range(a_end - a_start):
                    addCorrespondingTiming(a_start + i, b_start + i)
            elif operation == 'replace':
                # detect small changes like T V instead of tv and assign timing properly
                a_text,a_ids = join_words(normalized_words[a_start:a_end])
                b_text,b_ids = join_words(aligner_words[b_start:b_end])
                matches = difflib.SequenceMatcher(lambda x: x == ' ', a_text, b_text).get_matching_blocks();
                b_to_a = list([] for each in range(b_end-b_start))
                for j,k,n in matches:
                    for i in range(n):
                        a_id = a_ids[j+i]
                        b_id = b_ids[k+i]
                        if a_id >= 0 and b_id >= 0 and (a_id not in b_to_a[b_id]):
                            b_to_a[b_id].append(a_id)

                #self.log("{} to {}".format(a_text, b_text))
                #self.log(b_to_a)
                a_to_b = list(None for each in range(a_end-a_start))
                for i_b in range(len(b_to_a)):
                    if len(b_to_a[i_b]) == 1:
                        i_a = b_to_a[i_b][0]
                        cur = a_to_b[i_a]
                        if cur == None:
                            a_to_b[i_a] = (i_b, i_b)
                        else:
                            a_to_b[i_a] = (min(cur[0], i_b), max(cur[1], i_b))

                #self.log(a_to_b)

                for i_a in range(len(a_to_b)):
                    cur = a_to_b[i_a]
                    if cur == None:
                        continue
                    originalWord = normalized_words[a_start+i_a]
                    newWord = ''.join(aligner_words[b_start+cur[0]:b_start+cur[1]+1])
                    dist = nltk.metrics.edit_distance(originalWord, newWord)
                    #self.log('distance between "{}" and "{}" is {}'.format(originalWord, newWord, dist))
                    if dist/len(originalWord) > 0.4:
                        continue
                    for i_b in range(cur[0], cur[1]+1):
                        addCorrespondingTiming(a_start + i_a, b_start + i_b)
            #elif operation == 'delete':
                #Do nothing, timing info is missing
            #elif operation == 'insert':


def escape_arg(arg):
    return '"'+ arg.replace('\\','\\\\').replace('"','\\"') + '"'