from prosub.lib.module import Module

import subprocess
import os
import sys

class AnalyzeModule(Module):
    modulename = "analyze"

    def analyze(self):
        """Returns a string with some useful data, including the percentage of time-aligned words."""
        not_found =  sum(1 for word in self.run.data.words if word.start != None)
        word_count = len(self.run.data.words)
        if not word_count: return "There are no words for this."
        return "{}/{} words ({}%)".format(
                word_count - not_found,
                word_count,
                (1 - not_found / word_count) * 100)

    def execute(self, config):
        self.log(self.analyze(), level="info")
