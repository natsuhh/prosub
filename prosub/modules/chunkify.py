
from prosub.lib.module import Module
from prosub.data.data import Chunk
from prosub.modules import chunkify_naive
from prosub.modules import chunkify_greedy
from prosub.modules import chunkify_syntaxbased

import json
import os
import re
import subprocess
import sys
import queue


class ChunkifyModule(Module):
    """
    This module divides the transscrpt into subtitles (or chunks). One chunk specifies
    the words that appear at the same time on screen. Also line breaks are added. Variants:
    
    sytnaxbased: 
        This is done using syntax and timing information, some heuristics and a path finding 
        algotithm to find a global optimum.
    naive:
        TODO
    greedy:
        TODO
    """
    modulename = "chunkify"
    variants = ("syntaxbased", "greedy", "naive")

    def execute(self, config):

        data = self.run.data
        data.old_chunks = data.chunks
        data.chunks = []

        # Get the correct input submodule
        if self.variant == "syntaxbased":
            submodule = chunkify_syntaxbased
        elif self.variant == "naive":
            submodule = chunkify_naive
        elif self.variant == "greedy":
            submodule = chunkify_greedy
        else:
            submodule = chunkify_syntaxbased #is defualt

        submodule.execute(self, config)
