"""
Loads input text files and splits them into words. Does not normalize them. Supports different
formats, such as raw txt (simple transcript) or srt.
"""

from prosub.lib.module import Module
from prosub.modules import input_marytts
from prosub.modules import input_nltk
from prosub.data.data import Chunk

import nltk
import os
import re
import string
import sys

punctuation = string.punctuation.replace('.','')

class InputFormatError(Exception): pass

class InputModule(Module):
    modulename = "input"
    variants = ("marytts", "nltk")

    def execute(self, config):
        # Get the format
        fmt = config.format
        if isinstance(fmt, str): fmt = fmt.lower()
        if not fmt or fmt == "auto":
            _, fmt = os.path.splitext(self.run.config.input.transcript)
            fmt = fmt[1:]

        # Read the input file
        self.text = open(self.run.config.input.transcript, 'r').read()

        # Extract the plaintext using the input format and the raw file contents
        try:
            chunks = extract_plaintext(self.text, fmt)
        except InputFormatError as e:
            self.log(str(e), level="error")
            sys.exit(1)

        self.plaintext = '\n'.join(chunk[0] for chunk in chunks)

        # split text into sentences, generate token_maps for each
        data = self.run.data

        # Get the correct input submodule
        if self.variant == "marytts":
            submodule = input_marytts
        elif self.variant == "nltk":
            submodule = input_nltk

        # Run the input submodule
        submodule.execute(self, data, config)

        for i, word in enumerate(data.words):
            word.index = i

        data = self.run.data

        if config["import-chunking"]:
            map_chunks_to_data(data, chunks)

def map_chunks_to_data(data, chunks):
    words = data.words

    word_index = 0
    word_letter_index = 0
    chunk_letter_index = 0
    for chunk in chunks:
        #save the word start index
        word_index_start = word_index

        #take words until the following letter position is reached
        chunk_letter_index = chunk_letter_index + len(chunk[0])
        while word_index < len(words) and chunk_letter_index > word_letter_index:
            word_letter_index += len(words[word_index].original)
            word_index += 1

        prosub_chunk = Chunk(data, word_index_start, word_index)
        timing = chunk[1]
        if timing:
            prosub_chunk.start_time = timing[0]
            prosub_chunk.end_time = timing[1]
        data.chunks.append(prosub_chunk)

def extract_plaintext(text, fmt):
    if fmt == "txt":
        return [(re.sub(r"\s+", " ", text, flags=re.MULTILINE), None)]
    elif fmt == "srt":
        blocks = text.split('\n\n')
        result = []
        for block in blocks:
            block_parts = block.split('\n', 2)
            if len(block_parts) != 3:
                continue
            number, timing, content = block_parts
            timing_match = re.match(r'^\s*(.+?)\s*-->\s*(.+?)\s*$',timing)
            timing = None
            if timing_match:
                t1,t2 = timing_match.groups()
                timing = (parse_srt_timing(t1),parse_srt_timing(t2))
            result.append((content, timing))

        return result
    else:
        raise InputFormatError("Input format not implemented: {}".format(fmt))

def parse_srt_timing(text):
    hours,minutes,seconds,milliseconds = re.match(r'(\d+):(\d+):(\d+),(\d+)',text).groups()
    return int(milliseconds) + int(seconds)*(1000) + int(minutes)*(60*1000) + int(hours)*(60*60*1000)
