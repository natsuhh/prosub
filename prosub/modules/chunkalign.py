from prosub.lib.module import Module

class ChunkAlignModule(Module):
    """
    The goal of this module is to set the Chunk.start_time and Chunk.end_time
    properties. This is mainly computed from the timings of the contained words,
    using some extension rules, like a minimum length and adjacent chunk timings.
    """

    modulename = "chunkalign"

    def execute(self, config):
        chunks = self.run.data.chunks

        # Find basic chunk length
        for i, chunk in enumerate(chunks):
            start_times = [word.start for word in chunk.words if word.start != None]
            chunk.start_time = min(start_times) if start_times else None

            stop_times  = [word.stop for word in chunk.words if word.stop != None]
            chunk.end_time = max(stop_times) if stop_times else None

        # Make small chunks longer
        for i, chunk in enumerate(chunks):
            linecount = sum(len(word.original) for word in chunk.words) / 40
            # at least 1.5 seconds, or 3 seconds per line
            desired_length = round(max(1500, 3000 * linecount))

            # If it is too small, extend the end
            if chunk.length < desired_length:
                ends = [chunk.start_time + desired_length]

                # do not extend longer than the next chunk
                if i < len(chunks) - 1:
                    ends.append(chunks[i + 1].start_time)

                prev = chunk.end_time
                chunk.end_time = min(ends)

                if prev != chunk.end_time:
                    self.log("Extended the end of chunk: {}".format(chunk.text), level="info")

            # If it is still too small, extend the start
            if chunk.length < desired_length:
                starts = [chunk.end_time - desired_length, 0]

                # do not extend further back than the end of the previous chunk
                if i > 0:
                    starts.append(chunks[i - 1].end_time)

                prev = chunk.start_time
                chunk.start_time = max(starts)

                if prev != chunk.start_time:
                    self.log("Extended the start of chunk: {}".format(prev, chunk.start_time, chunk.text), level="info")
