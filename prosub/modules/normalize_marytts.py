from prosub.data.data import Word, Sentence
from prosub.lib.contractions import contractions
from prosub.lib.module import Module

import prosub.lib.marytts as marytts
import re


def execute(self, config):
    self.log("Using maryTTS for normalization", level="info")

    language = self.run.config.language
    prosub_tokens = self.run.data.words
    for t in prosub_tokens:
        t.normalized = t.original

    input_text, _ = marytts.tokens_to_text_and_tokenization(prosub_tokens)
    preprocessed_text = marytts.preprocess(input_text, language) # ersetzt m -> Meter etc.
    xml_string = marytts.requestXML(preprocessed_text, 'WORDS', language, config["mary-url"])
    mary_output = marytts.parseXML(xml_string)
    mary_output = marytts.postprocess(mary_output, language)

    # match the normalized tokens with the original tokens
    marytts.attach_match(prosub_tokens, mary_output.tokens, self.log)
    
    cleanup(prosub_tokens, self.log)


def cleanup(tokens, log):
    """delete punctuation"""
    for token in tokens:
        if token.normalized == None:
            token.normalized = ''
            log("Token {} was None".format(token.index), level="debug")
        token.normalized = re.sub(r'[^\w]+',' ',token.normalized, flags=re.MULTILINE)
        token.normalized = token.normalized.strip()
