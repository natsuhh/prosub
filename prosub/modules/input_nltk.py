
from prosub.lib.module import Module
from prosub.data.data import Word, Sentence

import nltk
import os
import re
import string
import sys

def execute(self, data, config):
    for sentence in split_sentences(self.plaintext):
        start_word = len(data.words)
        for original in preprocess(sentence):
            data.words.append(Word(original))
        end_word = len(data.words)

        data.sentences.append(Sentence(start_word, end_word))

def split_sentences(text):
    sent_detector = nltk.data.load('tokenizers/punkt/english.pickle')
    spans = sent_detector.span_tokenize(text.strip())
    # we want spaces after sentence end
    return [text[start:end+1] for start, end in spans]

def tokenize(text):
    """
    This tokenizes the input text with NLTK's PunktWordTokenizer.

    >>> tokenize("It's 1984.")
    ['It', "'s ", '1984. ']

    It splits contractions and keeps whitespace, so that we can join the result back together and have the input string.
    """

    # in order to make the logic beneath work, we need to have the text ending with ' '
    if text[-1] != ' ': text += ' '

    # this gives us the start and end char positions of each word
    words_pos = nltk.tokenize.punkt.PunktWordTokenizer().span_tokenize(text)

    # unfortunately, it doesn't give us everything the way we need it - with respect to whitespace. that's why we have to manually lengthen some spans.
    tokens = []
    for start,end in words_pos:
        if text[end] in [' ','\n']:
            w = text[start:end+1]
        else:
            w = text[start:end]

        tokens.append(w)

    return tokens


def join_splitted_tokens(tokens):
    """
    We need "I've" etc. as *one* token, to be further processed by :meth:`preprocess.expand_contractions`.

    The PunktWordTokenizer gives them as two tokens ("I", "'ve"), so we have to join them together again.

    **TODO**: It *might* be more elegant to rewrite the whole thing without NLTK, because we are undoing quite a lot of what it does..

    >>> join_splitted_tokens(["I", "'ll", "go"])
    ["I'll", 'go']
    """

    i = 0
    tokens_new = []
    while i<len(tokens):
        token = tokens[i]
        next_token = tokens[i+1] if i<len(tokens)-1 else ''
        if "'" in next_token:
            join = token + next_token
            tokens_new.append(join)
            i += 1
        elif any(char in next_token for char in punctuation):
            join = token + next_token
            tokens_new.append(join)
            i += 1
        else:
            tokens_new.append(token)

        i += 1

    return tokens_new

def preprocess(text):
    """
    This is the main function of the module.

    Maps from the original text to a list of words (strings). A word might be normalized later to multiple words. It still is one Word object.

    :param text: the raw input text from the `*.txt` as a string

    >>> preprocess("I'll go.")
    ["I'll " "go. "]
    """

    tokens = tokenize(text)
    tokens = join_splitted_tokens(tokens)
    return tokens

