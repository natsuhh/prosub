"""
This adapts Peter Norvig 23-line spellchecker (http://norvig.com/spell-correct.html)
It should run after `preprocess` but before `align`.

TODO:
- make it a module
- if a word gets corrected, change it, and place a 'misspelled' field with the original into
  the word dict
"""

from prosub.lib.module import Module
from prosub.lib.getch import getch

import collections
import os
import re
import subprocess
import sys

class SpellcheckModule(Module):
    """
    interactively check the original words for spelling errors and offer suggestions.
    Possibly update word.original, and save the `misspelled word` in `word.misspelled`.
    """
    modulename = "spellcheck"

    def execute(self, config):
        big_data = open('data/spellcheck/big.txt').read().lower()
        words = re.findall('[a-z]+', big_data)
        global NWORDS
        NWORDS = train(words)

        ignore_words = []
        ignore_path = config.ignore.format(name=self.run.name)
        if os.path.exists(ignore_path):
            with open(ignore_path) as f:
                ignore_words = f.read().splitlines()

        # # This is interactive, so allow `print`
        print('\npress \n- <enter> for accepting the correction, \n- <space> for rejecting, \n- <esc> for aborting spellchecking.\n')
        for word in self.run.data.words:

            # ignore words from ignore_words
            if word.original.strip() in ignore_words:
                continue

            # we don't look at original words with ' in them
            if "'" in word.original:
                continue

            # we don't look at numbers
            if re.match(r'[\d]+', word.original):
                continue

            regex = r'([\w]+)'
            if re.match(regex, word.original):
                # get correction from _correct function
                word_corrected = re.sub(regex, _correct, word.original)
                if word_corrected != word.original:
                    print(('{} -> {}'.format(
                        word.original.strip('\n'), word_corrected.strip('\n'))),
                        end='', flush=True)
                    answer = getch()

                    # if user presses escape, abort
                    if ord(answer) == 27:
                        print('\n\naborting spellcheck.')
                        break

                    # if user presses enter, accept
                    elif ord(answer) == 13:
                        print(' --> corrected.')
                        word.misspelled = word.original
                        word.original = word_corrected

                    # if user presses space, don't accept and ignore
                    else:
                        print(' --> not corrected + ignored in future.')
                        ignore_words.append(w)
        self.log([word.original for word in self.run.data.words])

alphabet = 'abcdefghijklmnopqrstuvwxyz'

def train(features):
    model = collections.defaultdict(lambda: 1)
    for f in features:
        model[f] += 1
    return model


def edits1(word):
    s = [(word[:i], word[i:]) for i in range(len(word) + 1)]
    deletes    = [a + b[1:] for a, b in s if b]
    transposes = [a + b[1] + b[0] + b[2:] for a, b in s if len(b)>1]
    replaces   = [a + c + b[1:] for a, b in s for c in alphabet if b]
    inserts    = [a + c + b     for a, b in s for c in alphabet]
    return set(deletes + transposes + replaces + inserts)

def known_edits2(word):
    return set(e2 for e1 in edits1(word) for e2 in edits1(e1) if e2 in NWORDS)

def known(words):
    return set(w for w in words if w in NWORDS)

def correct(word):
    candidates = known([word]) or known(edits1(word)) or  known_edits2(word) or [word]
    return max(candidates, key=NWORDS.get)

def _correct(match):
    # without symbols and stuff
    w = match.group(1)
    if w[0].isupper():
        return correct(w.lower()).capitalize()
    else:
        return correct(w)

