"""
The purpose of this module is to generate JSON and HTML for consumption by the visualization; from the word timing information.
"""
from prosub.lib.module import Module
from prosub.lib.helpers import to_sec

import cgi
import os
import shutil
import subprocess
import sys

class PlaybackModule(Module):
    modulename = "playback"

    def execute(self, config):
        outdir = os.path.join("data", "playback")

        audio = os.path.abspath(self.run.config.input.audio)
        _, audiofile = os.path.split(audio)
        audioname, audioext = os.path.splitext(audiofile)

        # Symlink the audio file
        audiotarget = os.path.join(outdir, "audio" + audioext)
        if os.path.exists(audiotarget):
            os.remove(audiotarget)
        os.symlink(audio, audiotarget)

        # Write data.json
        self.run.save_data(os.path.join(outdir, "data.json"))
        self.run.save_config(os.path.join(outdir, "config.json"))


