from prosub.data.data import Word, Sentence
from prosub.lib.contractions import contractions
from prosub.lib.module import Module

import numword.numword_en as numword
import re
import string
import subprocess
import sys

def execute(self, config):
    self.log("Using nltk for normalization", level="info")

    for word in self.run.data.words:
        word.normalized = normalize(word.original)


def num2word(token):
    """
    Convert a number to its textual representation.
    It tries to evaluate in the following order, regexing the token for matches:
     - ordinal
     - year
     - cardinal
     - currency

    >>> num2word('1987')
    'nineteen hundred and eighty-seven'
    >>> num2word('42rd')
    'forty-second'

    """

    # ordinals
    match = re.search(r'(\d+)[rd|th|\.|st]', token)
    if match: return numword.ordinal(int(match.group(1)))

    # year
    match = re.search(r'(\d\d\d\d)', token)
    if match: return numword.year(int(match.group(1)))

    # cardinals
    match = re.search(r'(\d+)', token)
    if match: return numword.cardinal(int(match.group(1)))

    # currency
    match = re.search(r'(\d+)$', token)
    if match: return numword.currency(int(match.group(1)))

    return token


def expand_contractions(token):
    """
    Replace contracted words with expanded versions, taken from a dictionary in `contractions.py`

    >>> expand_contractions("I'll")
    'i will'
    >>> expand_contractions("shouldn't've")
    'should not have'
    """

    return contractions.get(token.lower()) or token.replace("'",'')


def normalize(token):
    """
    Normalize a token:
    - remove punctuation and trailing space
    - lowercase
    - :meth:`preprocess.num2word` it
    - :meth:`preprocess.expand_contractions` on it, if they exist

    >>> normalize("I'll ")
    'i will'

    >>> normalize('3rd ')
    'third'

    >>> normalize('Foo? ')
    'foo'
    """


    token = ''.join( c for c in token if c not in ' .:?!,"\n')
    token = token.lower()
    token = num2word(token)
    if "'" in token:
        token = expand_contractions(token)
    return token