from prosub.data.data import Word, Sentence
from prosub.lib.module import Module

import os
import prosub.lib.marytts as marytts
import re


def execute(self, data, config):
    input_text = self.plaintext
    langugage = self.run.config.language

    #TODO: find annotations in brackets:
    # re.findall(r'(?:^|\s)(\[[^\n\[\]]{3,80}\])(?:\s|$)', '[test]')

    tempfile_mary_xml_words = os.path.join(self.run.tempdir, "mary_words.xml")

    xml_string = marytts.requestXML(input_text, 'WORDS', langugage, config["mary-url"])

    with open(tempfile_mary_xml_words, 'w') as f:
        f.write(xml_string)

    mary_output = marytts.parseXML(xml_string)
    original_text_start, original_text_end = marytts.match(mary_output, input_text, lambda arg:self.log(arg))

    # match mary_output with original text.  Output should not differ, but mary
    # replaces some special characters (hyphens) and we want to keep those
    
    # use the start position instead of the end position, because doing so implicitly appends any text outside a token to the previous one 
    original_text_start[0] = 0
    end = len(input_text)
    words_reversed = []
    for start in reversed(original_text_start):
        if end > start:
            words_reversed.append(Word(input_text[start:end]))
            end = start
        else:
            words_reversed.append(Word(""))

    data.words = list(reversed(words_reversed)) # double reversed :)

    for sentence in mary_output.sentences:
        data.sentences.append(Sentence(sentence[0], sentence[1]))
