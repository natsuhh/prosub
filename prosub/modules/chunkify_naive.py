
from prosub.data.data import Chunk

def execute(self, config):
    data = self.run.data
    data.chunks = []

    def char_len(list_of_words): return len(''.join(word.original for word in list_of_words))

    start_index = 0

    def add_line(end_index):
        nonlocal start_index
        if start_index >= end_index:
            return
        chunk = Chunk(data, start_index, end_index)
        data.chunks.append(chunk)
        start_index = end_index

    for index, word_info in enumerate(data.words):
        word = word_info.original
        line = data.words[start_index:index+1]

        # break after those chars
        if any(char in word for char in '.,:?!'):
            add_line(index+1)

        # break before 'and' and 'or'
        elif any(w == word for w in ['and ', 'or ']) and char_len(line) > 25:
            add_line(index+0)

        # break when the char length that amara recommends is reached
        elif char_len(line) > 42:
            add_line(index+0)

        # no dangling articles
        elif char_len(line) > 35 and word == 'the ':
            add_line(index+0)

    add_line(len(data.words));
