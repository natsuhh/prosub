#!/usr/bin/env python3

import argparse

parser = argparse.ArgumentParser(description='Process audio and transcript into subtitles.')
parser.add_argument('--config', '-c', type=str, help='path to base configuration file')
parser.add_argument('--name', '-n', type=str, help='name of the run, shortcut for -o name=NAME')
parser.add_argument('--audio', '-a', type=str, help='audio input file, shortcut for -o input.audio=AUDIO')
parser.add_argument('--transcript', '-t', type=str, help='audio input file, shortcut for -o input.transcript=TRANSCRIPT')
parser.add_argument('--option', '-o', type=str, nargs='*', action='append', default=[], help='overwrite configuration options')
parser.add_argument('--verbose', '-v', action='store_const', const=True, default=False, help='verbose output, shrotcut for -o loglevel=verbose')
parser.add_argument('--list-modules', '-m', action='store_const', const=True, default=False, help='lists available modules and exits')

import prosub.lib
import prosub.modules

def main():
    args = parser.parse_args()

    if args.list_modules:
        for name, module in prosub.lib.module.all_modules.items():
            print(name)
        return

    run = prosub.lib.module.Run()

    # load configuration
    run.load_config("config/default.json")
    if args.config: run.load_config(args.config)
    if args.name: run.set_config("name", args.name)
    if args.audio: run.set_config("input.audio", args.audio)
    if args.transcript: run.set_config("input.transcript", args.transcript)
    if args.verbose: run.set_config("loglevel", "verbose")
    for option in sum(args.option, []):
        key, value = option.split("=")
        run.set_config(key, value)

    run.prepare()
    run.execute()

# VM Size
# Data name
# Spellcheck

#preprocess/preprocess $obj $spellcheck
#aligner/sphinx-aligner $obj $vm_size
#$py postprocess/create_word_timings_map.py $obj
#playback/make $obj

