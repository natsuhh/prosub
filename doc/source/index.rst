.. Subtitles documentation master file, created by
   sphinx-quickstart on Mon Oct 27 13:03:31 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Subtitles's documentation!
=====================================

Contents:

.. toctree::
   :maxdepth: 10

   prosub/modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

