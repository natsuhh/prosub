Hallo ihr,

wir haben Feedback gesammelt und euch zur Orientierung auch einen alten
Projektbericht angehängt (der natürlich auch nicht in allen Belangen
perfekt ist — insbesondere fehlt die Literaturarbeit, aber die war
damals in dem zugehörigen Paper…).


Es fehlt eine Einordnung in das allgemeine Themenfeld sowie Referenzen
auf schon vorhandene Arbeiten (insbesondere die, auf die zurückgegriffen
wird, Long Audio Alignment z.B.).


Die Einleitung ist ziemlich speziell auf 30C3(sic) gemünzt.

In die Einleitung gehört auch eine Übersicht über den restlichen Text.

Ihr solltet aufpassen, keinen Verlaufsbericht zu schreiben, z.B.

.---------------------------------------------------------------.
| Zuerst waren diese einzelnen Module als eigenständige Skripte |
| implementiert                                                 |
`---------------------------------------------------------------'

Zudem auf die richtige Form achten, nicht so etwas schreiben:

.-----------------------------------.
| Ein allgemeines System musste her |
`-----------------------------------'

.-----------------------------------------------.
| Zuerst hat Jonathan eine Version geschrieben, |
`-----------------------------------------------'

(Obiges ist zudem Verlaufsbericht)

Beim Parser:

.-------------------------------------------------------------------------.
| Code der mit den Ergebnissen arbeitet muss also nicht verändert werden, |
| wenn mehr Sprachen als nur Englisch unterstütz werden sollen.           |
`-------------------------------------------------------------------------'

Gewagte These. Ich glaube, ihr habt da einige Worte hart kodiert ;-)


> Verfahren zur Aufteilung in Chunks:

Wenn ihr mehrere Ansätze vergleichen wollt, nicht Verlaufsform nutzen,
sondern Baseline mit ausgefuxter Methode vergleichen.


.-------------------------------------------------------------------------.
| Die Ergebnisse mit dieser Methode waren schon überraschend brauch- bar, |
| konnten aber diverse Fälle nicht abdecken.                              |
`-------------------------------------------------------------------------'

Nicht einfach Behauptungen aufstellen, Auswertungen machen! Qualitativ
oder Quantitativ. (Ersteres ist Beispiel bringen und zeigen, dass die
eine Methode funktioniert und die andere nicht). Zudem analysieren,
*warum* etwas nicht funktioniert.



> Optimale Lösung mit einer Dijkstra-Variante


Es wird nicht erklärt, warum die Penalty so ist wie sie ist und was sie
erreichen soll.



Zusammenfassend fehlt:
 - Related Work (oben schon gesagt)
 - Evaluation & Einordnung
 - Zusammenfassung
 - Ausblick


Wenn ihr möchtet, können wir gerne nochmal in Person über die Arbeit
sprechen. Timo und ich sind zu den normalen Zeiten eigentlich immer in
F-4XX. Ihr könnt euch auch gerne bei uns im Flur treffen und uns bei
Bedarf heranziehen.


Viele Grüße
  Arne und Timo.
