
# Einleitung
- Die Einleitung ist ziemlich speziell auf 30C3(sic) gemünzt.

- Es fehlt eine Einordnung in das allgemeine Themenfeld sowie Referenzen
auf schon vorhandene Arbeiten (insbesondere die, auf die zurückgegriffen
wird, Long Audio Alignment z.B.).
  --> JONA: hat da jmd noch Erinnerungen an relevante Literatur o.ä, die wir hier noch einfügen könnten??

# Kein Verlaufsbericht
- Ihr solltet aufpassen, keinen Verlaufsbericht zu schreiben,
  --> JONA: ok, das war dann wohl Misskommunikation. war ja irgendwie unsere Prämisse als wir das geschrieben haben.

    .-------------------------------------------------------------------------.
    | Code der mit den Ergebnissen arbeitet muss also nicht verändert werden, |
    | wenn mehr Sprachen als nur Englisch unterstütz werden sollen.           |
    `-------------------------------------------------------------------------'
    Gewagte These. Ich glaube, ihr habt da einige Worte hart kodiert ;-)

- Verfahren zur Aufteilung in Chunks:
  - Wenn ihr mehrere Ansätze vergleichen wollt, nicht Verlaufsform nutzen,
  sondern Baseline mit ausgefuxter Methode vergleichen.

# Auswertung
Nicht einfach Behauptungen aufstellen, Auswertungen machen! Qualitativ
oder Quantitativ. (Ersteres ist Beispiel bringen und zeigen, dass die
eine Methode funktioniert und die andere nicht). Zudem analysieren,
*warum* etwas nicht funktioniert.

---> JONA: Foo.. Daten anyone?

# Optimale Lösung mit einer Dijkstra-Variante
  Es wird nicht erklärt, warum die Penalty so ist wie sie ist und was sie
  erreichen soll.

# Letztes Kapitel
 - Zusammenfassung
 - Ausblick
