#!/usr/bin/env python3
import re

def seconds(t):
    regex = r'(\d\d)\:(\d\d):(\d\d),(\d\d\d)'
    m = re.match(regex, t)
    return int(m.group(1)) * 3600 + int(m.group(2)) * 60 + int(m.group(3)) + int(m.group(4)) * 0.001

files = ['a.txt', 'b.txt']
offsets = [-seconds('00:19:06,160'), -seconds('00:00:00,750')]
labels = ['Referenz (ohne Zeilenumbrüche)', 'Unsere Version']

output = r'''
\tikzset{chunk/.style={draw,thin}}
\begin{tikzpicture}[font=\sffamily\small]
'''

yscale = 0.3
xscale = 8
miny = 0

for col, (file, offset) in enumerate(zip(files, offsets)):
    with open(file) as f:
        lines = f.readlines()

    for line in lines:
        start, stop, text = line.split(' ', 2)

        text = text.strip().replace('\\n', r' \textcolor{red}{\textbf{/}} \\ ')

        x = col * xscale
        x2 = x + xscale
        y = -(seconds(start) + offset) * yscale
        y2 = -(seconds(stop) + offset) * yscale
        output += r'    \draw[chunk] ({x}, {y}) rectangle ({x2}, {y2});'.format(x=x, y=y, x2=x2, y2=y2)
        output += '\n'
        output += r'    \node[text width=7cm,align=center,font=\sffamily\scriptsize] at ({x}, {y}) {{{text}}};'.format(x=0.5*(x+x2), y=0.5*(y+y2), text=text)
        output += '\n'
        miny = min(miny, y, y2)

for col, label in enumerate(labels):
    x = (0.5+col) * xscale
    output += r'    \node[font=\sffamily\small] at ({x}, {y}) {{{text}}};'.format(x=x, y=0.5, text=label)
    output += '\n'

output += r'\draw[line width=3pt,draw=gray,->] ({x}, {y}) -> ({x}, {y2});'.format(x=xscale, y=0.5, y2=miny-0.5)
output += r'\node[anchor=north] at ({x}, {y}) {{time}};'.format(x=xscale, y=miny-0.5)

output += r'''
\end{tikzpicture}
'''

print(output)
