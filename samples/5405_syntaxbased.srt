0
00:00:00,000 --> 00:00:00,000
Thank you very much can you.. 

1
00:00:00,000 --> 00:00:00,000
You can hear me? 

2
00:00:00,000 --> 00:00:00,000
Yes! 

3
00:00:00,000 --> 00:00:00,000
I've been at this now 23 years 
we
worked with my colleagues and I

4
00:00:00,000 --> 00:00:00,000
've worked in about 30 countries with 
advised 9 truth
commissions 

5
00:00:00,000 --> 00:00:00,000
official truth commissions for UN missions 
for international criminal tribunals

6
00:00:00,000 --> 00:00:00,000
We have testified in four different cases 
to internationally to domestically 

7
00:00:00,000 --> 00:00:00,000
and we've
advised dozens and dozens of 
non-governmental human-rights groups around the world. 

8
00:00:00,000 --> 00:00:00,000
The
point of this stuff is to figure out 
how to bring the knowledge of the people who

9
00:00:00,000 --> 00:00:00,000
've suffered
human rights violations to bear On 
demanding accountability from the perpetrators. 

10
00:00:00,000 --> 00:00:00,000
Our
job is to figure out 
how we can tell the truth. 

11
00:00:00,000 --> 00:00:00,000
It is on of the moral foundations of the 
international
human rights movement 

12
00:00:00,000 --> 00:00:00,000
that we speak truth to power we look in 
the face of the powerful
and we tell 

13
00:00:00,000 --> 00:00:00,000
them what we believe they have done 
that is wrong if that's gonna work, 

14
00:00:00,000 --> 00:00:00,000
we
have to speak the truth we have to be right, 
we have to get the analysis on. 

15
00:00:00,000 --> 00:00:00,000
That's not
always easy 
and to get there they're 

16
00:00:00,000 --> 00:00:00,000
sort of three themes that 
I wanna try to touch
in this talk. 

17
00:00:00,000 --> 00:00:00,000
Since the talk is pretty short 
I'm really gonna touch on two of them 

18
00:00:00,000 --> 00:00:00,000
so at
the very end of the talk all 
invite people like to talk more about the 

19
00:00:00,000 --> 00:00:00,000
specifically technical
aspects of this 
work about classifiers about clustering 

20
00:00:00,000 --> 00:00:00,000
about statistical estimation 
about
database techniques. 

21
00:00:00,000 --> 00:00:00,000
People wanna talk about that I
'd love to gather and will try to find
a space. 

22
00:00:00,000 --> 00:00:00,000
I've been fighting with the wiki for two days 
I think I'm probably not the
only one. 

23
00:00:00,000 --> 00:00:00,000
We can gather we can talk about 
that stuff more in detail. 

24
00:00:00,000 --> 00:00:00,000
So today, in the
next 25 minutes 
I'm going to focus specifically on 

25
00:00:00,000 --> 00:00:00,000
the trial of General Jose Efrain Rios Montt

who ruled Guatemala from march 1982 until August 1983. 

26
00:00:00,000 --> 00:00:00,000
That's general Rios there 
in
the upper corner in the red tie. 

27
00:00:00,000 --> 00:00:00,000
During the government of 
general Rios Montt tens of thousands
of 

28
00:00:00,000 --> 00:00:00,000
people were killed by 
the army of Guatemala and the question 

29
00:00:00,000 --> 00:00:00,000
that has been facing guatemalan

since that time is, 

30
00:00:00,000 --> 00:00:00,000
did the pattern of killing that the 
army committed constitute acts and
genocide. 

31
00:00:00,000 --> 00:00:00,000
Now genocide is a very specific crime in 
international law. 

32
00:00:00,000 --> 00:00:00,000
It does not mean
you killed a lot of people, 
there are other war crimes for mass killing. 

33
00:00:00,000 --> 00:00:00,000
Genocide specifically
means 
that you picked out a particular group 

34
00:00:00,000 --> 00:00:00,000
and to the exclusion of other groups nearby

them you focused on eliminating that group. 

35
00:00:00,000 --> 00:00:00,000
That's key because for statistician 
that gives
us a 

36
00:00:00,000 --> 00:00:00,000
hypothesis we can test 
which is what is the relative risk, 

37
00:00:00,000 --> 00:00:00,000
what is the differential
probability 
of people in the target group 

38
00:00:00,000 --> 00:00:00,000
being killed relative to their neighbors 
who
are not in the target group. 

39
00:00:00,000 --> 00:00:00,000
So without further ado let
's look at the relative risk of being
killed for 

40
00:00:00,000 --> 00:00:00,000
indigenous people in the 
three rural counties of Chajul, Cotzal 

41
00:00:00,000 --> 00:00:00,000
and Nebaj
relative to their non-indigenous 
relative to their non-indigenous neighbors. 

42
00:00:00,000 --> 00:00:00,000
We haven't
all talked in 
a moment about how we have this, 

43
00:00:00,000 --> 00:00:00,000
we have information on evidence in estimations
of 
the death of about 2150 indigenous people. 

44
00:00:00,000 --> 00:00:00,000
People killed by the army in 
the period of
the government of General Rios. 

45
00:00:00,000 --> 00:00:00,000
The population the 
total number of people alive who 

46
00:00:00,000 --> 00:00:00,000
were
Indigenous in those counties in 
the census of 1981 is about 39,000. 

47
00:00:00,000 --> 00:00:00,000
So the approximate
crude 
mortality rate due to homicide by 

48
00:00:00,000 --> 00:00:00,000
the army is 5.5 percent for indigenous 
people
in that period. 

49
00:00:00,000 --> 00:00:00,000
Now that's relative to the homicide rate for 
non-indigenous people in
the same place of approximately .

50
00:00:00,000 --> 00:00:00,000
7 percent. 

51
00:00:00,000 --> 00:00:00,000
So what we ask is what is 
the ratio between
those two numbers? 

52
00:00:00,000 --> 00:00:00,000
And the ratio between those two numbers 
is the relative risk it's approximately
8. 

53
00:00:00,000 --> 00:00:00,000
We interpret that as if you were an 
indigenous person alive in one of those three counties
in 1982, 

54
00:00:00,000 --> 00:00:00,000
your probability of being killed by the army 
was eight times greater, 

55
00:00:00,000 --> 00:00:00,000
than
a person also living in those three counties 
who was not indigenous. 

56
00:00:00,000 --> 00:00:00,000
Eight times, eight
times! 

57
00:00:00,000 --> 00:00:00,000
To put that in relative 
terms the probability, the 

58
00:00:00,000 --> 00:00:00,000
relative risk of being in Bosnia 
relative
to being served in Bosnia 

59
00:00:00,000 --> 00:00:00,000
during the war in Bosnia was a a 
little less than three. 

60
00:00:00,000 --> 00:00:00,000
So
your relative risk of being indigenous 
was more than twice nearly 

61
00:00:00,000 --> 00:00:00,000
three times as much
as your 
relative risk of being Bosnia in the Bosnian war. 

62
00:00:00,000 --> 00:00:00,000
It's an 
astonishing level of
focus. 

63
00:00:00,000 --> 00:00:00,000
It shows a 
tremendous planning and coherence I believe. 

64
00:00:00,000 --> 00:00:00,000
So, again coming back
to the statistical conclusion, 
how do we come to that? 

65
00:00:00,000 --> 00:00:00,000
How do we find that information 
how
do we make that conclusion? 

66
00:00:00,000 --> 00:00:00,000
First we're 
only looking at homicides committed by the army
we're 

67
00:00:00,000 --> 00:00:00,000
not looking at homicides committed by 
other parties by the guerrillas by 

68
00:00:00,000 --> 00:00:00,000
private
actors, we're not looking at 
excess mortality, the mortality 

69
00:00:00,000 --> 00:00:00,000
that we might find in conflict

and is in excess of normal peacetime mortality. 

70
00:00:00,000 --> 00:00:00,000
We're not looking at any of that, 
only homicide.

71
00:00:00,000 --> 00:00:00,000
And the percentage relates the number 
people killed by the army with the population 

72
00:00:00,000 --> 00:00:00,000
that
was alive that's 
crucial here we're looking at rates and we

73
00:00:00,000 --> 00:00:00,000
're comparing the rate of the

indigenous people shown in the 

74
00:00:00,000 --> 00:00:00,000
blue bar to non-indigenous people 
shown in the green bar.

75
00:00:00,000 --> 00:00:00,000
The width of the bar show the 
relative populations in each of those two communities. 

76
00:00:00,000 --> 00:00:00,000
So clearly
the there are many more indigenous people 
but a higher fraction of them are also killed.

77
00:00:00,000 --> 00:00:00,000
The bars also show something else and that's 
what I focus on for the rest of the talk 

78
00:00:00,000 --> 00:00:00,000
there
are two sections to each of 
the two bars, a dark section on the 

79
00:00:00,000 --> 00:00:00,000
bottom a lighter section
on top 
and what that indicates is what 

80
00:00:00,000 --> 00:00:00,000
we know in terms of being able 
to name people
with their first and last 

81
00:00:00,000 --> 00:00:00,000
name their location and dates of death 
and what we must infer
statistically. 

82
00:00:00,000 --> 00:00:00,000
Now I'm beginning to touch on the 
second theme of my talk: 

83
00:00:00,000 --> 00:00:00,000
Which is the
when we are studying 
mass violence in war crimes, 

84
00:00:00,000 --> 00:00:00,000
we cannot do statistical or pattern
analysis with 
raw information we must use the tools of 

85
00:00:00,000 --> 00:00:00,000
mathematical statistics 
to understand
what we don't know. 

86
00:00:00,000 --> 00:00:00,000
The information which cannot 
be observed directly we have to estimate

87
00:00:00,000 --> 00:00:00,000
that in order to control for the process of the 
production information Information 

88
00:00:00,000 --> 00:00:00,000
doesn't
just fall out of the sky, 
the way it does for industry. 

89
00:00:00,000 --> 00:00:00,000
If I'm running an ISP I know
every packet 
that runs through my routers. 

90
00:00:00,000 --> 00:00:00,000
That's 
not how the social world works. 

91
00:00:00,000 --> 00:00:00,000
In
order to find information about killings 
we have to hear about that killing from someone,

92
00:00:00,000 --> 00:00:00,000
we have to investigate, we have 
to find the human remains and if we ca

93
00:00:00,000 --> 00:00:00,000
n't observe the
killing we won't hear 
about it and many killings are hidden. 

94
00:00:00,000 --> 00:00:00,000
In my team we have a a 
kind of
catch phrase: 

95
00:00:00,000 --> 00:00:00,000
that the world if a 
lawyer is killed in a big city at 

96
00:00:00,000 --> 00:00:00,000
high noon the world
knows 
about it before dinner time. 

97
00:00:00,000 --> 00:00:00,000
Every single time. 

98
00:00:00,000 --> 00:00:00,000
But when a rural peasant is killed 
three
days walk from a road in 

99
00:00:00,000 --> 00:00:00,000
the dead of night, 
were unlikely to ever hear. 

100
00:00:00,000 --> 00:00:00,000
And technology
is not changing this 
I'll talk later about 

101
00:00:00,000 --> 00:00:00,000
that technologies is 
actually making the problem
worse. 

102
00:00:00,000 --> 00:00:00,000
So, let's get back to Guatemala 
and just conclude that the little vertical bars

103
00:00:00,000 --> 00:00:00,000
little vertical lines at the top of 
each bar indicate the confidence interval. 

104
00:00:00,000 --> 00:00:00,000
Which is
similar to what lay people 
sometimes call a margin of error. 

105
00:00:00,000 --> 00:00:00,000
It is our level of uncertainty

about each of those estimates 

106
00:00:00,000 --> 00:00:00,000
and you noticed the uncertainty is 
much much smaller the difference
between the 

107
00:00:00,000 --> 00:00:00,000
two bars the uncertainty does not affect 
our ability to draw the conclusion,

108
00:00:00,000 --> 00:00:00,000
that there was a spectacular difference in 
the mortality rates between 

109
00:00:00,000 --> 00:00:00,000
the people who
were the 
hypothesized target of genocide and does who were not. 

110
00:00:00,000 --> 00:00:00,000
Now the data: 

111
00:00:00,000 --> 00:00:00,000
first read
the census of 1981, 
this was a crucial piece. 

112
00:00:00,000 --> 00:00:00,000
I think there's very interesting 
questions
to ask about why the government of 

113
00:00:00,000 --> 00:00:00,000
Guatemala conducted a census on 
the eve of committing
genocide their is 

114
00:00:00,000 --> 00:00:00,000
excellent work done by historical demographers 
about the use of censuses in
mass violence. 

115
00:00:00,000 --> 00:00:00,000
It has been common 
throughout history. 

116
00:00:00,000 --> 00:00:00,000
Similarly, or excuse me, 
in parallel
there were four very large projects, 

117
00:00:00,000 --> 00:00:00,000
first the CEH a group of 
non-governmental human-rights
groups, 

118
00:00:00,000 --> 00:00:00,000
collected 1240 records of deaths in 
this three-county region, 

119
00:00:00,000 --> 00:00:00,000
next the Catholic
Church collected 
a bit fewer than 800 deaths, 

120
00:00:00,000 --> 00:00:00,000
the truth commission the Comisión para

el Esclarecimiento Histórico conducted a 

121
00:00:00,000 --> 00:00:00,000
really big research project in the 
late
1990s and of that 

122
00:00:00,000 --> 00:00:00,000
we got information about a little bit 
more than a thousand deaths and

123
00:00:00,000 --> 00:00:00,000
then the national program for compensation is very 
very large and gave us about 4700
records of death. 

124
00:00:00,000 --> 00:00:00,000
Now this is interesting but 
this is not unique many of the 

125
00:00:00,000 --> 00:00:00,000
deaths
are reported in common 
across those data sources and so 

126
00:00:00,000 --> 00:00:00,000
we think about this in terms of a 
bend
diagram. 

127
00:00:00,000 --> 00:00:00,000
We think about how did these 
different data sets intersect with 

128
00:00:00,000 --> 00:00:00,000
each other or collide
with each other 
and we can diagram that as in 

129
00:00:00,000 --> 00:00:00,000
the sense of these three 
white circles
intersecting but as I mentioned 

130
00:00:00,000 --> 00:00:00,000
earlier we're also interested in 
what we have not observed
and this is 

131
00:00:00,000 --> 00:00:00,000
crucial for us because when we're 
thinking about how much information 

132
00:00:00,000 --> 00:00:00,000
we have
we have to distinguish between 
the world on the left in which 

133
00:00:00,000 --> 00:00:00,000
are intersecting circles
cover about 
a third of the reality versus the world on 

134
00:00:00,000 --> 00:00:00,000
the right where our intersecting
circles 
cover all of reality. 

135
00:00:00,000 --> 00:00:00,000
These are very different worlds and the reason 
they're 

136
00:00:00,000 --> 00:00:00,000
so
different is not simply 
because we want to know the magnitude, 

137
00:00:00,000 --> 00:00:00,000
not simply because we
want to know the 
total number of killings thats important. 

138
00:00:00,000 --> 00:00:00,000
But even more important we
have 
to know that we

139
00:00:00,000 --> 00:00:00,000
've covered with estimated in equal 
proportions the two parties. 

140
00:00:00,000 --> 00:00:00,000
We have
to estimate in 
equal proportions the number of deaths of 

141
00:00:00,000 --> 00:00:00,000
non-indigenous people and the
number 
of deaths of indigenous people 

142
00:00:00,000 --> 00:00:00,000
because if we don't 
get those estimates correct 

143
00:00:00,000 --> 00:00:00,000
our
comparison of their 
mortality rates will be biased. 

144
00:00:00,000 --> 00:00:00,000
Our store will be wrong we 
will fail
to speak truth to power. 

145
00:00:00,000 --> 00:00:00,000
Can't have that. 

146
00:00:00,000 --> 00:00:00,000
So what do we do? 

147
00:00:00,000 --> 00:00:00,000
Algebra! 

148
00:00:00,000 --> 00:00:00,000
Algebra is our
friend, so I'm 
gonna give you just a tiny taste of how 

149
00:00:00,000 --> 00:00:00,000
we solve this problem and I'm

going to introduce a series of assumptions 

150
00:00:00,000 --> 00:00:00,000
those of you who would 
like to debate those
assumptions: 

151
00:00:00,000 --> 00:00:00,000
I invite you 
to join me after the talk and we 

152
00:00:00,000 --> 00:00:00,000
will talk endlessly and tediously

about capture heterogeneity. 

153
00:00:00,000 --> 00:00:00,000
But in the short term, 
we have a universe of total killings
in a 

154
00:00:00,000 --> 00:00:00,000
specific time-space ethnicity location and of 
that we have to projects 

155
00:00:00,000 --> 00:00:00,000
A and B. A
captures 
some number of deaths from the universe N, 

156
00:00:00,000 --> 00:00:00,000
and the probability of with which 
a death
is captured by project 

157
00:00:00,000 --> 00:00:00,000
A from the universe N is by 
elementary probability theory the

158
00:00:00,000 --> 00:00:00,000
N. Similarly the probability with 
which a death from N 

159
00:00:00,000 --> 00:00:00,000
is documented by project B is
B 
over N and this is the cool part: 

160
00:00:00,000 --> 00:00:00,000
the probability with which are 
death is documented by both
A and B is 

161
00:00:00,000 --> 00:00:00,000
M. Now we can put the two databases 
together we can compare them. 

162
00:00:00,000 --> 00:00:00,000
Let's talk about
the use of random 
force classifiers and clustering to do that later 

163
00:00:00,000 --> 00:00:00,000
but we can put the two databases

together compare them, 

164
00:00:00,000 --> 00:00:00,000
determine the deaths that are in 
M that is in both A and B and
divide M by N 

165
00:00:00,000 --> 00:00:00,000
But also by probability theory 
the probability that a death occurs in 

166
00:00:00,000 --> 00:00:00,000
M is
equal to the product of the 
individual probabilities. 

167
00:00:00,000 --> 00:00:00,000
The probability of any compound event, 
an
event made up of two 

168
00:00:00,000 --> 00:00:00,000
independent events is equal to 
the product of those two events,

169
00:00:00,000 --> 00:00:00,000
so M over N is equal to A over 
N times B over N solve for N. solve for 

170
00:00:00,000 --> 00:00:00,000
N. Multiplied by
through 
N squared and divide by M we have an estimate 

171
00:00:00,000 --> 00:00:00,000
N which is equal to AB over M.

square Now the lights in my eyes I 

172
00:00:00,000 --> 00:00:00,000
can't see but I saw a few light 
bulbs go off over people's
heads and when 

173
00:00:00,000 --> 00:00:00,000
I showed this proof to 
the judge in the trial of general 

174
00:00:00,000 --> 00:00:00,000
Rios I saw a

light bulb go on over her head. 

175
00:00:00,000 --> 00:00:00,000
It's a beautiful thing, 
so a beautiful thing. 

176
00:00:00,000 --> 00:00:00,000
*Applause* So
we do
n't do it in two systems because 

177
00:00:00,000 --> 00:00:00,000
that takes a lot of assumptions we 
do it in four.

178
00:00:00,000 --> 00:00:00,000
You will recall that we have 
four data sources that we have for 

179
00:00:00,000 --> 00:00:00,000
data sources we organize
the 
data sources in this format such that 

180
00:00:00,000 --> 00:00:00,000
we have an inclusion an 
explosion pattern
in the table on the left which, 

181
00:00:00,000 --> 00:00:00,000
for which we can define the number of deaths 
which fall
into each of these 

182
00:00:00,000 --> 00:00:00,000
intersecting patterns and I
'll give you a very quick metaphor here.

183
00:00:00,000 --> 00:00:00,000
The metaphor is: 

184
00:00:00,000 --> 00:00:00,000
imagine that you have two 
dark rooms and you want to assess 

185
00:00:00,000 --> 00:00:00,000
the size
of those two rooms which room is 
larger and the only tool that 

186
00:00:00,000 --> 00:00:00,000
you have to assess the
size of those 
rooms is a handful a little rubber balls. 

187
00:00:00,000 --> 00:00:00,000
The little rubber balls 
have
a property that when 

188
00:00:00,000 --> 00:00:00,000
they hit each other they make a 
sound *Makes one click sound* 

189
00:00:00,000 --> 00:00:00,000
so we
throw the balls into the first round 
and we listen and we hear *Makes 

190
00:00:00,000 --> 00:00:00,000
several click sounds
We collect 
the balls and go to the second room, 

191
00:00:00,000 --> 00:00:00,000
throw them with 
equal force imagining
us (???

192
00:00:00,000 --> 00:00:00,000
) with equal density we throw 
the balls into the second room with 

193
00:00:00,000 --> 00:00:00,000
equal force and
we hear *Makes 
one clicking sound* So which room is larger? 

194
00:00:00,000 --> 00:00:00,000
The second room, because 
we
hear fewer collisions, right? 

195
00:00:00,000 --> 00:00:00,000
Well the estimation, 
the tox example I gave in the 

196
00:00:00,000 --> 00:00:00,000
previous slide
is the 
mathematical formalization of the intuition 

197
00:00:00,000 --> 00:00:00,000
that fewer collisions mean a 
larger space
and so what we

198
00:00:00,000 --> 00:00:00,000
're doing here 
is laying out the pattern of collisions 

199
00:00:00,000 --> 00:00:00,000
not just the collisions
the 
pairwise collisions but the three way 

200
00:00:00,000 --> 00:00:00,000
and 4-way collisions and that allows 
us to
make the estimate that was shown 

201
00:00:00,000 --> 00:00:00,000
in the bar graph of the 
light part of each of the bars.

202
00:00:00,000 --> 00:00:00,000
So we can come back to our conclusion 
and put a confidence interval on 

203
00:00:00,000 --> 00:00:00,000
the estimates
and the confidence 
intervals are shown there. 

204
00:00:00,000 --> 00:00:00,000
Now I'm gonna move through this 
somewhat more
quickly to get to the end of 

205
00:00:00,000 --> 00:00:00,000
the talk but I wanna put up one more slide 
that was used
in the testimony and that is 

206
00:00:00,000 --> 00:00:00,000
that we divided time into sixteen 
month periods and compared
the 

207
00:00:00,000 --> 00:00:00,000
16-month period of 
general Riosis governance of only 

208
00:00:00,000 --> 00:00:00,000
16 month because we April 
and july
because it is only a 

209
00:00:00,000 --> 00:00:00,000
few days in August a few days in march 
so we shaved those off it.

210
00:00:00,000 --> 00:00:00,000
16-month period of general 
Riosis government and compared it to 

211
00:00:00,000 --> 00:00:00,000
several periods before
and after 
and I think that the key observation here 

212
00:00:00,000 --> 00:00:00,000
is met the rate of killing 
against indigenous
people is 

213
00:00:00,000 --> 00:00:00,000
substantially higher done 
under general Riosis government than 

214
00:00:00,000 --> 00:00:00,000
under previous
or succeeding 
governments but more importantly 

215
00:00:00,000 --> 00:00:00,000
the ratio between the two the 
relative risk
of being killed as an 

216
00:00:00,000 --> 00:00:00,000
indigenous person was at its peak during 
the government of general
Riosis. 

217
00:00:00,000 --> 00:00:00,000
Have we proven genocide? 

218
00:00:00,000 --> 00:00:00,000
No. This is evidence consistent with the hypothesis

that a genocide was committed. 

219
00:00:00,000 --> 00:00:00,000
The finding of genocide is a 
legal finding not so much
a scientific one. 

220
00:00:00,000 --> 00:00:00,000
So as scientists our job 
is to provide evidence but the 

221
00:00:00,000 --> 00:00:00,000
finders of
fact the judges in this 
case can use in their determination 

222
00:00:00,000 --> 00:00:00,000
this is evidence consistent
with 
the hypothesis. 

223
00:00:00,000 --> 00:00:00,000
Where this evidence otherwise as scientists 
we would say we would rechecked
the 

224
00:00:00,000 --> 00:00:00,000
hypothesis that genocide was committed 
however with this evidence we find 

225
00:00:00,000 --> 00:00:00,000
that the
evidence the data is consistent 
with the the prosecutions hypothesis. 

226
00:00:00,000 --> 00:00:00,000
So, it worked! 

227
00:00:00,000 --> 00:00:00,000
Rios
Montt was convicted on 
genocide charges. 

228
00:00:00,000 --> 00:00:00,000
*Applause* For a week! 

229
00:00:00,000 --> 00:00:00,000
Then the Constitutional Court

intervened, there 

230
00:00:00,000 --> 00:00:00,000
I know a couple of experts 
on Guatemala here in the audience 

231
00:00:00,000 --> 00:00:00,000
who can
tell you more about 
why that happened exactly what happened. 

232
00:00:00,000 --> 00:00:00,000
However the 
constitutional
court ordered a new trial, 

233
00:00:00,000 --> 00:00:00,000
which is at this time scheduled for 
the very beginning of 2015.

234
00:00:00,000 --> 00:00:00,000
And I look forward to testifying again 
and again and again and again! 

235
00:00:00,000 --> 00:00:00,000
*Applause* Look
but 
i wanna come back to this point 

236
00:00:00,000 --> 00:00:00,000
because as a bunch of technologist there is 
a lot
of folks really like technology here. 

237
00:00:00,000 --> 00:00:00,000
I really like it too. 

238
00:00:00,000 --> 00:00:00,000
Technology doesn't get us to
science you 
have to have science to get you to science. 

239
00:00:00,000 --> 00:00:00,000
Technology helps you 
to organize
the data. 

240
00:00:00,000 --> 00:00:00,000
It helps you do 
all kinds extremely great and cool 

241
00:00:00,000 --> 00:00:00,000
things without which we would
n't
be able to even do the science, 

242
00:00:00,000 --> 00:00:00,000
but you can't have just technology 
you can't just have a
bunch of data and make 

243
00:00:00,000 --> 00:00:00,000
conclusions that's naive 
and you will get the wrong conclusions.

244
00:00:00,000 --> 00:00:00,000
The point of rigorous statistics is 
to be right and there is a little bit of 

245
00:00:00,000 --> 00:00:00,000
a caveat
there - or to 
at least know how uncertain you are. 

246
00:00:00,000 --> 00:00:00,000
Statistics is often called 
the science
of uncertainty 

247
00:00:00,000 --> 00:00:00,000
that is actually my 
favorite definition of it. 

248
00:00:00,000 --> 00:00:00,000
So I'm going to assume that

we care about getting it right. 

249
00:00:00,000 --> 00:00:00,000
No one laughed, that's good. 

250
00:00:00,000 --> 00:00:00,000
Not everyone does to my distress.

251
00:00:00,000 --> 00:00:00,000
So if you only have some of 
the data and I will argue 

252
00:00:00,000 --> 00:00:00,000
that with always only have some
of the data 
you need some kind of model 

253
00:00:00,000 --> 00:00:00,000
that will tell you the relationship between 
your
data and the real world. 

254
00:00:00,000 --> 00:00:00,000
Statistician pollen inference 
in order to get from here to 

255
00:00:00,000 --> 00:00:00,000
there
you're gonna need some kind of a 
probability model that tells 

256
00:00:00,000 --> 00:00:00,000
you why your data is like
the world or in 
what sense you have to tweet, 

257
00:00:00,000 --> 00:00:00,000
twiddle and do algebra with your data to get
from 
what you can observe to what is actually true. 

258
00:00:00,000 --> 00:00:00,000
And statistics is about comparisons

yeah we get a big number and journalist 

259
00:00:00,000 --> 00:00:00,000
love the big number but it's really 
about these
relationships and patterns 

260
00:00:00,000 --> 00:00:00,000
so to get those relationships 
and patterns in order for them
to be 

261
00:00:00,000 --> 00:00:00,000
right in order for our answer 
to be correct everyone of the estimates 

262
00:00:00,000 --> 00:00:00,000
we make
for every point in 
the pattern has to be right. 

263
00:00:00,000 --> 00:00:00,000
It's a hard problem it's 
a hard problem and
what I worry about is 

264
00:00:00,000 --> 00:00:00,000
that we have come into this world 
in which people throw the notion
of 

265
00:00:00,000 --> 00:00:00,000
big data around as though 
the data allows us to make 

266
00:00:00,000 --> 00:00:00,000
an end-run around problems of sampling

and modeling it doesn't. 

267
00:00:00,000 --> 00:00:00,000
So as technologist reason - 
you know ranting at you guys about
it is 

268
00:00:00,000 --> 00:00:00,000
that it's very tempting to 
have a lot of data and think you have an answer and it

269
00:00:00,000 --> 00:00:00,000
even more tempting because an 
industry contacts you might be right. 

270
00:00:00,000 --> 00:00:00,000
Not so much in 
human rights
not so much. 

271
00:00:00,000 --> 00:00:00,000
Violence is a hidden process the people 
who commit violence have an 

272
00:00:00,000 --> 00:00:00,000
enormous
commitment to hiding, 
distorting it, explaining it in different ways. 

273
00:00:00,000 --> 00:00:00,000
All of those things

dramatically affect the information 

274
00:00:00,000 --> 00:00:00,000
that is produced from the violence 
that we're going
to use to do our analysis. 

275
00:00:00,000 --> 00:00:00,000
So we usually don't know 
what we don't know in human rights and

276
00:00:00,000 --> 00:00:00,000
data collection and that means 
that we don't know if what we do

277
00:00:00,000 --> 00:00:00,000
n't know is systematically
different from 
what we do know. 

278
00:00:00,000 --> 00:00:00,000
Maybe we know about all the lawyers and we 
don't know about
the people the countryside 

279
00:00:00,000 --> 00:00:00,000
maybe we know about all the 
indigenous people and not the non-indigenous
people. 

280
00:00:00,000 --> 00:00:00,000
If that were true the argument 
that I just made would be merely 

281
00:00:00,000 --> 00:00:00,000
an artifact of
the 
reporting process rather than some true analysis. 

282
00:00:00,000 --> 00:00:00,000
Now we did that estimations why

I believe we can reject that critic, 

283
00:00:00,000 --> 00:00:00,000
but that's what we have 
to worry about and let's 

284
00:00:00,000 --> 00:00:00,000
go back
to the bend diagrammed 
and say which of these is accurate? 

285
00:00:00,000 --> 00:00:00,000
It's not just for one of the
points in 
our pattern analysis the problem is that we're 

286
00:00:00,000 --> 00:00:00,000
going to compare things as in
Peru 
where we compared killings committed by the 

287
00:00:00,000 --> 00:00:00,000
peruvian army against 
killings committed
by the ?? 

288
00:00:00,000 --> 00:00:00,000
We found there that in fact 
we knew very little about what the ?? 

289
00:00:00,000 --> 00:00:00,000
had done. 

290
00:00:00,000 --> 00:00:00,000
Whereas
we know almost everything 
what the peruvian army had done. 

291
00:00:00,000 --> 00:00:00,000
This is called the coverage
rate. 

292
00:00:00,000 --> 00:00:00,000
The rate between what we know and what we do
n't know and raw data however big does
not get us to ?? 

293
00:00:00,000 --> 00:00:00,000
And here is a bunch of kinds of raw data 
that I've used in and that I really
enjoy using. 

294
00:00:00,000 --> 00:00:00,000
You know - 
truth commission testimonies, ?? 

295
00:00:00,000 --> 00:00:00,000
investigations, press articles, 
SMS messages
crowdsourcing 

296
00:00:00,000 --> 00:00:00,000
and your documentation, 
social media Feeds, perpetrator records, 

297
00:00:00,000 --> 00:00:00,000
government
archives, 
state agency registries - 

298
00:00:00,000 --> 00:00:00,000
I know those sound all the same but they 
actually
turn out to be slightly different. 

299
00:00:00,000 --> 00:00:00,000
Happy to talk ?? 

300
00:00:00,000 --> 00:00:00,000
to tell. 

301
00:00:00,000 --> 00:00:00,000
Refugee camp records any

non random sample. 

302
00:00:00,000 --> 00:00:00,000
All of those are gonna take some 
kind of probability model and we
do

303
00:00:00,000 --> 00:00:00,000
n't have that 
many probability models to use. 

304
00:00:00,000 --> 00:00:00,000
So raw ? 

305
00:00:00,000 --> 00:00:00,000
data is great for cases 
but
it doesn't get you to patterns 

306
00:00:00,000 --> 00:00:00,000
and patterns again patterns are the thing 
that allow us
to do analysis. 

307
00:00:00,000 --> 00:00:00,000
They are the thing, 
the patterns of what get us to something 

308
00:00:00,000 --> 00:00:00,000
that we can use
to help prosecutors, 
advocates and the, and the victims themselves. 

309
00:00:00,000 --> 00:00:00,000
I gave a version of
this talk, 
a earlier version of this 

310
00:00:00,000 --> 00:00:00,000
talk several years 
ago in meeting in Medellín
Columbia. 

311
00:00:00,000 --> 00:00:00,000
I've worked a lot in Columbia, it's 
really, it's a great place to work on. 

312
00:00:00,000 --> 00:00:00,000
There's
really terrific victims rights 
groups there and a woman from a 

313
00:00:00,000 --> 00:00:00,000
township smaller than a
county near too 
Medellín came up to me after the talk and she said: 

314
00:00:00,000 --> 00:00:00,000
you know a lot
of people 
you know .. 

315
00:00:00,000 --> 00:00:00,000
I'm a human rights activist my job is 
to collect data, 

316
00:00:00,000 --> 00:00:00,000
I tell stories
about people 
who have suffered But 

317
00:00:00,000 --> 00:00:00,000
there are people in my village 
I know who have had people
in 

318
00:00:00,000 --> 00:00:00,000
their families disappeared and they
're never gonna talk about ever. 

319
00:00:00,000 --> 00:00:00,000
Never going to
be able to use 
their names, because they are afraid. 

320
00:00:00,000 --> 00:00:00,000
Can't name the victims 
at least we
better count them. 

321
00:00:00,000 --> 00:00:00,000
So about that counting 
there's three ways to do it right: 

322
00:00:00,000 --> 00:00:00,000
You can
have a perfect census, 
you can have all the data. 

323
00:00:00,000 --> 00:00:00,000
Yeah it's nice 
good luck with that.

324
00:00:00,000 --> 00:00:00,000
You can have a random sample of 
the Population. 

325
00:00:00,000 --> 00:00:00,000
That's hard. 

326
00:00:00,000 --> 00:00:00,000
Sometimes doable but 
very hard
in my experience 

327
00:00:00,000 --> 00:00:00,000
we rarely interview victims 
of homicide very rarely. 

328
00:00:00,000 --> 00:00:00,000
*Laughing* And that
means there's 
a complicated probability relationship between the 

329
00:00:00,000 --> 00:00:00,000
person you sampled the interview
and the depth 
that they talk to you about. 

330
00:00:00,000 --> 00:00:00,000
Or you can do some kind of mysterious 
modeling
of the sampling process 

331
00:00:00,000 --> 00:00:00,000
which is the which is the essence of 
what I proposed in the earlier
slide. 

332
00:00:00,000 --> 00:00:00,000
So what can we 
do with raw data guys? 

333
00:00:00,000 --> 00:00:00,000
We can say that a case exists. 

334
00:00:00,000 --> 00:00:00,000
Ok, - that's
actually important! 

335
00:00:00,000 --> 00:00:00,000
We can say 
something happened with raw data. 

336
00:00:00,000 --> 00:00:00,000
We can say 
we know something
about that case. 

337
00:00:00,000 --> 00:00:00,000
We can say there were 
hundred victims in that case or at least 

338
00:00:00,000 --> 00:00:00,000
a hundred
victims in that case, 
if we can name a hundred people 

339
00:00:00,000 --> 00:00:00,000
but we can't do comparisons 
this is
the biggest massacre this year. 

340
00:00:00,000 --> 00:00:00,000
We don't really know because we don't know 
about that massacres
we don't know about. 

341
00:00:00,000 --> 00:00:00,000
No Patterns. 

342
00:00:00,000 --> 00:00:00,000
Don't talk about the 
hot spots of violence. 

343
00:00:00,000 --> 00:00:00,000
No we don't
know that. 

344
00:00:00,000 --> 00:00:00,000
Happy to talk more about 
that if we gather after but i 

345
00:00:00,000 --> 00:00:00,000
wanna come to a close
here with 
the importance of getting it right. 

346
00:00:00,000 --> 00:00:00,000
I've talked about one case today 
this is another
case the case of this man: 

347
00:00:00,000 --> 00:00:00,000
Edgar Fernando Garcia. 

348
00:00:00,000 --> 00:00:00,000
Mister Garcia was a student 
labor
leader in guatemala early in the 1980s. 

349
00:00:00,000 --> 00:00:00,000
He left his office in 
February 1984 did not come
home. 

350
00:00:00,000 --> 00:00:00,000
People reported later that they saw someone 
shoving mister Garcia into a vehicle
and driving away. 

351
00:00:00,000 --> 00:00:00,000
His widow became a very important 
human rights activist in Guatemala
and now 

352
00:00:00,000 --> 00:00:00,000
she's a very important and in my opinion 
impressive politician and there's her infant
daughter. 

353
00:00:00,000 --> 00:00:00,000
She continued to struggle to find out what 
had happened to mister Garcia for
decades and in 

354
00:00:00,000 --> 00:00:00,000
2006 documents came to light in the 
national archives of the.. 

355
00:00:00,000 --> 00:00:00,000
excuse me
of the 
historical archives of the national Police, 

356
00:00:00,000 --> 00:00:00,000
showing that the police 
had realized
an operation in the area of mister 

357
00:00:00,000 --> 00:00:00,000
Garcia's office and it was 
very likely that they had
disappeared him. 

358
00:00:00,000 --> 00:00:00,000
These two guys up here in the 
upper right were police officers in that
area, 

359
00:00:00,000 --> 00:00:00,000
they were arrested charged with 
the disappearance of mister Garcia and convicted.

360
00:00:00,000 --> 00:00:00,000
Part of the evidence used 
to convict them was communications 

361
00:00:00,000 --> 00:00:00,000
meta data showing that

documents flowed through the archive. 

362
00:00:00,000 --> 00:00:00,000
I mean paper communications. 

363
00:00:00,000 --> 00:00:00,000
We coded it by hand
we went through and read 
the from and to lines from every Memo 

364
00:00:00,000 --> 00:00:00,000
and they were convicted in
2010 
and after that conviction mister 

365
00:00:00,000 --> 00:00:00,000
Garcias infant daughter - now a 
grown woman - was
clearly joyful. 

366
00:00:00,000 --> 00:00:00,000
Justice brings closure to a family 
that never knows 

367
00:00:00,000 --> 00:00:00,000
when to start talking
about someone in 
the past tense. 

368
00:00:00,000 --> 00:00:00,000
Perhaps even more powerfully: 

369
00:00:00,000 --> 00:00:00,000
those guys grand boss their

bosses boss kernel Héctor Bol de la Cruz, this man 

370
00:00:00,000 --> 00:00:00,000
right here, was convicted of mister

Garcias disappearance in september this year. 

371
00:00:00,000 --> 00:00:00,000
*Applause* I don't know if any of you 
have
ever been dissident students 

372
00:00:00,000 --> 00:00:00,000
but if you've been dissidents 
students demonstrating in
the streets 

373
00:00:00,000 --> 00:00:00,000
think about how you would feel if 
your friends and comrades 

374
00:00:00,000 --> 00:00:00,000
were disappeared
and take a 
long look at kernel de la Cruz. 

375
00:00:00,000 --> 00:00:00,000
Here is the rest of the stuff that we 
will
talk about if we gather afterwards. 

376
00:00:00,000 --> 00:00:00,000
Thank you very much for your attention 
I really
have enjoyed CCC *Applause*

