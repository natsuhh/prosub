The timing information is the `real time` taken from `modules/aligner/time.log` after a run.

video           video length  runtime   comments
-----           ------------  -------   --------
agile           27.2min       17.10min  no noise, clear pronunciation
radio           36.1min       ~88min    finish accent, lots of noise
opening         11.5min       ~15min    quite clear, perfect transcript
keynote         ~55min        fails     Out of Memory Error, please replicate
datamining      26.7min       fails     hangs in liveCMN after 1h, please replicate

