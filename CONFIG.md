# Configuration

## How configuration works

* the file `config/default.json` is *always* loaded first, please add the default for every new configuration option in here as well
* then the file specified with `--config/-c` command line parameter is loaded, it overrides existing options
* when a config file is loaded, and the `parent` option is set, the referenced parent configuration file is loaded first, recursively
* you can override any option from the command line using the `--option/-o` parameter, such as:
    - you can supply boolean arguments with `yes/no` or `1/0` or similar
    - you can supply lists of strings by comma-separating them (`one,two,three`)

            -o name=some-name \
            -o input.audio=path/to/audio.wav  \
            -o steps=preprocess,align,playback \
            -o modules.align.skip=yes \
            -o one-option=1 two-option=2

* the type of a configuration option is read from `config/default.json`, e.g. for boolean conversion from the command line


## General

* `parent`: path to a configuration that is loaded before this file
* `output`: output directory name, run name (`name`) will be appended as path component
* `language`: the language seeting used for processing the input. Esamples: "en_US" "de_DE"
* `name`: name of the run, used for file name defaults etc. (Flag: `-n`)
* `loglevel`: one of `debug,info,warning,error,all` (Flag: `-v` for verbose/`all`)
* `save-steps` (bool): should the data be saved separately after every module ran?
* `input.transcript`: path to text file of transcript. Variables:
    - `{name}`: run name
* `input.audio`: path to audio file. Variables:
    - `{name}`: run name
* `steps` (list<str>): list of module names to run, you can choose variants of a module with the syntax 'modulename/variant'
* `modules`: dictionary of module identifiers and their configuration options, see below
* `data` (dict|str): inital data object (inline JSON) or path to exported data file -- use this to build tests without having to run the previous modules


## Modules

Please document here if your module supports any custom configuration options. Place default values in `config/default.json`.

### Align (`align`)

* `memory`: argument for java memory heap size, passed to java with `-Xmx<memory>`, e.g. `4G` for 4 gigabytes
* `cp`: classpath for sphinx aligner
* `acousticModel`: path
* `dictionary`: path
* `g2p`: path
* `skip` (bool): skip the actual java command, expect the aligner output to exists already in the temp file from previous runs
* `gzip-log` (bool): pipe the log output through gzip to decrease file size (default: true)


### Analyze (`analyze`)

This module has no configuration options.


### Chunkify (`chunkify`)

This module has no configuration options.


### ChunkAlign (`chunkalign`)

This module has no configuration options.


### Input (`input`)

* `format`: input format to use, one of `txt,srt,auto` -- `auto` is auto-detection from file extension


### Normalize (`normalize`)

* `use-nltk`: yes for nltk normalization, no for maryTTS normalization


### Output (`output`)

(WIP)

* `format`: output format to use, one of `srt,usf`
* `file`: path to output file, use these variables (see default config for example):
    - `{output}`: data output directory
    - `{name}`: run name
    - `{format}`: content of `format` option, useful as file extension


### Playback (`playback`)

This module has no configuration options.


### Spellcheck (`spellcheck`)

* `ignore`: file for words to ignore, variables:
    - `{name}`: run name


### Syntax parsing (`syntax`)

* `parser-model`: path to parser model file
* `tagger-model`: path to tagger model file
* `skip` (bool): skip both tagger and parser
* `skip-tagger` (bool): skip the actual tagging, expect the output to exists already in the temp file from previous runs
* `skip-parser` (bool): skip the actual parsing, expect the output to exists already in the temp file from previous runs
